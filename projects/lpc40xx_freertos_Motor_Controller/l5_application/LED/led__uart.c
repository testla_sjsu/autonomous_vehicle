#include "led__uart.h"

#include "board_io.h"
#include "clock.h"
#include "gpio.h"
#include "uart.h"

static uart_e led_uart = UART__3;
static char output_data = 'a';

void led_uart__init() {
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2); // P4.28 - Uart-3 Tx
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2); // P4.29 - Uart-3 Rx

  uart__init(led_uart, clock__get_peripheral_clock_hz(), 9600);

  QueueHandle_t rxq_handle = xQueueCreate(4, sizeof(char));   // Nothing to receive
  QueueHandle_t txq_handle = xQueueCreate(100, sizeof(char)); // We send a lot of data
  uart__enable_queues(led_uart, rxq_handle, txq_handle);
}

void change_LED_color() {
  bool switch_zero = gpio__get(board_io__get_sw0());
  bool switch_one = gpio__get(board_io__get_sw1());

  if (switch_zero) {
    uart__put(UART__3, '0', 0);
  } else if (switch_one) {
    uart__put(UART__3, '1', 0);
  }

  char input = 0;
  if (uart__get(UART__3, &input, 2)) {
    if (input == '0') {
      gpio__reset(board_io__get_led0());
    } else if (input == '1') {
      gpio__reset(board_io__get_led1());
    } else {
      gpio__set(board_io__get_led0());
      gpio__set(board_io__get_led1());
    }
  }
}
#pragma once

#include <stdbool.h>
#include <stddef.h>

void led__turn_on_0();
void led__turn_on_1();
void led__turn_on_2();
void led__turn_on_3();

void led__turn_off_0();
void led__turn_off_1();
void led__turn_off_2();
void led__turn_off_3();

void led__turn_on_all_leds();
void led__turn_off_all_leds();
#include "led__manager.h"

#include "board_io.h"
#include "gpio.h"

void led__turn_on_0() { gpio__reset(board_io__get_led0()); }
void led__turn_on_1() { gpio__reset(board_io__get_led1()); }
void led__turn_on_2() { gpio__reset(board_io__get_led2()); }
void led__turn_on_3() { gpio__reset(board_io__get_led3()); }

void led__turn_off_0() { gpio__set(board_io__get_led0()); }
void led__turn_off_1() { gpio__set(board_io__get_led1()); }
void led__turn_off_2() { gpio__set(board_io__get_led2()); }
void led__turn_off_3() { gpio__set(board_io__get_led3()); }

void led__turn_on_all_leds() {
  led__turn_on_0();
  led__turn_on_1();
  led__turn_on_2();
  led__turn_on_3();
}

void led__turn_off_all_leds() {
  led__turn_off_0();
  led__turn_off_1();
  led__turn_off_2();
  led__turn_off_3();
}
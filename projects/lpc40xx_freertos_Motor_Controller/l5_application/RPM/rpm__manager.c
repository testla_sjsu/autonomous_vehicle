#include "rpm__manager.h"
#include "board_io.h"
#include "gpio.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

static volatile float interrupt_count;
static volatile float sensor_speed__raw;
static volatile float sensor_speed__rpm;
static volatile float sensor_speed__kph;

const float tire_radius__meters = 0.06223; // CHANGE VALUE

void rpm__init() {
  lpc_peripheral__turn_on_power_to(LPC_PERIPHERAL__TIMER2);
  LPC_TIM2->IR |= 0x3F;                // IR = Interrupt Register. Setting all 1s to bits 0-5 resets all interrupts
                                       // Bit 2 - (CAP0I) is set to 1 to generate an interrupt on every CAP0 event
  LPC_TIM2->CCR = (1 << 1) | (1 << 2); // TC captured in CR0 on falling edge and interrupt is generated
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__TIMER2, rpm__sensor_interrupt, NULL);

  uint32_t prescalar_divider =
      9590; // The prescale divider counts the timer in the PR register up until this value before incrementing TC once
  LPC_TIM2->PR = prescalar_divider; // This prescalar was chosen to give us more reasonable values of TC to work with
                                    // (10^3 vs 10^7)

  LPC_TIM2->TCR |= (1 << 0); // Enable the Timer (TC) and Prescalar (PC) clocks for counting
  LPC_TIM2->TC = 0;          // Set the Timer Clock to 0

  gpio__construct_with_function(2, 6, 3); // Port 2.6, Function 3 for CAP
  interrupt_count = 0;
  sensor_speed__rpm = 0;
  sensor_speed__kph = 0;
  // Note, the encoder fires 4 pulses per revolution, and 4 *legitimate* interrupts per revolution
}

void rpm__sensor_interrupt() {
  // if (LPC_TIM2->TC > 105) {
  //   interrupt_count++;
  // }
  // LPC_TIM2->TC = 0;

  if (LPC_TIM2->TC > 105) {
    interrupt_count++;
  }

  LPC_TIM2->TC = 0;
  LPC_TIM2->IR |= 0x10;
}

void rpm__update_all_speeds() {
  sensor_speed__raw = interrupt_count;
  interrupt_count = 0;

  rpm__calculate_rpm__2hz();
  rpm__calculate_kph__2hz();
}

void rpm__calculate_rpm__2hz() {
  sensor_speed__rpm =
      sensor_speed__raw / 3.92f * 5 * 60; // (# of interrupts) / (3.92 interrupts per revolution) * (2 to
                                          // scale to 1 second (2hz -> 1hz)) * (60 to scale to 1 minute)
}

void rpm__calculate_kph__2hz() { sensor_speed__kph = 0.12f * 3.14159f * tire_radius__meters * sensor_speed__rpm; }

float rpm__get_speed__raw() { return sensor_speed__raw; }
float rpm__get_speed__rpm() { return sensor_speed__rpm; }
float rpm__get_speed__kph() { return sensor_speed__kph; }
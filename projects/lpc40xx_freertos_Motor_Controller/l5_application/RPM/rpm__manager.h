#pragma once

#include <stddef.h>
#include <stdint.h>

void rpm__init();
void rpm__sensor_interrupt();

void rpm__update_all_speeds();
void rpm__calculate_rpm__2hz();
void rpm__calculate_kph__2hz();

float rpm__get_speed__raw();
float rpm__get_speed__rpm();
float rpm__get_speed__kph();
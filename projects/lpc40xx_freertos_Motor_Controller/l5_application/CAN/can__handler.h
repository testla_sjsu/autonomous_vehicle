#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "pwm__manager.h"

bool can_handler__init(void);
void can_handler__manage_mia__10hz(void);
void can_handler__handle_all_incoming_messages__10hz(uint32_t callback_time, bool disable_motor, bool disable_steering);
void can_handler__transmit_messages__10hz(ESC_CALIBRATION_STATUS esc);
void can_handler__health_check__1hz(void);
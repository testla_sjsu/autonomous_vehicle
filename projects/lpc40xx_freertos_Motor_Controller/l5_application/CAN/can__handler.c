#include "can__handler.h"

#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"

#include <stdio.h>

#include "can__mia_configurations.c"
#include "pwm__calculations.h"
#include "rpm__manager.h"
#include "testla_proto1.h"

// UPDATE ALL MIAs
const uint32_t dbc_mia_threshold_DRIVER_STEERING = 500;
const dbc_DRIVER_STEERING_s dbc_mia_replacement_DRIVER_STEERING = {.DRIVER_STEERING_yaw = 0.0f,
                                                                   .DRIVER_STEERING_velocity = 0.0f};

static dbc_DRIVER_STEERING_s can_msg__driver_steering;

bool can_handler__init(void) {
  bool status = false;
  if (!can__init(can1, 500, 1024, 1024, NULL, NULL)) {
    return status;
  } else {
    status = true;
  }

  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can1);

  return status;
}

void can_handler__manage_mia__10hz(void) {
  const uint32_t mia_increment_value = 100;

  if (dbc_service_mia_DRIVER_STEERING(&can_msg__driver_steering, mia_increment_value)) {
    gpio__reset(board_io__get_led3());
  } else {
    gpio__set(board_io__get_led3());
  }
}

void can_handler__handle_all_incoming_messages__10hz(uint32_t callback_time, bool disable_motor,
                                                     bool disable_steering) {
  can__msg_t can_msg = {};

  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    dbc_decode_DRIVER_STEERING(&can_msg__driver_steering, header, can_msg.data.bytes);

    float angle_in_radians = can_msg__driver_steering.DRIVER_STEERING_yaw;
    float speed_in_kph = can_msg__driver_steering.DRIVER_STEERING_velocity;

    set_steering_and_motor_state(angle_in_radians, speed_in_kph, callback_time, disable_motor, disable_steering);

    gpio__toggle(board_io__get_led0());
  }
}

void can_handler__transmit_messages__10hz(ESC_CALIBRATION_STATUS esc) {

  ///////////////////////-----MOTOR HEARTBEAT-----///////////////////////
  dbc_MOTOR_HEARTBEAT_s motor_heartbeat_message = {};

  motor_heartbeat_message.MOTOR_HEARTBEAT_speed_raw = rpm__get_speed__raw();
  motor_heartbeat_message.MOTOR_HEARTBEAT_speed_rpm = rpm__get_speed__rpm();
  motor_heartbeat_message.MOTOR_HEARTBEAT_speed_kph = rpm__get_speed__kph();

  can__msg_t can_msg__heartbeat = {};
  const dbc_message_header_t header_heartbeat =
      dbc_encode_MOTOR_HEARTBEAT(can_msg__heartbeat.data.bytes, &motor_heartbeat_message);

  can_msg__heartbeat.msg_id = header_heartbeat.message_id;
  can_msg__heartbeat.frame_fields.data_len = header_heartbeat.message_dlc;

  ///////////////////////-----ESC CALIBRATION-----///////////////////////
  dbc_MOTOR_ESC_CALIBRATED_s calibration_status_message = {};

  calibration_status_message.MOTOR_ESC_CALIBRATED_calibration_status = esc; // unit: esc_calibration_e

  can__msg_t can_msg__esc_status = {};
  const dbc_message_header_t header_esc_status =
      dbc_encode_MOTOR_ESC_CALIBRATED(can_msg__esc_status.data.bytes, &calibration_status_message);

  can_msg__esc_status.msg_id = header_esc_status.message_id;
  can_msg__esc_status.frame_fields.data_len = header_esc_status.message_dlc;

  ///////////////////////-----MOTOR DEBUG-----///////////////////////
  dbc_MOTOR_DEBUG_s motor_debug_message = {};

  motor_debug_message.MOTOR_DEBUG_speed_target = pwm__debug_speed__target();
  motor_debug_message.MOTOR_DEBUG_speed_kph = pwm__debug_speed__kph();
  motor_debug_message.MOTOR_DEBUG_speed_duty = pwm__debug_speed__duty();
  motor_debug_message.MOTOR_DEBUG_integral_error = pwm__debug_integral_error();
  motor_debug_message.MOTOR_DEBUG_current_state = pwm__debug_current_state();
  motor_debug_message.MOTOR_DEBUG_next_state = pwm__debug_next_state();

  can__msg_t can_msg__motor_debug = {};
  const dbc_message_header_t header_motor_debug =
      dbc_encode_MOTOR_DEBUG(can_msg__motor_debug.data.bytes, &motor_debug_message);

  can_msg__motor_debug.msg_id = header_motor_debug.message_id;
  can_msg__motor_debug.frame_fields.data_len = header_motor_debug.message_dlc;

  //-----TRANSMIT-----//
  can__tx(can1, &can_msg__heartbeat, 0);
  can__tx(can1, &can_msg__esc_status, 0);
  can__tx(can1, &can_msg__motor_debug, 0);
  gpio__toggle(board_io__get_led1());
}

void can_handler__health_check__1hz(void) {
  if (can__is_bus_off(can1)) {
    fprintf(stderr, "RESET CAN\n");
    can__reset_bus(can1);
  }
}
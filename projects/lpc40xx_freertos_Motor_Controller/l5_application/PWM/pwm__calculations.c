#include "pwm__calculations.h"

float calculate__kmh_to_duty(float speed_in_kmh) {

  float speed_in_duty = 15.0f;

  if (speed_in_kmh < -0.01f) {
    speed_in_duty = speed_in_kmh / 9.0f + 15.0f - 0.7f; // the 0.7f is an offset to account for motor inertia (reverse)
  } else if (speed_in_kmh > 0.01f) {
    speed_in_duty = speed_in_kmh / 9.0f + 15.0f + 0.7f; // the 0.7f is an offset to account for motor inertia (forward)
  }

  return speed_in_duty;
}

float calculate__radians_to_duty(float steer_angle_in_radians) {
  // Max Steering Angle = 20 degrees = 0.348 Radians
  float angle_in_duty = 0;
  angle_in_duty = steer_angle_in_radians * 14.33f + 15;
  return angle_in_duty;
}
#include "pwm__manager.h"

#include "board_io.h"
#include "gpio.h"
#include "pwm1.h"

#include "pwm__calculations.h"
#include "rpm__manager.h"

const float MAXIMUM_FORWARD_SPEED_KPH = 15.0f;
const float MAXIMUM_REVERSE_SPEED_KPH = -10.0f;

const uint8_t frequency = 100;
const float idle_duty_cycle = 15.0f;
pwm1_channel_e PWM_steering = PWM1__2_0;
pwm1_channel_e PWM_motor = PWM1__2_1;

static float speed_for_motor__kph;
static float speed_for_motor__duty;
static float angle_for_steering;
static float speed_for_motor__tracking;

uint32_t start_reverse_time;

static float error;
const float gain = 0.01f;
static float watchdog_timer_speed;
static float watchdog_timer_reverse_speed;

CAR_MOTION_STATUS current_state;
CAR_MOTION_STATUS next_state;

void pwm__init() {
  pwm1__init_single_edge(frequency);
  setup_gpio_for_PWM();
  send_neutral_duty_signal_to_motor();

  speed_for_motor__kph = 0.0f;
  speed_for_motor__duty = 15.0f;
  angle_for_steering = 0.0f;

  speed_for_motor__tracking = 0.0f;
  error = 0.0f;

  current_state = IDLE;
  next_state = IDLE;
  watchdog_timer_speed = 0;
  watchdog_timer_reverse_speed = 0;
}

bool set_steering_and_motor_state(float target_steering__radians, float target_motor__kph, uint32_t callback_time,
                                  bool disable_motor, bool disable_steering) {

  bool executed = false;

  if (target_motor__kph < MAXIMUM_REVERSE_SPEED_KPH || target_motor__kph > MAXIMUM_FORWARD_SPEED_KPH) {
    gpio__reset(board_io__get_led0());
    return executed;
  }

  executed = true;

  //-------Update the encoder speed reading-------//
  float current_speed = rpm__get_speed__kph();

  //---------------Update a Getter ---------------//
  speed_for_motor__tracking = target_motor__kph;

  //~~~~~~~~~~~~~~~~~~~~~START STATE MACHINE~~~~~~~~~~~~~~~~~~~~~//
  //---Use Driver direction and Current State to determine Next State---//
  if (target_motor__kph >= -0.1f && target_motor__kph <= 0.1f) {
    if (current_state == IDLE) {
      next_state = IDLE;
    } else if (current_state == FORWARD) {
      next_state = BRAKING;
    } else if (current_state == BRAKING) {
      if (current_speed >= -0.1f && current_speed <= 0.1f) {
        next_state = IDLE;
      } else {
        next_state = BRAKING;
      }
    } else if (current_state == START_NEUTRAL_FOR_REVERSE) {
      next_state = IDLE;
    } else if (current_state == NEUTRAL_FOR_REVERSE) {
      next_state = IDLE;
    } else if (current_state == REVERSE) {
      next_state = IDLE;
    }
  } else if (target_motor__kph > 0.1f) {
    if (current_state == IDLE) {
      if (current_speed >= -0.1f && current_speed <= 0.1f) {
        next_state = FORWARD;
      } else {
        next_state = IDLE;
      }
    } else if (current_state == FORWARD) {
      next_state = FORWARD;
    } else if (current_state == BRAKING) {
      next_state = IDLE;
    } else if (current_state == START_NEUTRAL_FOR_REVERSE) {
      next_state = IDLE;
    } else if (current_state == NEUTRAL_FOR_REVERSE) {
      next_state = IDLE;
    } else if (current_state == REVERSE) {
      next_state = IDLE;
    }
  } else if (target_motor__kph < -0.1f) {
    if (current_state == IDLE) {
      next_state = START_NEUTRAL_FOR_REVERSE;
    } else if (current_state == FORWARD) {
      next_state = BRAKING;
    } else if (current_state == BRAKING) {
      if (current_speed >= -0.1f && current_speed <= 0.1f) {
        next_state = START_NEUTRAL_FOR_REVERSE;
      } else {
        next_state = BRAKING;
      }
    } else if (current_state == START_NEUTRAL_FOR_REVERSE) {
      start_reverse_time = callback_time;
      next_state = NEUTRAL_FOR_REVERSE;
    } else if (current_state == NEUTRAL_FOR_REVERSE) {
      if (callback_time >= start_reverse_time + 2) {
        next_state = REVERSE;
        watchdog_timer_reverse_speed = 0;
      } else {
        next_state = NEUTRAL_FOR_REVERSE;
      }
    } else if (current_state == REVERSE) {
      if (current_speed >= -0.1f && current_speed <= 0.1f) {
        watchdog_timer_reverse_speed++;
      }

      if (watchdog_timer_reverse_speed >= 10) {
        next_state = START_NEUTRAL_FOR_REVERSE;
      } else {
        next_state = REVERSE;
      }
    }
  }

  // -------------Error Calculations-------------//
  if (next_state == FORWARD) {
    error = target_motor__kph - current_speed;
  } else if (next_state == REVERSE) {
    error = target_motor__kph + current_speed;
  }

  // -------------Use Next State to set Motor instruction ------------- //
  if (next_state == IDLE) {
    speed_for_motor__kph = 0.0f;
  } else if (next_state == FORWARD) {
    speed_for_motor__kph += gain * error;
  } else if (next_state == BRAKING) {
    if (current_speed >= -0.1f && current_speed <= 0.1f) {
      speed_for_motor__kph = 0.0f;
      next_state = IDLE;
    } else {
      speed_for_motor__kph = -38.0f;
    }
  } else if (next_state == START_NEUTRAL_FOR_REVERSE) {
    speed_for_motor__kph = 0.0f;
  } else if (next_state == NEUTRAL_FOR_REVERSE) {
    speed_for_motor__kph = 0.0f;
  } else if (next_state == REVERSE) {
    speed_for_motor__kph += gain * error;
  }

  //---Propogate State Change---//
  current_state = next_state;
  //~~~~~~~~~~~~~~~~~~~~~END STATE MACHINE~~~~~~~~~~~~~~~~~~~~~//

  //---------Speed Limits and Watchdog Increments---------//
  if (speed_for_motor__kph >= 15.0f) {
    if (current_speed >= -0.1f && current_speed <= 0.1f) {
      watchdog_timer_speed++;
    }
    speed_for_motor__kph = 15.0f;
  } else if (speed_for_motor__kph <= -40.0f) {
    if (current_speed >= -0.1f && current_speed <= 0.1f) {
      watchdog_timer_speed++;
    }
    speed_for_motor__kph = -40.0f;
  }

  //------Check Watchdog Timer------//
  if (watchdog_timer_speed >= 20) {
    next_state = IDLE;
    watchdog_timer_speed = 0;
  }

  //----------Perform Motor and Steering Calculations to Duty Cycle----------//
  speed_for_motor__duty = calculate__kmh_to_duty(speed_for_motor__kph);
  angle_for_steering = calculate__radians_to_duty(target_steering__radians);

  //-------------Update PWM controls (influenced by SJ2 Button Settings)-------------//
  if (disable_motor && disable_steering) {
    pwm__send_idle_steering_and_motor();
    speed_for_motor__kph = 0.0f; // Important to reset this value so it doesn't keep counting up while motor operations are paused.
  } else if (disable_motor) {
    update_pwm_for_steering_and_motor(angle_for_steering, idle_duty_cycle);
    speed_for_motor__kph = 0.0f;
  } else if (disable_steering) {
    update_pwm_for_steering_and_motor(idle_duty_cycle, speed_for_motor__duty);
  } else {
    update_pwm_for_steering_and_motor(angle_for_steering, speed_for_motor__duty);
  }

  return executed;
}

ESC_CALIBRATION_STATUS pwm__esc_setup(uint32_t start_time, uint32_t current_time, ESC_CALIBRATION_STATUS esc) {

  if (current_time < 40 + start_time) { // Startup blinky LED 0 ~ 2 seconds / 15% duty to ESC
    pwm1__set_duty_cycle(PWM_motor, 15.0f);
    if (current_time % 2) {
      gpio__toggle(board_io__get_led0());
    }
  } else if (current_time < 60 + start_time) { // Intense blinky LED 0 ~ 2 seconds / 15% duty to ESC (still)
    gpio__toggle(board_io__get_led0());
  } else if (current_time < 80 + start_time) { // LED 1 on, LED 0 off ~ 2 seconds / 20% duty to ESC
    esc = CALIBRATION_IN_PROGRESS;
    pwm1__set_duty_cycle(PWM_motor, 20.0f);
    gpio__set(board_io__get_led0());
    gpio__reset(board_io__get_led1());
  } else if (current_time < 100 + start_time) { // LED 2 on, LED 1 off ~ 2 seconds / 10% duty to ESC
    pwm1__set_duty_cycle(PWM_motor, 10.0f);
    gpio__set(board_io__get_led1());
    gpio__reset(board_io__get_led2());
  } else if (current_time < 120 + start_time) { // LEDs 0 - 2 intense blinky ~ 2 seconds / 15% duty to ESC
    pwm1__set_duty_cycle(PWM_motor, 15.0f);
    gpio__toggle(board_io__get_led0());
    gpio__toggle(board_io__get_led1());
    gpio__toggle(board_io__get_led2());
  } else {
    gpio__set(board_io__get_led0());
    gpio__set(board_io__get_led1());
    gpio__set(board_io__get_led2());
    esc = CALIBRATION_FINISHED;
  }

  return esc;
}

void pwm__send_idle_steering_and_motor() {
  pwm1__set_duty_cycle(PWM_motor, 15.0f);
  pwm1__set_duty_cycle(PWM_steering, 15.0f);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
float pwm__debug_speed__kph() { return speed_for_motor__kph; }
float pwm__debug_speed__duty() { return speed_for_motor__duty; }
float pwm__debug_speed__target() { return speed_for_motor__tracking; }
float pwm__debug_integral_error() { return error; }
CAR_MOTION_STATUS pwm__debug_current_state() { return current_state; }
CAR_MOTION_STATUS pwm__debug_next_state() { return next_state; }

//~~~Helper Functions (Should be static? Produces warnings though)~~~//
void setup_gpio_for_PWM() {
  gpio__construct_with_function(2, 0, 1);
  gpio__construct_with_function(2, 1, 1);
}

void send_neutral_duty_signal_to_motor() {
  float steering_value__duty = calculate__radians_to_duty(0);
  pwm1__set_duty_cycle(PWM_steering, steering_value__duty);
  pwm1__set_duty_cycle(PWM_motor, 15.0f);
}

void update_pwm_for_steering_and_motor(float steering_value, float motor_value) {
  pwm1__set_duty_cycle(PWM_steering, steering_value);
  pwm1__set_duty_cycle(PWM_motor, motor_value);
}
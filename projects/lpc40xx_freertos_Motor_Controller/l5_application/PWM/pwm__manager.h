#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef enum {
  CALIBRATION_NOT_REQUIRED = 0,
  CALIBRATION_START = 1,
  CALIBRATION_IN_PROGRESS = 2,
  CALIBRATION_FINISHED = 3,
} ESC_CALIBRATION_STATUS;

typedef enum {
  IDLE = 0,
  FORWARD = 1,
  BRAKING = 2,
  START_NEUTRAL_FOR_REVERSE = 3,
  NEUTRAL_FOR_REVERSE = 4,
  REVERSE = 5,
} CAR_MOTION_STATUS;

void pwm__init();
bool set_steering_and_motor_state(float steering_value_radians, float motor_value_kph, uint32_t callback_time,
                                  bool disable_motor, bool disable_steering);

ESC_CALIBRATION_STATUS pwm__esc_setup(uint32_t start_time, uint32_t current_time, ESC_CALIBRATION_STATUS esc);
void pwm__send_idle_steering_and_motor();

//~~~~~~~~~~~~~~~Getters~~~~~~~~~~~~~~~//
float pwm__debug_speed__kph();
float pwm__debug_speed__duty();
float pwm__debug_speed__target();
float pwm__debug_integral_error();
CAR_MOTION_STATUS pwm__debug_current_state();
CAR_MOTION_STATUS pwm__debug_next_state();

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Helper Functions~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
void setup_gpio_for_PWM();
void set_neutral_duty_cycle_on_both_channels();
void send_neutral_duty_signal_to_motor();
void update_pwm_for_steering_and_motor(float steering_value, float motor_value);
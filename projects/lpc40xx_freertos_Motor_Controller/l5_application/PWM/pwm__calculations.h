#pragma once

float calculate__radians_to_duty(float steer_angle_in_radians);
float calculate__kmh_to_duty(float speed_in_kph);
#include "unity.h"

#include "pwm__calculations.h"

void test_calculate__kmh_to_duty__nominal(void) {
  TEST_ASSERT_GREATER_THAN(15, calculate__kmh_to_duty(10.0f));
  TEST_ASSERT_LESS_THAN(17, calculate__kmh_to_duty(10.0f));
}

// void test_calculate__kmh_to_duty__fail(void) {
//   TEST_ASSERT_EQUAL(15, calculate__kmh_to_duty(46.0f));
//   TEST_ASSERT_EQUAL(15, calculate__kmh_to_duty(-46.0f));
// }

void test_calculate__kmh_to_duty__conversion(void) {
  TEST_ASSERT_EQUAL(20.0f, calculate__kmh_to_duty(38.7f));
  TEST_ASSERT_EQUAL(18.0f, calculate__kmh_to_duty(20.7f));
  TEST_ASSERT_EQUAL(16.0f, calculate__kmh_to_duty(2.7f));
  TEST_ASSERT_EQUAL(15.0f, calculate__kmh_to_duty(0.0f));
  TEST_ASSERT_EQUAL(14.0f, calculate__kmh_to_duty(-2.7f));
  TEST_ASSERT_EQUAL(12.0f, calculate__kmh_to_duty(-20.7f));
  TEST_ASSERT_EQUAL(10.0f, calculate__kmh_to_duty(-38.7f));
}

void test_calculate__radians_to_duty_nominal(void) {
  TEST_ASSERT_GREATER_THAN(16, calculate__radians_to_duty(0.174f));
  TEST_ASSERT_LESS_THAN(18, calculate__radians_to_duty(0.174f));
}
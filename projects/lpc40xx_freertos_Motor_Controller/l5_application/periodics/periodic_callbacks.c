#include "periodic_callbacks.h"

#include "board_io.h"
#include "gpio.h"

#include "can__handler.h"
#include "led__manager.h"
#include "led__uart.h"
#include "pwm__manager.h"
#include "rpm__manager.h"

#include <stdio.h>

bool disable_motor;
bool disable_steering;
ESC_CALIBRATION_STATUS esc;
uint32_t esc_calibration_start_time;

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  disable_motor = true;
  disable_steering = true;
  esc = CALIBRATION_NOT_REQUIRED;

  can_handler__init();
  pwm__init();
  rpm__init();
  led__turn_off_all_leds();
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  // Line to stop auto-format
  can_handler__health_check__1hz();
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  can_handler__manage_mia__10hz();

  //~~~~~~~~~~~~~~~Switch controls~~~~~~~~~~~~~~~//
  if (gpio__get(board_io__get_sw0())) {
    esc = CALIBRATION_START;
    esc_calibration_start_time = callback_count;
  } else if (gpio__get(board_io__get_sw1())) {
    disable_motor = true;
  } else if (gpio__get(board_io__get_sw2())) {
    disable_steering = true;
  } else if (gpio__get(board_io__get_sw3())) {
    disable_steering = false;
    disable_motor = false;
  }

  if (esc == 1 || esc == 2) { // ESC command, can be called at any time during operation
    esc = pwm__esc_setup(esc_calibration_start_time, callback_count, esc);
    can_handler__transmit_messages__10hz(esc);
  } else if (callback_count < 30) { // Send idle signal for first 3 seconds
    send_neutral_duty_signal_to_motor();
  } else { // t > 3, handle can TX / RX and update speed values 2x a second
    if (callback_count % 2 == 0) {
      rpm__update_all_speeds();
    }
    can_handler__handle_all_incoming_messages__10hz(callback_count, disable_motor, disable_steering);
    can_handler__transmit_messages__10hz(esc);
  }
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  // Line to stop auto-format
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // Line to stop auto-format
}
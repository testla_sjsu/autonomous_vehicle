#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "board_io.h"
#include "esp32.h"
#include "esp32_task.h"
#include "gpio.h"
#include "uart3_init.h"

#include "testla_proto1.h"

#include "bridge.h"
#include "can_handler.h"
#include "sensor.h"

static const uart_e esp32__uart = UART__3;

static const char *hostname = "54.151.27.107"; // aws ec2
static uint16_t port = 8080;

static bridge__line_buffer_t response_buffer = {0};
static volatile bool esp32_ready = false;
static volatile bool tcp_socket_created = false;

static gpio_s wifi_reset_sw;
static uint16_t sw_debounce_state = 0;

// static dbc_DEBUG_GPS_CURRENT_LOCATION_s location;
// static bool new_location_received = false;

static bool wifi_reset_pressed() {
  bool isPressed = gpio__get(wifi_reset_sw);
  sw_debounce_state = (sw_debounce_state << 1) | isPressed | 0xE000;
  if (sw_debounce_state == 0xf000) {
    printf("network reset pressed\n");
    return true;
  } else {
    return false;
  }
}

static void callback__server_response(char *response, size_t length) {
  dbc_SENSOR_DESTINATION_LOCATION_s dest = {0};
  memset(response_buffer, '\0', sizeof(char) * BRIDGE_BUFFER_SIZE); // clear BUFFER
  memcpy(response_buffer, response, length);                        // copy new response to buffer
  char *message = strstr(response_buffer, "GPS_DEST:");             // move ptr to start of message
  if (message != NULL) {
    message += 9; // offset "GPS_DEST:" header
    char *ptr = strtok(message, ",");
    if (ptr != NULL) {
      dest.SENSOR_DESTINATION_latitude = (float)atof(ptr);
    }
    ptr = strtok(NULL, ",");
    if (ptr != NULL) {
      dest.SENSOR_DESTINATION_longitude = (float)atof(ptr);
    }
    if (dest.SENSOR_DESTINATION_latitude != 0 && dest.SENSOR_DESTINATION_longitude != 0) {
      can_handler__send_gps_destination(&dest);
    }
  }
}

static bool setup_esp32() {
  vTaskDelay(200);
  bool success = true;
  // Init ESP32
  esp32__init(esp32__uart);
  esp32__send_command("AT+RST");
  esp32__clear_receive_buffer(5000);
  esp32__register_response_callback(callback__server_response);
  // Test AT commands
  success &= esp32__wait_for_successful_command("AT", "OK", "Basic communication test");
  success &= esp32__wait_for_successful_command("ATE0", "OK", "Turn off echo");
  success &= esp32__wait_for_successful_command("AT+CIPMUX=0", "OK", "Single connection");
  // Enter "Station Mode"
  success &= esp32__wait_for_successful_command("AT+CWMODE=1", "OK", "Connect to WIFI");
  return success;
}

// static void bridge__send_sensors_to_server(dbc_SENSOR_SONARS_s sensors) {
//   bridge__line_buffer_t buffer = {0};
//   snprintf(buffer, sizeof(buffer), "SENSORS:%d,%d,%d,%d\r\n", sensors.SENSOR_SONARS_left,
//   sensors.SENSOR_SONARS_middle,
//            sensors.SENSOR_SONARS_right, sensors.SENSOR_SONARS_back);
//   esp32__cipsend(buffer, strlen(buffer));
// }

static bool bridge__compose_and_send_message(dbc_SENSOR_SONARS_s sensors, dbc_DEBUG_GPS_CURRENT_LOCATION_s location,
                                             dbc_GEO_STATUS_s geo, dbc_DRIVER_STEERING_s steer) {
  bridge__line_buffer_t buffer = {0};
  snprintf(buffer, sizeof(buffer), "SENSORS:%d,%d,%d,%d;LOCATION:%.8f,%.8f;HEADING:%d;STEERING:%.8f,%.8f\r\n",
           sensors.SENSOR_SONARS_left, sensors.SENSOR_SONARS_middle, sensors.SENSOR_SONARS_right,
           sensors.SENSOR_SONARS_back, location.DEBUG_GPS_CURRENT_LOCATION_latitude,
           location.DEBUG_GPS_CURRENT_LOCATION_longitude, geo.GEO_STATUS_compass_heading, steer.DRIVER_STEERING_yaw,
           steer.DRIVER_STEERING_velocity);
  bool result = esp32__cipsend(buffer, strlen(buffer));
  return result;
}

// static void bridge__send_location_to_server(dbc_DEBUG_GPS_CURRENT_LOCATION_s location) {
//   bridge__line_buffer_t buffer = {0};
//   snprintf(buffer, sizeof(buffer), "LOCATION:%.8f,%.8f\r\n", location.DEBUG_GPS_CURRENT_LOCATION_latitude,
//            location.DEBUG_GPS_CURRENT_LOCATION_longitude);
//   esp32__cipsend(buffer, strlen(buffer));
// }

// static void bridge__send_compass_heading_to_server(dbc_GEO_STATUS_s geo) {
//   bridge__line_buffer_t buffer = {0};
//   snprintf(buffer, sizeof(buffer), "HEADING:%d\r\n", geo.GEO_STATUS_compass_heading);
//   esp32__cipsend(buffer, strlen(buffer));
// }

void bridge__init(void) {}

// ARCHIVE: This logic has been migrated to Network-Handler Task
// void bridge__run_once(void) {
//   if (tcp_socket_created) {
//     dbc_SENSOR_SONARS_s sensor_data = sensor__get_sensor_data();
//     dbc_DEBUG_GPS_CURRENT_LOCATION_s location = can_handler__get_current_location();
//     dbc_GEO_STATUS_s geo = can_handler__get_current_geo_status();
//     bridge__compose_and_send_message(sensor_data, location, geo);
//   } else if (esp32_ready) {
//     tcp_socket_created = esp32__tcp_connect(hostname, port, 5000); // establish tcp/ip socket
//   }
// }

void bridge__network_handle_task(void) {
  gpio__reset(board_io__get_led0());
  gpio__reset(board_io__get_led1());
  uart3_init();
  esp32_ready = setup_esp32() && esp32__status_wifi_connected();
  uint8_t failures = 0;
  while (true) {
    if (!esp32_ready || wifi_reset_pressed()) {
      esp32_ready = setup_esp32() && esp32__status_wifi_connected();
    } else if (esp32_ready) {
      if (tcp_socket_created) {
        dbc_SENSOR_SONARS_s sensor_data = sensor__get_sensor_data();
        dbc_DEBUG_GPS_CURRENT_LOCATION_s location = can_handler__get_current_location();
        dbc_GEO_STATUS_s geo = can_handler__get_current_geo_status();
        dbc_DRIVER_STEERING_s steer = can_handler__get_driver_steering();
        failures = (bridge__compose_and_send_message(sensor_data, location, geo, steer)) ? 0 : (failures + 1);
        if (failures > 3) {
          esp32__tcp_close();
          esp32_ready = esp32__status_wifi_connected();
          tcp_socket_created = false;
        } else {
          vTaskDelay(200);
        }
      } else {
        tcp_socket_created = esp32__tcp_connect(hostname, port, 5000); // establish tcp/ip socket
        if (!tcp_socket_created) {
          esp32_ready = esp32__status_wifi_connected();
        }
      }
    }
    // LED-0: NETWORK-CONNECTED
    if (esp32_ready)
      gpio__toggle(board_io__get_led0());
    else
      gpio__reset(board_io__get_led0());
    // LED-1: TCP-ESTABLISHED
    if (tcp_socket_created)
      gpio__toggle(board_io__get_led1());
    else
      gpio__reset(board_io__get_led1());
  }
}
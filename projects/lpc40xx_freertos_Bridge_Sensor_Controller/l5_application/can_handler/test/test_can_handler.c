#include <stdio.h>
#include <string.h>

#include "unity.h"

#include "Mockcan_bus.h"

#include "can_handler.h"

void setUp(void) {}

void tearDown(void) {}

void test__can_handler__init(void) {
  can__init_ExpectAnyArgsAndReturn(true);
  can__bypass_filter_accept_all_msgs_Expect();
  can__reset_bus_ExpectAnyArgs();

  can_handler__init();
}

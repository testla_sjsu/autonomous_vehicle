#include "can_bus.h"
#include "testla_proto1.h"
#include <stdint.h>
#include <stdio.h>

#include "can_handler.h"

static dbc_DEBUG_GPS_CURRENT_LOCATION_s can_msg__current_location = {0};
static dbc_GEO_STATUS_s can_msg__current_geo_status = {0};
static dbc_DRIVER_STEERING_s can_msg__driver_steering = {0};

static can__num_e can_option = can1;
static uint32_t timeout_ms = 10;
static uint32_t can_baudrate_kbps = 500;
static uint32_t can_rxq_size = 1024;
static uint32_t can_txq_size = 1024;

static bool new_location_available = false;
static bool new_geo_status_available = false;
static bool new_driver_steering_available = false;

static void callback_BusOFF(uint32_t parameter1) {
  // simply reset bus for now
  can__reset_bus(can_option);
}
static void callback_OverRun(uint32_t parameter1) {
  // simply reset bus for now
  can__reset_bus(can_option);
}

bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc) {
  can__msg_t tx_msg = {0};
  tx_msg.msg_id = message_id;
  tx_msg.frame_fields.data_len = dlc;
  for (int i = 0; i < 8; i++) {
    tx_msg.data.bytes[i] = bytes[i];
  }
  return can__tx(can_option, &tx_msg, timeout_ms);
}

void can_handler__init(void) {
  can__init(can_option, can_baudrate_kbps, can_rxq_size, can_txq_size, callback_BusOFF, callback_OverRun);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can_option);
}

void can_handler__handle_all_incoming_messages(void) {
  can__msg_t rx_msg = {};

  while (can__rx(can_option, &rx_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = rx_msg.msg_id,
        .message_dlc = rx_msg.frame_fields.data_len,
    };

    if (dbc_decode_DEBUG_GPS_CURRENT_LOCATION(&can_msg__current_location, header, rx_msg.data.bytes)) {
      new_location_available = true;
    } else if (dbc_decode_GEO_STATUS(&can_msg__current_geo_status, header, rx_msg.data.bytes)) {
      new_geo_status_available = true;
    } else if (dbc_decode_DRIVER_STEERING(&can_msg__driver_steering, header, rx_msg.data.bytes)) {
      new_driver_steering_available = true;
    }
  }
}

bool can_handler__new_geo_status_available() { return new_geo_status_available; }
bool can_handler__new_location_available() { return new_location_available; }
bool can_handler__driver_steering_available() { return new_driver_steering_available; }

dbc_DEBUG_GPS_CURRENT_LOCATION_s can_handler__get_current_location() { return can_msg__current_location; }
dbc_GEO_STATUS_s can_handler__get_current_geo_status() { return can_msg__current_geo_status; }
dbc_DRIVER_STEERING_s can_handler__get_driver_steering() { return can_msg__driver_steering; }

bool can_handler__send_sensor_update(dbc_SENSOR_SONARS_s *sensor_data) {
  // printf("sensor data:\n        left: %d\nback: %d      middle: %d\nright: %d\n\n", sensor_data->SENSOR_SONARS_left,
  //        sensor_data->SENSOR_SONARS_back, sensor_data->SENSOR_SONARS_middle, sensor_data->SENSOR_SONARS_right);
  return dbc_encode_and_send_SENSOR_SONARS(NULL, sensor_data);
}

bool can_handler__send_gps_destination(dbc_SENSOR_DESTINATION_LOCATION_s *gps_data) {
  // printf("gps data: (%d,%d)\n\n", gps_data->SENSOR_DESTINATION_latitude, gps_data->SENSOR_DESTINATION_longitude);
  return dbc_encode_and_send_SENSOR_DESTINATION_LOCATION(NULL, gps_data);
}
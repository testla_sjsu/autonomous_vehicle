#include "can__handler.h"
#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"
#include "testla_proto1.h"
#include <stdio.h>

void can_handler__init(void) {
  if (!can__init(can1, 100, 1024, 1024, NULL, NULL)) {
    fprintf(stderr, "failed to init CAN\n");
  } else {
    fprintf(stderr, "can running\n");
  }
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can1);
}

void can_handler__transmit_messages_10hz(void) {
  bool switch_zero = gpio__get(board_io__get_sw0());
  bool switch_one = gpio__get(board_io__get_sw1());
  bool switch_two = gpio__get(board_io__get_sw2());

  dbc_DRIVER_STEERING_s driver_steering_message = {};

  if (switch_zero) {
    gpio__reset(board_io__get_led0());
    driver_steering_message.DRIVER_STEERING_yaw = 0.52359877559f;
    driver_steering_message.DRIVER_STEERING_velocity = 0.0f;
  } else if (switch_one) {
    gpio__reset(board_io__get_led1());
    driver_steering_message.DRIVER_STEERING_yaw = -0.52359877559f;
    driver_steering_message.DRIVER_STEERING_velocity = 0.0f;
  } else if (switch_two) {
    gpio__reset(board_io__get_led2());
    driver_steering_message.DRIVER_STEERING_yaw = 0.0f;
    driver_steering_message.DRIVER_STEERING_velocity = 10.0f;
  } else {
    gpio__set(board_io__get_led0());
    gpio__set(board_io__get_led1());
    gpio__set(board_io__get_led2());
    driver_steering_message.DRIVER_STEERING_yaw = 0.0f;
    driver_steering_message.DRIVER_STEERING_velocity = 0.0f;
  }

  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_DRIVER_STEERING(can_msg.data.bytes, &driver_steering_message);

  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);
}

void can_handler__health_check(void) {
  if (can__is_bus_off(can1)) {
    fprintf(stderr, "RESET CAN\n");
    can__reset_bus(can1);
  }
}
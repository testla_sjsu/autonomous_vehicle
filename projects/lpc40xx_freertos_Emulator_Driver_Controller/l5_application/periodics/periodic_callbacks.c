#include "periodic_callbacks.h"

#include "board_io.h"
#include "gpio.h"

#include "can_handler.h"
#include "pwm__manager.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // Line to stop auto-format
  can_handler__init();
  gpio__set(board_io__get_led0());
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
  gpio__set(board_io__get_led3());
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  // Line to stop auto-format
  can_handler__health_check();
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  // Line to stop auto-format
  can_handler__transmit_messages_10hz();
}

void periodic_callbacks__100Hz(uint32_t callback_count) {
  // Line to stop auto-format
  //
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  // Line to stop auto-format
  //
}
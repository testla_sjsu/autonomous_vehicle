#include "Mockcompass.h"
#include "Mockff.h"
#include "Mockgps.h"
#include "geo.h"
#include "unity.h"

typedef struct {
  float dest_lat;
  float dest_lon;
  float current_lat;
  float current_lon;
  uint16_t expected_distance;
  uint16_t expected_bearing;
} geo_test_data_s;

static const geo_test_data_s test_data[] = {
    {0, 0, 0, 0, 0, 0},

    {37.4415854965695, -121.89012355014361, 37.43449484173257, -121.88849276703408, 801, 349},  // North
    {37.43722094226244, -121.87681979319741, 37.43449484173257, -121.88849276703408, 1073, 73}, // East
    {37.42767915599625, -121.88488787805511, 37.43449484173257, -121.88849276703408, 821, 157}, // South
    {37.43285913375272, -121.89664668258176, 37.43449484173257, -121.88849276703408, 742, 255}, // West
};

void test_geo__init(void) {
  gps__init_Expect();
  compass__init_Expect();
  FRESULT result = FR_DISK_ERR;
  f_open_ExpectAndReturn(NULL, NULL, 0, result);
  f_open_IgnoreArg_fp();
  f_open_IgnoreArg_path();
  f_open_IgnoreArg_mode();

  geo__init();
}

void test_geo__get_status_no_messages(void) {
  gps__coordinates_t gps_dummy = {0};
  gps__get_coordinates_ExpectAndReturn(gps_dummy);

  compass__get_heading_ExpectAndReturn(0);

  dbc_GEO_STATUS_s status = {
      .GEO_STATUS_destination_distance = 5,
      .GEO_STATUS_destination_bearing = 5,
      .GEO_STATUS_compass_heading = 5,
  };
  geo__get_status(&status);

  TEST_ASSERT_EQUAL(0, status.GEO_STATUS_destination_distance);
  TEST_ASSERT_EQUAL(0, status.GEO_STATUS_destination_bearing);
  TEST_ASSERT_EQUAL(0, status.GEO_STATUS_compass_heading);
}

void test_geo__get_status_test_data(void) {
  for (uint16_t i = 0; i < sizeof(test_data) / sizeof(test_data[0]); i++) {
    dbc_SENSOR_DESTINATION_LOCATION_s destination = {
        .SENSOR_DESTINATION_latitude = test_data[i].dest_lat,
        .SENSOR_DESTINATION_longitude = test_data[i].dest_lon,
    };
    geo__update_dest(&destination);
    gps__coordinates_t gps_dummy = {.lat_rad = test_data[i].current_lat, .lon_rad = test_data[i].current_lon};

    gps__get_coordinates_ExpectAndReturn(gps_dummy);

    uint16_t test_heading = 10;
    compass__get_heading_ExpectAndReturn(test_heading);

    dbc_GEO_STATUS_s status = {
        .GEO_STATUS_destination_distance = 5,
        .GEO_STATUS_destination_bearing = 5,
        .GEO_STATUS_compass_heading = 0,
    };

    geo__get_status(&status);

    TEST_ASSERT_EQUAL(test_data[i].expected_distance, (uint16_t)status.GEO_STATUS_destination_distance);
    TEST_ASSERT_EQUAL(test_data[i].expected_bearing, status.GEO_STATUS_destination_bearing);
    TEST_ASSERT_EQUAL(test_heading, status.GEO_STATUS_compass_heading);
  }
}
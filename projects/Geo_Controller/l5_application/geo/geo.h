#pragma once
#include "compass.h"
#include "gps.h"
#include "testla_proto1.h"

void geo__init(void);
void geo__get_status(dbc_GEO_STATUS_s *msg);
void geo__update_dest(dbc_SENSOR_DESTINATION_LOCATION_s *msg);
void geo__save_location_to_file(void);

// helper functions
float geo_helper__convert_from_degrees_to_radians(float degrees);
float geo_helper__find_distance_between_two_sets_of_coordinates_in_radians(gps__coordinates_t gps_current,
                                                                           gps__coordinates_t gps_destination);
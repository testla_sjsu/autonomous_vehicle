#include "geo.h"
#include "ff.h"
#include <math.h>
#include <stdio.h>

static const float PI = 3.14159265358979323846; // TODO: why is M_PI not found?

static gps__coordinates_t gps_dest;
const uint32_t EARTH_RADIUS_KM = 6371e3;

static void print_heading(void) {
  const char *filename = "geo_log.txt";
  FIL file;
  UINT bytes_written = 0;
  FRESULT result = f_open(&file, filename, (FA_WRITE | FA_OPEN_APPEND));

  if (result == FR_OK) {
    char separator[] = "\n=====================================================================\n";
    FRESULT eof = f_lseek(&file, f_size(&file));

    if (eof == FR_OK) {
      FRESULT write_result = f_write(&file, separator, strlen(separator), &bytes_written);
      if (write_result != FR_OK) {
        printf("write fail\n");
      }
    }
    f_close(&file);
  }
}

void geo__init(void) {
  // initialize gps module
  gps__init();
  compass__init();
  print_heading();
}

void geo__get_status(dbc_GEO_STATUS_s *msg) {
  gps__coordinates_t gps_current = gps__get_coordinates();
  // read data out of the gps module and use dest data to generate heading, bearing, distance
  gps_current.lat_rad *= (PI / 180);
  gps_current.lon_rad *= (PI / 180);

  float lat_delta = gps_dest.lat_rad - gps_current.lat_rad;
  float lon_delta = gps_dest.lon_rad - gps_current.lon_rad;

  // ref: https://www.movable-type.co.uk/scripts/latlong.html
  // distance using two points
  float a = pow(sin(lat_delta / 2), 2) + cos(gps_current.lat_rad) * cos(gps_dest.lat_rad) * pow(sin(lon_delta / 2), 2);
  float c = 2 * atan2(sqrt(a), sqrt(1 - a));
  float distance = EARTH_RADIUS_KM * c;

  // bearing using two points
  float y = sin(lon_delta) * cos(gps_dest.lat_rad);
  float x = (cos(gps_current.lat_rad) * sin(gps_dest.lat_rad)) -
            sin(gps_current.lat_rad) * cos(gps_dest.lat_rad) * cos(lon_delta);
  float theta = atan2(y, x);
  uint16_t bearing = (uint16_t)(theta * 180 / PI + 360) % 360;

  msg->GEO_STATUS_destination_distance = distance;
  msg->GEO_STATUS_destination_bearing = bearing;
  msg->GEO_STATUS_compass_heading = compass__get_heading();
}

void geo__update_dest(dbc_SENSOR_DESTINATION_LOCATION_s *msg) {
  // read in the dest data and save it for later
  gps_dest.lat_rad = msg->SENSOR_DESTINATION_latitude * (PI / 180);
  gps_dest.lon_rad = msg->SENSOR_DESTINATION_longitude * (PI / 180);
}

void geo__save_location_to_file(void) {
  const char *filename = "geo_log.txt";
  FIL file;
  UINT bytes_written = 0;
  FRESULT result = f_open(&file, filename, (FA_WRITE | FA_OPEN_APPEND));
  if (result == FR_OK) {
    char geo_string[200];
    gps__coordinates_t gps_current = gps__get_coordinates();
    uint8_t fix = gps__get_fix_status();
    sprintf(geo_string, "%d,%f,%f\n", fix, (double)gps_current.lat_rad, (double)gps_current.lon_rad);
    FRESULT eof = f_lseek(&file, f_size(&file));
    if (eof == FR_OK) {
      FRESULT write_result = f_write(&file, geo_string, strlen(geo_string), &bytes_written);
      if (write_result != FR_OK) {
        printf("write fail\n");
      }
    }
    f_close(&file);
  }
}

// helper functions

float geo_helper__convert_from_degrees_to_radians(float degrees) { return degrees * (PI / 180); }

float geo_helper__find_distance_between_two_sets_of_coordinates_in_radians(gps__coordinates_t gps_current,
                                                                           gps__coordinates_t gps_destination) {
  float lat_delta = gps_destination.lat_rad - gps_current.lat_rad;
  float lon_delta = gps_destination.lon_rad - gps_current.lon_rad;

  // ref: https://www.movable-type.co.uk/scripts/latlong.html
  // distance using two points
  float a =
      pow(sin(lat_delta / 2), 2) + cos(gps_current.lat_rad) * cos(gps_destination.lat_rad) * pow(sin(lon_delta / 2), 2);
  float c = 2 * atan2(sqrt(a), sqrt(1 - a));
  float distance = EARTH_RADIUS_KM * c;
  return distance;
}
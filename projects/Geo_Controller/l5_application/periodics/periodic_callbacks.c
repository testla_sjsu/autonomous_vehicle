#include "periodic_callbacks.h"

#include "board_io.h"
#include "can_handler.h"
#include "compass.h"
#include "geo.h"
#include "gpio.h"
#include "gps.h"
#include "navigation.h"
#include "waypoints_graph.h"

static uint8_t state;

#define STATE_SETUP 0
#define STATE_RUN 1

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  can_handler__init();
  geo__init();
  waypoints__init();
  state = STATE_SETUP;
}

void periodic_callbacks__1Hz(uint32_t callback_count) {}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led0());
  if (gpio__get(board_io__get_sw0())) {
    compass__update_offset();
  }

  switch (state) {
  case STATE_SETUP: {
    if (callback_count % 10 == 0) {
      if (gps__send_setup()) {
        state = STATE_RUN;
      }
    }
  } break;

  case STATE_RUN: {
    if (callback_count % 10 == 0) {
      can_handler__health_check();
      can_handler__transmit_debug_messages_1Hz();
      uint16_t heading = compass__get_heading();
      printf("heading: %d", heading);
    }
    can_handler__handle_all_incoming_messages();
    can_handler__manage_mia_10Hz();
    gps__run_once(callback_count);
    compass__update_heading_10Hz();
    can_handler__transmit_status_10Hz();
    if (gpio__get(board_io__get_sw3())) {
      geo__save_location_to_file();
    }
  } break;

  default:
    break;
  }
}

void periodic_callbacks__100Hz(uint32_t callback_count) {}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {}
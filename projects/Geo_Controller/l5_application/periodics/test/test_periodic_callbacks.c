#include <stdio.h>
#include <string.h>

#include "Mockcan_handler.h"
#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

// Include the source we wish to test
#include "Mockcompass.h"
#include "Mockgeo.h"
#include "Mockgps.h"
#include "Mocknavigation.h"
#include "Mockwaypoints_graph.h"
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_handler__init_Expect();
  geo__init_Expect();
  waypoints__init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {}

void test_periodic_callbacks__10Hz(void) {
  uint32_t num_setup_states = 4;
  for (uint32_t i = 0; i < ((num_setup_states - 1) * 10) + 1; i++) {
    gpio_s gpio = {};
    board_io__get_led0_ExpectAndReturn(gpio);
    gpio__toggle_Expect(gpio);
    board_io__get_sw0_ExpectAndReturn(gpio);
    gpio__get_ExpectAndReturn(gpio, false);

    if (i % 10 == 0) {
      gps__send_setup_ExpectAndReturn((i / 10) == (num_setup_states - 1));
    }
    periodic_callbacks__10Hz(i);
  }

  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);
  board_io__get_sw0_ExpectAndReturn(gpio);
  gpio__get_ExpectAndReturn(gpio, false);

  can_handler__handle_all_incoming_messages_Expect();
  can_handler__manage_mia_10Hz_Expect();
  gps__run_once_Expect(1);
  compass__update_heading_10Hz_Expect();
  can_handler__transmit_status_10Hz_Expect();
  board_io__get_sw3_ExpectAndReturn(gpio);
  gpio__get_ExpectAndReturn(gpio, false);
  periodic_callbacks__10Hz(1);
}
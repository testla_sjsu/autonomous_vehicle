#include "waypoints_graph.h"

// get rid of these declarations and put the only instance of this declaration somewhere in geo helper
// static const float PI = 3.14159265358979323846; // TODO: why is M_PI not found?

/*********************************************************************************************************************
 * Value Definitions for Graph Functionality
 ********************************************************************************************************************/
// Weights for assigning to the nodes
const uint8_t node_weight_danger_zone_offset = 255;         // gets assigned with our hardcoding
const uint8_t node_weight_parking_garage_ramp_offset = 255; // gets assigned with our hardcoding
const uint8_t node_weight_normal_offset = 0;                // gets assigned at initialization of array of waypoints
const uint8_t node_weight_perimeter_offset = 5;             // gets assigned at initialization of array of waypoints
const uint8_t node_weight_corner_offset = 6;                // gets assigned at initialization of array of waypoints
const uint8_t node_weight_concrete_pole_nearby_offset = 20; // gets assigned with our hardcoding

// Distance values
// to use when no eligible calculation can be made using the Haversine formula when no valid neighbor
const float distance_neighbor_node_doe_not_exist = 9999.99;     // NOTE: Dijktras technically uses a weight of 0
const float distance_neighbor_node_deemed_unvisitable = 1000.0; // Update the distance of

// Weights for edges // TODO: consider removing this if using distance values for edge weights
const uint8_t edge_weight_no_eligible_neighbor = 255; // for edges that are impossible
const uint8_t edge_weight_diagonal_edge_node = 17;    // for edges labled NW, NE, SE, SW of a node
const uint8_t edge_weight_square_edge_node = 12;      // for edges labled N, E, S, W of a node
// 5√2 = 7.07106781187
// 12√2 = 16.9705627485 diagonal of square of 12 weight

/*********************************************************************************************************************
 * Helper Functions
 ********************************************************************************************************************/
static uint8_t assign_weight_to_edge_helper(neighbors_e neigbor_direction);

static float find_distance_to_neighbor_helper(neighbors_e neigbor_direction, uint8_t row, uint8_t col);

/*********************************************************************************************************************
 * Data used by this file
 ********************************************************************************************************************/

// float waypoints__latitude_in_radians[num_row_waypoint][num_col_waypoint];
// float waypoints__longitude_in_radians[num_row_waypoint][num_col_waypoint];

static float waypoints__latitude[num_row_waypoint][num_col_waypoint] = {
    {37.339549, 37.339585, 37.339621, 37.339656, 37.339692, 37.339728, 37.339764, 37.339799, 37.339835},
    {37.339492, 37.339527, 37.339563, 37.339598, 37.339634, 37.339669, 37.339705, 37.339741, 37.339776},
    {37.339434, 37.339470, 37.339505, 37.339540, 37.339576, 37.339611, 37.339646, 37.339682, 37.339717},
    {37.339377, 37.339412, 37.339447, 37.339482, 37.339518, 37.339553, 37.339588, 37.339623, 37.339658},
    {37.339320, 37.339355, 37.339390, 37.339425, 37.339460, 37.339494, 37.339529, 37.339564, 37.339599},
    {37.339262, 37.339297, 37.339332, 37.339367, 37.339401, 37.339436, 37.339471, 37.339506, 37.339540},
    {37.339205, 37.339240, 37.339274, 37.339309, 37.339343, 37.339378, 37.339412, 37.339447, 37.339481},
    {37.339148, 37.339182, 37.339216, 37.339251, 37.339285, 37.339319, 37.339354, 37.339388, 37.339422},
    {37.339091, 37.339125, 37.339159, 37.339193, 37.339227, 37.339261, 37.339295, 37.339329, 37.339364},
    {37.339033, 37.339067, 37.339101, 37.339135, 37.339169, 37.339203, 37.339237, 37.339271, 37.339305},
    {37.338976, 37.339010, 37.339043, 37.339077, 37.339111, 37.339144, 37.339178, 37.339212, 37.339246},
    {37.338919, 37.338952, 37.338986, 37.339019, 37.339053, 37.339086, 37.339120, 37.339153, 37.339187},
    {37.338861, 37.338895, 37.338928, 37.338961, 37.338995, 37.339028, 37.339061, 37.339094, 37.339128},
    {37.338804, 37.338837, 37.338870, 37.338903, 37.338936, 37.338969, 37.339003, 37.339036, 37.339069},
    {37.338747, 37.338780, 37.338812, 37.338845, 37.338878, 37.338911, 37.338944, 37.338977, 37.339010},
    {37.338689, 37.338722, 37.338755, 37.338787, 37.338820, 37.338853, 37.338886, 37.338918, 37.338951},
    {37.338632, 37.338665, 37.338697, 37.338730, 37.338762, 37.338795, 37.338827, 37.338860, 37.338892}};

static float waypoints__longitude[num_row_waypoint][num_col_waypoint] = {
    {-121.881423, -121.881344, -121.881265, -121.881186, -121.881107, -121.881027, -121.880948, -121.880869,
     -121.880790},
    {-121.881378, -121.881299, -121.881220, -121.881142, -121.881063, -121.880984, -121.880905, -121.880826,
     -121.880747},
    {-121.881333, -121.881255, -121.881176, -121.881098, -121.881019, -121.880940, -121.880862, -121.880783,
     -121.880705},
    {-121.881288, -121.881210, -121.881132, -121.881053, -121.880975, -121.880897, -121.880819, -121.880741,
     -121.880662},
    {-121.881243, -121.881165, -121.881087, -121.881009, -121.880932, -121.880854, -121.880776, -121.880698,
     -121.880620},
    {-121.881198, -121.881121, -121.881043, -121.880965, -121.880888, -121.880810, -121.880732, -121.880655,
     -121.880577},
    {-121.881153, -121.881076, -121.880999, -121.880921, -121.880844, -121.880767, -121.880689, -121.880612,
     -121.880535},
    {-121.881108, -121.881031, -121.880954, -121.880877, -121.880800, -121.880723, -121.880646, -121.880569,
     -121.880492},
    {-121.881064, -121.880987, -121.880910, -121.880833, -121.880757, -121.880680, -121.880603, -121.880526,
     -121.880450},
    {-121.881019, -121.880942, -121.880866, -121.880789, -121.880713, -121.880636, -121.880560, -121.880483,
     -121.880407},
    {-121.880974, -121.880897, -121.880821, -121.880745, -121.880669, -121.880593, -121.880517, -121.880441,
     -121.880364},
    {-121.880929, -121.880853, -121.880777, -121.880701, -121.880625, -121.880549, -121.880474, -121.880398,
     -121.880322},
    {-121.880884, -121.880808, -121.880733, -121.880657, -121.880582, -121.880506, -121.880430, -121.880355,
     -121.880279},
    {-121.880839, -121.880764, -121.880688, -121.880613, -121.880538, -121.880462, -121.880387, -121.880312,
     -121.880237},
    {-121.880794, -121.880719, -121.880644, -121.880569, -121.880494, -121.880419, -121.880344, -121.880269,
     -121.880194},
    {-121.880749, -121.880674, -121.880600, -121.880525, -121.880450, -121.880376, -121.880301, -121.880226,
     -121.880152},
    {-121.880704, -121.880630, -121.880555, -121.880481, -121.880407, -121.880332, -121.880258, -121.880183,
     -121.880109}};

static float north_garage_ramp_latitude_points[4] = {37.339223, 37.339274, 37.338986, 37.338924};

static float north_garage_ramp_longitude_points[4] = {-121.88119, -121.881058, -121.880844, -121.880972};

static float north_garage_boundaries_latitude_points[4] = {37.339549, 37.339835, 37.338892, 37.338632};

static float north_garage_boundaries_longitude_points[4] = {-121.881423, -121.88079, -121.880109, -121.880704};

static four_side_polygon_s polygon__garage_ramp;

static four_side_polygon_s polygon__garage_extreme_boundaries;

// Functions in this section are done
/*********************************************************************************************************************
 * Functions that help initialize graph
 ********************************************************************************************************************/
void waypoints__init(void) { waypoints__initialize_all_waypoint_data(); }

void waypoints__initialize_all_waypoint_data(void) {
  waypoints__initialize_coordinates_of_all_nodes();
  // waypoints__convert_all_coordinates_from_degrees_to_radians();
  waypoints__initialize_coordinates_in_radians_of_all_nodes();

  waypoints__initialize_distance_between_nodes();

  // Init nodes
  waypoints__initialize_weight_of_nodes();

  // Replace bad nodes
  waypoints__find_and_replace_all_bad_nodes_in_all_polygons();

  // Calc weight of edges
  waypoints__initialize_weight_of_edges();
}

void waypoints__initialize_coordinates_of_all_nodes(void) {
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      all_waypoints[row][col].latitude = waypoints__latitude[row][col];
      all_waypoints[row][col].longitude = waypoints__longitude[row][col];
    }
  }
}

void waypoints__initialize_coordinates_in_radians_of_all_nodes(void) {
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      all_waypoints[row][col].coordinates_in_radians.lat_rad =
          geo_helper__convert_from_degrees_to_radians(waypoints__latitude[row][col]);
      all_waypoints[row][col].coordinates_in_radians.lon_rad =
          geo_helper__convert_from_degrees_to_radians(waypoints__longitude[row][col]);
    }
  }
}

void waypoints__initialize_row_and_col_indices(void) {
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      all_waypoints[row][col].row_idx_of_waypoint_graph = row;
      all_waypoints[row][col].col_idx_of_waypoint_graph = col;
    }
  }
}

void waypoints__initialize_weight_of_nodes(void) {
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      if (row == 0) {
        if (col == 0) {
          all_waypoints[row][col].weight_of_this_node = node_weight_corner_offset;
        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].weight_of_this_node = node_weight_perimeter_offset;
        } else {
          all_waypoints[row][col].weight_of_this_node = node_weight_corner_offset;
        }
      } else if (row == (num_row_waypoint - 1)) {
        if (col == 0) {
          all_waypoints[row][col].weight_of_this_node = node_weight_corner_offset;
        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].weight_of_this_node = node_weight_perimeter_offset;
        } else {
          all_waypoints[row][col].weight_of_this_node = node_weight_corner_offset;
        }
      } else {
        if (col == 0) {
          all_waypoints[row][col].weight_of_this_node = node_weight_perimeter_offset;
        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].weight_of_this_node = node_weight_perimeter_offset;
        } else { // all other spaces are filled with normal weights for neighbor type
          all_waypoints[row][col].weight_of_this_node = node_weight_normal_offset;
        }
      }
    }
  }
}

void waypoints__initialize_distance_between_nodes(void) {
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      if (row == 0) {

        if (col == 0) {
          all_waypoints[row][col].distance_neighbor_SW = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_W = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_NW = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_N = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_NE = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_E = find_distance_to_neighbor_helper(neighbor_e, row, col);
          all_waypoints[row][col].distance_neighbor_SE = find_distance_to_neighbor_helper(neighbor_se, row, col);
          all_waypoints[row][col].distance_neighbor_S = find_distance_to_neighbor_helper(neighbor_s, row, col);

        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].distance_neighbor_NW = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_N = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_NE = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_E = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_SE = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_S = find_distance_to_neighbor_helper(neighbor_s, row, col);
          all_waypoints[row][col].distance_neighbor_SW = find_distance_to_neighbor_helper(neighbor_sw, row, col);
          all_waypoints[row][col].distance_neighbor_W = find_distance_to_neighbor_helper(neighbor_w, row, col);
        } else {
          all_waypoints[row][col].distance_neighbor_NW = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_N = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_NE = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_E = find_distance_to_neighbor_helper(neighbor_e, row, col);
          all_waypoints[row][col].distance_neighbor_SE = find_distance_to_neighbor_helper(neighbor_se, row, col);
          all_waypoints[row][col].distance_neighbor_S = find_distance_to_neighbor_helper(neighbor_s, row, col);
          all_waypoints[row][col].distance_neighbor_SW = find_distance_to_neighbor_helper(neighbor_sw, row, col);
          all_waypoints[row][col].distance_neighbor_W = find_distance_to_neighbor_helper(neighbor_w, row, col);
        }
      } else if (row == (num_row_waypoint - 1)) {

        if (col == 0) {
          all_waypoints[row][col].distance_neighbor_SE = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_S = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_SW = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_W = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_NW = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_N = find_distance_to_neighbor_helper(neighbor_n, row, col);
          all_waypoints[row][col].distance_neighbor_NE = find_distance_to_neighbor_helper(neighbor_ne, row, col);
          all_waypoints[row][col].distance_neighbor_E = find_distance_to_neighbor_helper(neighbor_e, row, col);

        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].distance_neighbor_NE = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_E = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_SE = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_S = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_SW = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_W = find_distance_to_neighbor_helper(neighbor_w, row, col);
          all_waypoints[row][col].distance_neighbor_NW = find_distance_to_neighbor_helper(neighbor_nw, row, col);
          all_waypoints[row][col].distance_neighbor_N = find_distance_to_neighbor_helper(neighbor_n, row, col);

        } else {
          all_waypoints[row][col].distance_neighbor_SE = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_S = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_SW = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_W = find_distance_to_neighbor_helper(neighbor_w, row, col);
          all_waypoints[row][col].distance_neighbor_NW = find_distance_to_neighbor_helper(neighbor_nw, row, col);
          all_waypoints[row][col].distance_neighbor_N = find_distance_to_neighbor_helper(neighbor_n, row, col);
          all_waypoints[row][col].distance_neighbor_NE = find_distance_to_neighbor_helper(neighbor_ne, row, col);
          all_waypoints[row][col].distance_neighbor_E = find_distance_to_neighbor_helper(neighbor_e, row, col);
        }
      } else {

        if (col == 0) {
          all_waypoints[row][col].distance_neighbor_SW = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_W = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_NW = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_N = find_distance_to_neighbor_helper(neighbor_n, row, col);
          all_waypoints[row][col].distance_neighbor_NE = find_distance_to_neighbor_helper(neighbor_ne, row, col);
          all_waypoints[row][col].distance_neighbor_E = find_distance_to_neighbor_helper(neighbor_e, row, col);
          all_waypoints[row][col].distance_neighbor_SE = find_distance_to_neighbor_helper(neighbor_se, row, col);
          all_waypoints[row][col].distance_neighbor_S = find_distance_to_neighbor_helper(neighbor_s, row, col);

        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].distance_neighbor_NE = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_E = distance_neighbor_node_doe_not_exist;
          all_waypoints[row][col].distance_neighbor_SE = distance_neighbor_node_doe_not_exist;

          all_waypoints[row][col].distance_neighbor_S = find_distance_to_neighbor_helper(neighbor_s, row, col);
          all_waypoints[row][col].distance_neighbor_SW = find_distance_to_neighbor_helper(neighbor_sw, row, col);
          all_waypoints[row][col].distance_neighbor_W = find_distance_to_neighbor_helper(neighbor_w, row, col);
          all_waypoints[row][col].distance_neighbor_NW = find_distance_to_neighbor_helper(neighbor_nw, row, col);
          all_waypoints[row][col].distance_neighbor_N = find_distance_to_neighbor_helper(neighbor_n, row, col);

        } else { // all other spaces are filled with normal weights for neighbor type
          all_waypoints[row][col].distance_neighbor_N = find_distance_to_neighbor_helper(neighbor_n, row, col);
          all_waypoints[row][col].distance_neighbor_NE = find_distance_to_neighbor_helper(neighbor_ne, row, col);
          all_waypoints[row][col].distance_neighbor_E = find_distance_to_neighbor_helper(neighbor_e, row, col);
          all_waypoints[row][col].distance_neighbor_SE = find_distance_to_neighbor_helper(neighbor_se, row, col);
          all_waypoints[row][col].distance_neighbor_S = find_distance_to_neighbor_helper(neighbor_s, row, col);
          all_waypoints[row][col].distance_neighbor_SW = find_distance_to_neighbor_helper(neighbor_sw, row, col);
          all_waypoints[row][col].distance_neighbor_W = find_distance_to_neighbor_helper(neighbor_w, row, col);
          all_waypoints[row][col].distance_neighbor_NW = find_distance_to_neighbor_helper(neighbor_nw, row, col);
        }
      }
    }
  }
}

// TODO: think about if this is really a section that will be needing to be refactored based on distance and weight of
// nodes or not
void waypoints__initialize_weight_of_edges(void) {
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      if (row == 0) {
        if (col == 0) {
          all_waypoints[row][col].weight_neighbor_SW = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_W = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_NW = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_N = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_NE = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_E = assign_weight_to_edge_helper(neighbor_e);
          all_waypoints[row][col].weight_neighbor_SE = assign_weight_to_edge_helper(neighbor_se);
          all_waypoints[row][col].weight_neighbor_S = assign_weight_to_edge_helper(neighbor_s);

        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].weight_neighbor_NW = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_N = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_NE = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_E = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_SE = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_S = assign_weight_to_edge_helper(neighbor_s);
          all_waypoints[row][col].weight_neighbor_SW = assign_weight_to_edge_helper(neighbor_sw);
          all_waypoints[row][col].weight_neighbor_W = assign_weight_to_edge_helper(neighbor_w);
        } else {
          all_waypoints[row][col].weight_neighbor_NW = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_N = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_NE = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_E = assign_weight_to_edge_helper(neighbor_e);
          all_waypoints[row][col].weight_neighbor_SE = assign_weight_to_edge_helper(neighbor_se);
          all_waypoints[row][col].weight_neighbor_S = assign_weight_to_edge_helper(neighbor_s);
          all_waypoints[row][col].weight_neighbor_SW = assign_weight_to_edge_helper(neighbor_sw);
          all_waypoints[row][col].weight_neighbor_W = assign_weight_to_edge_helper(neighbor_w);
        }
      } else if (row == (num_row_waypoint - 1)) {

        if (col == 0) {
          all_waypoints[row][col].weight_neighbor_SE = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_S = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_SW = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_W = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_NW = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_N = assign_weight_to_edge_helper(neighbor_n);
          all_waypoints[row][col].weight_neighbor_NE = assign_weight_to_edge_helper(neighbor_ne);
          all_waypoints[row][col].weight_neighbor_E = assign_weight_to_edge_helper(neighbor_e);

        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].weight_neighbor_NE = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_E = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_SE = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_S = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_SW = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_W = assign_weight_to_edge_helper(neighbor_w);
          all_waypoints[row][col].weight_neighbor_NW = assign_weight_to_edge_helper(neighbor_nw);
          all_waypoints[row][col].weight_neighbor_N = assign_weight_to_edge_helper(neighbor_n);

        } else {
          all_waypoints[row][col].weight_neighbor_SE = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_S = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_SW = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_W = assign_weight_to_edge_helper(neighbor_w);
          all_waypoints[row][col].weight_neighbor_NW = assign_weight_to_edge_helper(neighbor_nw);
          all_waypoints[row][col].weight_neighbor_N = assign_weight_to_edge_helper(neighbor_n);
          all_waypoints[row][col].weight_neighbor_NE = assign_weight_to_edge_helper(neighbor_ne);
          all_waypoints[row][col].weight_neighbor_E = assign_weight_to_edge_helper(neighbor_e);
        }
      } else {

        if (col == 0) {
          all_waypoints[row][col].weight_neighbor_SW = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_W = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_NW = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_N = assign_weight_to_edge_helper(neighbor_n);
          all_waypoints[row][col].weight_neighbor_NE = assign_weight_to_edge_helper(neighbor_ne);
          all_waypoints[row][col].weight_neighbor_E = assign_weight_to_edge_helper(neighbor_e);
          all_waypoints[row][col].weight_neighbor_SE = assign_weight_to_edge_helper(neighbor_se);
          all_waypoints[row][col].weight_neighbor_S = assign_weight_to_edge_helper(neighbor_s);

        } else if (col == (num_col_waypoint - 1)) {
          all_waypoints[row][col].weight_neighbor_NE = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_E = edge_weight_no_eligible_neighbor;
          all_waypoints[row][col].weight_neighbor_SE = edge_weight_no_eligible_neighbor;

          all_waypoints[row][col].weight_neighbor_S = assign_weight_to_edge_helper(neighbor_s);
          all_waypoints[row][col].weight_neighbor_SW = assign_weight_to_edge_helper(neighbor_sw);
          all_waypoints[row][col].weight_neighbor_W = assign_weight_to_edge_helper(neighbor_w);
          all_waypoints[row][col].weight_neighbor_NW = assign_weight_to_edge_helper(neighbor_nw);
          all_waypoints[row][col].weight_neighbor_N = assign_weight_to_edge_helper(neighbor_n);

        } else { // all other spaces are filled with normal weights for neighbor type
          all_waypoints[row][col].weight_neighbor_N = assign_weight_to_edge_helper(neighbor_n);
          all_waypoints[row][col].weight_neighbor_NE = assign_weight_to_edge_helper(neighbor_ne);
          all_waypoints[row][col].weight_neighbor_E = assign_weight_to_edge_helper(neighbor_e);
          all_waypoints[row][col].weight_neighbor_SE = assign_weight_to_edge_helper(neighbor_se);
          all_waypoints[row][col].weight_neighbor_S = assign_weight_to_edge_helper(neighbor_s);
          all_waypoints[row][col].weight_neighbor_SW = assign_weight_to_edge_helper(neighbor_sw);
          all_waypoints[row][col].weight_neighbor_W = assign_weight_to_edge_helper(neighbor_w);
          all_waypoints[row][col].weight_neighbor_NW = assign_weight_to_edge_helper(neighbor_nw);
        }
      }
    }
  }
}

// Functions in this section are done
/*********************************************************************************************************************
 * Functions that help update graph in real time based on real performance
 * or other factors predetermined to cause issues such as:
 * 1. When car gets stuck trying to visit a node due to obstacle, it updates the graph edge and vertice weights to make
 *that edge connecting to a point less algorithmically desireable and maybe assigns the vertice weight higher.
 * 2. Node exists in a danger zone and will be filled in and updated with undesirable values to have the algorithm
 *reflect that
 ********************************************************************************************************************/
void waypoints__create_polygon_using_coordinate_data(four_side_polygon_s *polygon, float *latitude_points,
                                                     float *longitude_points) {
  for (int idx = 0; idx < FOUR_SIDED_POLYGON_SIDES; idx++) {
    polygon->latitude_points[idx] = latitude_points[idx];
    polygon->longitude_points[idx] = longitude_points[idx];
  }
}

// calls all polygon create functions below
void waypoints__create_all_polygons(void) {
  waypoints__create_polygon_parking_ramp();
  waypoints__create_polygon_parking_garage_extremes();
  // add any additional polygons that need special attention here
}

// populate specific four_side_polygon_s struct using waypoints__create_polygon_using_coordinate_data from glob arrays
void waypoints__create_polygon_parking_ramp(void) {
  waypoints__create_polygon_using_coordinate_data(&polygon__garage_ramp, north_garage_ramp_latitude_points,
                                                  north_garage_ramp_longitude_points);
}

// populate specific four_side_polygon_s struct using waypoints__create_polygon_using_coordinate_data from glob arrays
void waypoints__create_polygon_parking_garage_extremes(void) {
  waypoints__create_polygon_using_coordinate_data(&polygon__garage_extreme_boundaries,
                                                  north_garage_boundaries_latitude_points,
                                                  north_garage_boundaries_longitude_points);
}

// used by all specialized polygon functions to replace the node weights to appropriate values
void waypoints__find_and_replace_all_bad_nodes_and_assign_appropriate_node_weights_helper(
    four_side_polygon_s *polygon_to_check, uint8_t weight_to_assign_to_nodes_inside_polygon) {
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      waypoint_s *current_waypoint = &all_waypoints[row][col];
      if (waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(polygon_to_check, current_waypoint)) {
        current_waypoint->weight_of_this_node = weight_to_assign_to_nodes_inside_polygon;
      }
    }
  }
}

// Calls all of the functions below it so we can call one function from initialize graph function
void waypoints__find_and_replace_all_bad_nodes_in_all_polygons(void) {
  waypoints__find_and_replace_parking_garage_ramp_node_weights();
  waypoints__find_and_replace_parking_garage_light_post_node_weights();
  // Add more function calls if necessary with more polygons
}

// uses the waypoints__find_and_replace_all_bad_nodes_and_assign_appropriate_node_weights_helper to run for polygon
void waypoints__find_and_replace_parking_garage_ramp_node_weights(void) {
  waypoints__find_and_replace_all_bad_nodes_and_assign_appropriate_node_weights_helper(
      &polygon__garage_ramp, node_weight_parking_garage_ramp_offset);
}

// uses the waypoints__find_and_replace_all_bad_nodes_and_assign_appropriate_node_weights_helper to run for polygon
void waypoints__find_and_replace_parking_garage_light_post_node_weights(void) {
  // TODO: if this is necessary, fill in this information
}

/*********************************************************************************************************************
 * Functions that help determine condional locations of waypoints against other waypoints in graph
 ********************************************************************************************************************/
// returns true if waypoint is inside polygon defined by 4 coordinates passed in as arrays
bool waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(four_side_polygon_s *polygon_to_check,
                                                                      waypoint_s *waypoint_to_check) {
  // referrenced http://alienryderflex.com/polygon/ for the following code:
  //  Globals which should be set before calling this function:
  //
  //  int    polyCorners  =  how many corners the polygon has (no repeats)
  //  float  polyX[]      =  horizontal coordinates of corners
  //  float  polyY[]      =  vertical coordinates of corners
  //  float  x, y         =  point to be tested
  //
  //  (Globals are used in this example for purposes of speed.  Change as
  //  desired.)
  //
  //  The function will return YES if the point x,y is inside the polygon, or
  //  NO if it is not.  If the point is exactly on the edge of the polygon,
  //  then the function may return YES or NO.
  //
  //  Note that division by zero is avoided because the division is protected
  //  by the "if" clause which surrounds it.

  float waypoint_latitude = waypoint_to_check->latitude;
  float waypoint_longitude = waypoint_to_check->longitude;
  float *polygon_latitudes = polygon_to_check->latitude_points;
  float *polygon_longitudes = polygon_to_check->longitude_points;

  // Local variables
  uint8_t i;
  uint8_t j = FOUR_SIDED_POLYGON_SIDES - 1;
  bool oddNodes = false;

  // If odd nodes is true, it indicates that the point is inside the polygon
  for (i = 0; i < FOUR_SIDED_POLYGON_SIDES; i++) {
    if ((polygon_latitudes[i] < waypoint_latitude && polygon_latitudes[j] >= waypoint_latitude) ||
        (polygon_latitudes[j] < waypoint_latitude && polygon_latitudes[i] >= waypoint_latitude)) {
      if (polygon_longitudes[i] + (waypoint_latitude - polygon_latitudes[i]) /
                                      (polygon_latitudes[j] - polygon_latitudes[i]) *
                                      (polygon_longitudes[j] - polygon_longitudes[i]) <
          waypoint_longitude) {
        oddNodes = !oddNodes;
      }
    }
    j = i;
  }
  return oddNodes;
}

// returns true if car is located within boundaries of graph. Uses function below to check with 4 coordinates
extern bool waypoints__is_car_located_within_graph_boundaries(waypoint_s *car_location_waypoint) {
  return waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(&polygon__garage_extreme_boundaries,
                                                                          car_location_waypoint);
}

// returns true if dest is located within boundaries of graph. Uses function below to check with 4 coordinates
extern bool waypoints__is_dest_located_within_graph_boundaries(waypoint_s *dest_location_waypoint) {
  return waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(&polygon__garage_extreme_boundaries,
                                                                          dest_location_waypoint);
}


// this is used to find the closest waypoint to a point such as the car's or destination's waypoint by checking through
// all waypoints in the graph
extern waypoint_s *
waypoints__closest_waypoint_inside_graph_to_waypoint_outside_graph(waypoint_s *waypoint_outside_graph) {
  // set high value because we need it to be bigger than first waypoint distance logic below to set correctly
  float shortest_distance_to_waypoint_outside_graph = 99999.9;
  float distance_to_waypoint_outside_graph;
  waypoint_s *shortest_waypoint_inside_graph = NULL; // return param

  // iterate through all the waypoint nodes in the graph
  for (uint8_t row = 0; row < num_row_waypoint; row++) {
    for (uint8_t col = 0; col < num_col_waypoint; col++) {
      distance_to_waypoint_outside_graph = geo_helper__find_distance_between_two_sets_of_coordinates_in_radians(
          waypoint_outside_graph->coordinates_in_radians, all_waypoints[row][col].coordinates_in_radians);
      if (distance_to_waypoint_outside_graph < shortest_distance_to_waypoint_outside_graph) {
        shortest_distance_to_waypoint_outside_graph = distance_to_waypoint_outside_graph; // set newest shortest node
        shortest_waypoint_inside_graph = &all_waypoints[row][col]; // assign pointer to the return param
      }
    }
  }
  return shortest_waypoint_inside_graph;
}


/*********************************************************************************************************************
 * Helper Functions
 ********************************************************************************************************************/
static uint8_t assign_weight_to_edge_helper(neighbors_e neigbor_direction) {
  uint8_t weight = 0;
  switch (neigbor_direction) {
  case neighbor_n:
    weight = edge_weight_square_edge_node;
    break;
  case neighbor_ne:
    weight = edge_weight_diagonal_edge_node;
    break;
  case neighbor_e:
    weight = edge_weight_square_edge_node;
    break;
  case neighbor_se:
    weight = edge_weight_diagonal_edge_node;
    break;
  case neighbor_s:
    weight = edge_weight_square_edge_node;
    break;
  case neighbor_sw:
    weight = edge_weight_diagonal_edge_node;
    break;
  case neighbor_w:
    weight = edge_weight_square_edge_node;
    break;
  case neighbor_nw:
    weight = edge_weight_diagonal_edge_node;
    break;
  }
  return weight;
}

static float find_distance_to_neighbor_helper(neighbors_e neigbor_direction, uint8_t row, uint8_t col) {
  waypoint_s current_waypoint = all_waypoints[row][col];
  waypoint_s neighbor_waypoint;
  switch (neigbor_direction) {
  case neighbor_n:
    neighbor_waypoint = all_waypoints[row - 1][col];
    break;
  case neighbor_ne:
    neighbor_waypoint = all_waypoints[row - 1][col + 1];
    break;
  case neighbor_e:
    neighbor_waypoint = all_waypoints[row][col + 1];
    break;
  case neighbor_se:
    neighbor_waypoint = all_waypoints[row + 1][col + 1];
    break;
  case neighbor_s:
    neighbor_waypoint = all_waypoints[row + 1][col];
    break;
  case neighbor_sw:
    neighbor_waypoint = all_waypoints[row + 1][col - 1];
    break;
  case neighbor_w:
    neighbor_waypoint = all_waypoints[row][col - 1];
    break;
  case neighbor_nw:
    neighbor_waypoint = all_waypoints[row - 1][col - 1];
    break;
  }
  return geo_helper__find_distance_between_two_sets_of_coordinates_in_radians(current_waypoint.coordinates_in_radians,
                                                                              neighbor_waypoint.coordinates_in_radians);
}

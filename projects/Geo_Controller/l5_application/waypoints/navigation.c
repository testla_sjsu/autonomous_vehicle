#include "navigation.h"

/*********************************************************************************************************************
 * Value Definitions for Checkpoint Functinality
 ********************************************************************************************************************/
const float checkpoint_completed_distance_threshold = 2.0; // unit: meters

static uint8_t curr_checkpoint_idx;
static uint8_t prev_checkpoint_idx;
// static navigation_status_e navigation_status; // TODO: send out this value on the can bus for peripherals to use

/*********************************************************************************************************************
 * Waypoint Algorithm Functions
 ********************************************************************************************************************/
// this function runs on the command received over the can bus from the bridge controller that a new destination has
// been input by the mobile app
void navigation__init_new_navigation_sequence(void) {
  //

  // Locate closest graph waypoint to destination

  // Locate closest graph waypoint to car

  // Fill in the car_navagation_waypoints_s struct for destination and reposition it to the closest waypoint towards the
  // car's location

  // Readjust the car_navagation_waypoints_s struct for repositioning the closest waypoint to point towards car

  //
}

// Used to clear out memory that the checkpoints are stored in to reinitialize a navigation sequence
void navigation__clear_checkpoints_array_memory_section(void) {
  memset(&all_checkpoints, max_num_of_checkpoints, sizeof(checkpoint_s));
}

// should run every 1000 milliseconds for book keeping purposes to update the nodes that are closest to the car. This is
// used in cases where a node selected by the algorithm next originally appeared to be visitable, but is completely
// blocked by an unseen obstacle
void navigation__maintain_navigation_algorithm(void) {
  // 1st step, update current car navigation information
  navigation__update_curr_car_navigation_waypoints_values(curr_car_navigation_waypoints);

  // 2nd step, serive checkpoint data
  prev_checkpoint_idx = curr_checkpoint_idx;
  curr_checkpoint_idx = navigation__update_checkpoint_information(all_checkpoints, prev_checkpoint_idx); // use

  if (curr_checkpoint_idx != prev_checkpoint_idx) {
    // do logic that happens to switch over checkponts
  } else {
    // do nothing because
  }
  navigation__copy_curr_car_navagation_waypoints_into_prev(curr_car_navigation_waypoints,
                                                           prev_car_navigation_waypoints);
}

void navigation__update_curr_car_navigation_waypoints_values(car_navagation_waypoints_s *navigation_waypoints) {
  //
}

void navigation__service_and_update_checkpoint_information(checkpoint_s *checkpoints, uint8_t checkpoint_idx) {
  checkpoint_status_e status = navigation__has_checkpoint_been_completed(all_checkpoints, curr_checkpoint_idx);

  switch (status) {
  case checkpoint_not_completed_yet:
    // Do nothing
    break;
  case checkpoint_completed:
    // do something here
    curr_checkpoint_idx += 1;
    break;
  case checkpoint_uncompletable:
    // do something here
    navigation__insert_new_checkpoint_into_checkpoint_list(all_checkpoints, curr_checkpoint_idx);
    break;
  }
}

// Returns the status of navigating to the checkpoint
checkpoint_status_e navigation__update_checkpoint_status(checkpoint_s *checkpoints, uint8_t checkpoint_idx) {
  checkpoint_status_e return_checkpoint_status;
  bool is_checkpoint_finished = navigation__has_checkpoint_been_completed(checkpoints, checkpoint_idx);
  bool is_checkpoint_uncompletable = navigation__is_checkpoint_uncompleteable(checkpoints, checkpoint_idx);
  if (is_checkpoint_finished) {
    return_checkpoint_status = checkpoint_completed;
  } else if (is_checkpoint_uncompletable) {
    return_checkpoint_status = checkpoint_uncompletable;
  } else {
    return_checkpoint_status = checkpoint_not_completed_yet;
  }
  return return_checkpoint_status;
}

// if we reach a threshold under checkpoint_completed_distance_threshold for the checkpoint node's
// waypoint_s.distance_to_car (checkpoint->waypoint_s.distance_to_car<checkpoint_completed_distance_threshold), then
// we mark the checkpoint completed in the checkpoints list and move forward to the nect checkpoint to use use the
// output of this
bool navigation__has_checkpoint_been_completed(checkpoint_s *checkpoints, uint8_t checkpoint_idx) {
  // Check if distance to the checkpoint waypoint is under the distance threshold to car to be considered visited
  if (checkpoints[curr_checkpoint_idx].checkpoint_waypoint->distance_to_car < checkpoint_completed_distance_threshold) {
    return true;
  } else {
    return false;
  }
}

// TODO this function needs to use a time
bool navigation__is_checkpoint_uncompleteable(checkpoint_s *checkpoints, uint8_t checkpoint_num) {
  // Check if time used to visit checkpoint is over maximum allotted time to use for a checkpoint
  // uint32_t max_time if () { return true; }
  //   else {
  //     return false;
  //   }
  return false;
}

void navigation__insert_new_checkpoint_into_checkpoint_list(checkpoint_s *cehckpoints, uint8_t checkpoint_idx) {
  //
}

// Copies
void navigation__copy_curr_car_navagation_waypoints_into_prev(car_navagation_waypoints_s *navigation_waypoints_one,
                                                              car_navagation_waypoints_s *navigation_waypoints_two) {
  memcpy(prev_car_navigation_waypoints, curr_car_navigation_waypoints, sizeof(*curr_car_navigation_waypoints));
}

bool navigation__mark_checkpoint_completed_and_select_new_checkpoint(void) { return false; }

bool navigation__insert_new_checkpoint(void) { return false; }

// void navigation__update_waypoint();

bool navigation__has_closest_node_changed(car_navagation_waypoints_s *car_navigation) { return false; }

void navigation__update_car_location_in_node(car_navagation_waypoints_s one) {}

bool navigation__is_car_at_final_destination(checkpoint_s *checkpoints, uint8_t checkpoint_idx) {
  if ((checkpoints[curr_checkpoint_idx].checkpoint_status == checkpoint_completed) &&
      (checkpoints[curr_checkpoint_idx].is_checkpoint_final_destination == true)) {
    return true;
  } else {
    return false;
  }
}

void navigation__send_navigation_status_over_can_bus(void) {
  //
}

uint8_t navigation__update_checkpoint_information(checkpoint_s *one, uint8_t two) { return 0; }
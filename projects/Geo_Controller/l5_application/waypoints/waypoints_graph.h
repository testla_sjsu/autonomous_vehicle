#pragma once
#include "board_io.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "geo.h"
#include "gps.h"
#include <math.h>

#include "testla_proto1.h"

// waypoints
#define num_row_waypoint 17 // has to be define because of matrix issue with const uint
#define num_col_waypoint 9  // has to be define because of matrix issue with const uint

#define FOUR_SIDED_POLYGON_SIDES 4

// checkpoints
// should be like 35 or something in the end
#define max_num_of_checkpoints 153 // this should never be more than num_row_waypoint * num_col_waypoint

/*********************************************************************************************************************
 * Data structures and enumerations used in Car Navigation to store data related to car
 ********************************************************************************************************************/
typedef enum {
  neighbor_n = 0,
  neighbor_ne = 1,
  neighbor_e = 2,
  neighbor_se = 3,
  neighbor_s = 4,
  neighbor_sw = 5,
  neighbor_w = 6,
  neighbor_nw = 7
} neighbors_e;

// Used in graph construction and maintainance
typedef struct {
  gps__coordinates_t coordinates;
  float latitude;  // TODO: get rid of this
  float longitude; // TODO: get rid of this

  uint8_t row_idx_of_waypoint_graph; // this is useful for the navigation of car to next checkpoint
  uint8_t col_idx_of_waypoint_graph; // this is useful for the navigation of car to next checkpoint

  // Weight of this specific node
  uint8_t weight_of_this_node;
  // Neighors to node in the graph
  uint8_t weight_neighbor_N;
  uint8_t weight_neighbor_NW;
  uint8_t weight_neighbor_W;
  uint8_t weight_neighbor_SW;
  uint8_t weight_neighbor_S;
  uint8_t weight_neighbor_SE;
  uint8_t weight_neighbor_E;
  uint8_t weight_neighbor_NE;
  // Store distance to other nodes here
  float distance_neighbor_N;
  float distance_neighbor_NW;
  float distance_neighbor_W;
  float distance_neighbor_SW;
  float distance_neighbor_S;
  float distance_neighbor_SE;
  float distance_neighbor_E;
  float distance_neighbor_NE;
  // is this waypoint connected to the car waypoint?
  bool is_connected_to_car;
  bool is_connected_to_destination;
  // Store distance to car or dest if graphically connected to it/otherwise store infinity
  float distance_to_car;
  float distance_to_destination;

} __attribute__((packed)) waypoint_s;

// Define instances of this struct to check if waypoints belong inside polygon
typedef struct {
  float latitude_points[4];
  float longitude_points[4];
} four_side_polygon_s;

/*********************************************************************************************************************
 * Global structs that contain data for the waypoint algorithm
 ********************************************************************************************************************/
waypoint_s all_waypoints[num_row_waypoint][num_col_waypoint]; // the main graph used in algo

/*********************************************************************************************************************
 * Functions that help initialize graph
 ********************************************************************************************************************/
void waypoints__init(void);

void waypoints__initialize_all_waypoint_data(void);

void waypoints__initialize_row_and_col_indices(void);

void waypoints__initialize_coordinates_of_all_nodes(void);

void waypoints__initialize_coordinates_in_radians_of_all_nodes(void);

void waypoints__initialize_weight_of_nodes(void);

void waypoints__initialize_distance_between_nodes(void);

void waypoints__initialize_weight_of_edges(void);

/*********************************************************************************************************************
 * Functions that help update graph in real time based on real performance
 * or other factors predetermined to cause issues such as:
 * 1. When car gets stuck trying to visit a node due to obstacle, it updates the graph edge and vertice weights to make
 *that edge connecting to a point less algorithmically desireable and maybe assigns the vertice weight higher.
 * 2. Node exists in a danger zone and will be filled in and updated with undesirable values to have the algorithm
 *reflect that
 ********************************************************************************************************************/
void waypoints__create_polygon_using_coordinate_data(four_side_polygon_s *polygon, float *latitude_points,
                                                     float *longitude_points);

// calls all polygon create functions below
void waypoints__create_all_polygons();

// populate specific four_side_polygon_s struct using waypoints__create_polygon_using_coordinate_data from glob arrays
void waypoints__create_polygon_parking_ramp();

void waypoints__create_polygon_parking_garage_extremes();

// used by all specialized polygon functions to replace the node weights to appropriate values
void waypoints__find_and_replace_all_bad_nodes_and_assign_appropriate_node_weights_helper(
    four_side_polygon_s *polygon_to_check, uint8_t weight_to_assign_to_nodes_inside_polygon);

// Calls all of the functions below it so we can call one function from initialize graph function
void waypoints__find_and_replace_all_bad_nodes_in_all_polygons(void);

// uses the waypoints__find_and_replace_all_bad_nodes_and_assign_appropriate_node_weights_helper to run for polygon
void waypoints__find_and_replace_parking_garage_ramp_node_weights(void);

// uses the waypoints__find_and_replace_all_bad_nodes_and_assign_appropriate_node_weights_helper to run for polygon
void waypoints__find_and_replace_parking_garage_light_post_node_weights(void);

/*********************************************************************************************************************
 * Functions that help determine condional locations of waypoints against other waypoints in graph
 ********************************************************************************************************************/
// TODO: change how the polygon_function works and make it such that it works by using the
// bool waypoints__is_waypoint_inside_polygon_of_n_sides(gps_coordinates_decimal_s *starting_polygon_coodinate, uint8_t
// num_of_points, waypoint_s *waypoint_to_check);

// returns true if waypoint is inside polygon defined by 4 coordinates passed in as arrays
bool waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(four_side_polygon_s *polygon_to_check,
                                                                      waypoint_s *waypoint_to_check);

// returns true if car is located within boundaries of graph. Uses function below to check with 4 coordinates
extern bool waypoints__is_car_located_within_graph_boundaries(waypoint_s *car_location_waypoint);

// returns true if dest is located within boundaries of graph. Uses function below to check with 4 coordinates
extern bool waypoints__is_dest_located_within_graph_boundaries(waypoint_s *dest_location_waypoint);

// this is used to find the closest waypoint to a point such as the car's or destination's waypoint by checking through
// all waypoints in the graph
extern waypoint_s *
waypoints__closest_waypoint_inside_graph_to_waypoint_outside_graph(waypoint_s *waypoint_outside_graph);

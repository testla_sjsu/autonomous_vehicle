#include "Mockcompass.h"
#include "Mockgps.h"
#include "geo.h"
#include "unity.h"
#include "waypoints_graph.h"

/*********************************************************************************************************************
 * test_values
 ********************************************************************************************************************/
// These values are near the corners of the SJSU north garage. They start with NW corner, then NE, SE, SW corners.
static float test_north_garage_boundaries_latitude_points[4] = {37.339549, 37.339835, 37.338892, 37.338632};
static float test_north_garage_boundaries_longitude_points[4] = {-121.881423, -121.88079, -121.880109, -121.880704};

// https://www.google.com/maps/place/North+Parking+Garage,+S+10th+St,+San+Jose,+CA+95112/@37.3392292,-121.8806336,177m/data=!3m1!1e3!4m5!3m4!1s0x808fccbf0b89f1ab:0x14dec5b5a0131533!8m2!3d37.3393318!4d-121.8807309
static float test_point_middle_of_parking_garage_latitude = 37.339358;
static float test_point_middle_of_parking_garage_longitude = -121.880742;

// https://www.google.com/maps/place/37°20'15.3%22N+121°52'54.3%22W/@37.3381404,-121.8806915,364m/data=!3m1!1e3!4m5!3m4!1s0x0:0x14e5932601cdd2fc!8m2!3d37.3373479!4d-121.8815393
static float test_point_sjsu_engineering_building_latitude = 37.337595;
static float test_point_sjsu_engineering_building_longitude = -121.881760;

// https://www.google.com/maps/@37.3392292,-121.8806336,177m/data=!3m1!1e3
static float test_point_near_but_just_outside_of_parking_garage_latitude = 37.339271;
static float test_point_near_but_just_outside_of_parking_garage_longitude = -121.881276;

/*********************************************************************************************************************
 * test_functions
 ********************************************************************************************************************/
// void test_waypoints__init(void) { waypoints__initialize_all_waypoint_data(); }

// returns true if waypoint is inside polygon defined by 4 coordinates passed in as arrays
void test_waypoints__point_is_in_middle_of_polygon_defined_by_four_coodinates_test(void) {
  four_side_polygon_s parking_garage_boundaries_polygon;
  waypoint_s point_in_the_middle_of_parking_garage;
  bool is_point_inside_of_polygon;
  point_in_the_middle_of_parking_garage.latitude = test_point_middle_of_parking_garage_latitude;
  point_in_the_middle_of_parking_garage.longitude = test_point_middle_of_parking_garage_longitude;
  waypoints__create_polygon_using_coordinate_data(&parking_garage_boundaries_polygon,
                                                  test_north_garage_boundaries_latitude_points,
                                                  test_north_garage_boundaries_longitude_points);

  is_point_inside_of_polygon = waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(
      &parking_garage_boundaries_polygon, &point_in_the_middle_of_parking_garage);
  TEST_ASSERT_EQUAL(true, is_point_inside_of_polygon);
}

void test_waypoints__point_is_outside_of_all_coordinates_range_polygon_defined_by_four_coodinates_test(void) {
  four_side_polygon_s parking_garage_boundaries_polygon;
  waypoint_s point_is_outside_all_coordinates_range_of_parking_garage;
  bool is_point_inside_of_polygon;
  point_is_outside_all_coordinates_range_of_parking_garage.latitude = test_point_sjsu_engineering_building_latitude;
  point_is_outside_all_coordinates_range_of_parking_garage.longitude = test_point_sjsu_engineering_building_longitude;
  waypoints__create_polygon_using_coordinate_data(&parking_garage_boundaries_polygon,
                                                  test_north_garage_boundaries_latitude_points,
                                                  test_north_garage_boundaries_longitude_points);
  is_point_inside_of_polygon = waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(
      &parking_garage_boundaries_polygon, &point_is_outside_all_coordinates_range_of_parking_garage);
  TEST_ASSERT_EQUAL(false, is_point_inside_of_polygon);
}

void test_waypoints__point_is_near_but_outside_polygon_defined_by_four_coodinates_test(void) {
  four_side_polygon_s parking_garage_boundaries_polygon;
  waypoint_s point_is_near_but_outside_coordinates_range_of_parking_garage;
  bool is_point_inside_of_polygon;
  point_is_near_but_outside_coordinates_range_of_parking_garage.latitude =
      test_point_near_but_just_outside_of_parking_garage_latitude;
  point_is_near_but_outside_coordinates_range_of_parking_garage.longitude =
      test_point_near_but_just_outside_of_parking_garage_longitude;
  waypoints__create_polygon_using_coordinate_data(&parking_garage_boundaries_polygon,
                                                  test_north_garage_boundaries_latitude_points,
                                                  test_north_garage_boundaries_longitude_points);
  is_point_inside_of_polygon = waypoints__is_waypoint_inside_polygon_defined_by_four_coodinates(
      &parking_garage_boundaries_polygon, &point_is_near_but_outside_coordinates_range_of_parking_garage);
  TEST_ASSERT_EQUAL(false, is_point_inside_of_polygon);
}
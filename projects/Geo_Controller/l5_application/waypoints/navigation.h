#pragma once
#include "board_io.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "geo.h"
#include "gps.h"
#include "waypoints_graph.h"

#include "testla_proto1.h"

/*********************************************************************************************************************
 * Data structures and enumerations used in Car Navigation to store data related to car
 ********************************************************************************************************************/
typedef struct {
  waypoint_s current_car_location; // seperate waypoint instance constructed for the car to be compatible with graph
  waypoint_s destination_location; // seperate waypoint instance constructed for the dest to be compatible with graph
  waypoint_s *next_checkpoint_location;

  uint8_t closest_waypoint_to_car_row;         // idx of waypoint the car is nearest to
  uint8_t closest_waypoint_to_car_col;         // waypoint the car is nearest to
  uint8_t next_checkpoint_waypoint_row;        // waypoint the car is going to
  uint8_t next_checkpoint_waypoint_col;        // waypoint the car is going to
  uint8_t closest_waypoint_to_destination_row; // final waypoint before reaching destination
  uint8_t closest_waypoint_to_destination_col; // final waypoint before reaching destination

  // Only store pointers to the waypoints for reduced space usage
  waypoint_s *first_closest_waypoint_to_current_location;
  waypoint_s *second_closest_waypoint_to_current_location;
  waypoint_s *third_closest_waypoint_to_current_location;
  waypoint_s *fourth_closest_waypoint_to_current_location;
  waypoint_s *fifth_closest_waypoint_to_current_location;
  waypoint_s *sixth_closest_waypoint_to_current_location;
  waypoint_s *seventh_closest_waypoint_to_current_location;
  waypoint_s *eighth_closest_waypoint_to_current_location;
  waypoint_s *ninth_closest_waypoint_to_current_location;
} __attribute__((packed)) car_navagation_waypoints_s;

// Used by the checkpoint struct and stores information used in the checkpoint_s struct
typedef enum {
  checkpoint_not_completed_yet = 0, // start up, and switches into this after checkpoint_completed if more are queued
  checkpoint_completed = 1,         // when car is within n amount of meters of checkpoint, checkpoint_status updates
  checkpoint_uncompletable = 2      // when car times out trying to go to this checkpoint, it defaults to this value
} checkpoint_status_e;

typedef enum {
  navigation_currently_not_in_progress = 0, // a start up state when the car is first powered on
  navigation_in_progress = 1,               // if navigation is in progress it is represented with this number
  navigation_just_finished = 2, // if navigation sequence is finished, this is what will tell the rest of the MCUs
  navigation_error = 3 // if navigation sequence is attempted to begin, but it is not ready to initialize for reasons
} navigation_status_e;

typedef struct {
  waypoint_s *checkpoint_waypoint;
  // waypoint_s *curr_checkpoint; // uncomment if using a list implementation
  checkpoint_status_e checkpoint_status; // 0 if not visited yet, 1 if visitable, 2 if uncompletable
  bool is_checkpoint_final_destination;
} __attribute__((packed)) checkpoint_s;

/*********************************************************************************************************************
 * Global structs that contain data for the navigation algorithm
 ********************************************************************************************************************/
car_navagation_waypoints_s *curr_car_navigation_waypoints;
car_navagation_waypoints_s *prev_car_navigation_waypoints;
checkpoint_s all_checkpoints[max_num_of_checkpoints];

/*********************************************************************************************************************
 * Waypoint Algorithm Functions
 ********************************************************************************************************************/
// this function runs on the command received over the can bus from the bridge controller that a new destination has
// been input by the mobile app
void navigation__init_new_navigation_sequence(void);

// Used to clear out memory where the array
void navigation__clear_checkpoints_array_memory_section(void);

// should run every 1000 milliseconds for book keeping purposes to update the nodes that are closest to the car. This is
// used in cases where a node selected by the algorithm next originally appeared to be visitable, but is completely
// blocked by an unseen obstacle
void navigation__update_current_car_location_closest_waypoint_members_values(void);

void navigation__service_and_update_checkpoint_information(checkpoint_s *checkpoints, uint8_t checkpoint_idx);

// if we reach a threshold under checkpoint_completed_distance_threshold for the checkpoint node's
// waypoint_s.distance_to_car (checkpoint->waypoint_s.distance_to_car<checkpoint_completed_distance_threshold), then
// we mark the checkpoint completed in the checkpoints list and move forward to the nect checkpoint to use use the
// output of this

checkpoint_status_e navigation__update_checkpoint_status(checkpoint_s *checkpoints, uint8_t checkpoint_num);

// Returns if the checkpoint has been completed
bool navigation__has_checkpoint_been_completed(checkpoint_s *checkpoints, uint8_t checkpoint_num);

// Returns if the checkpoint is not able to be completed
bool navigation__is_checkpoint_uncompleteable(checkpoint_s *checkpoints, uint8_t checkpoint_num);

bool navigation__mark_checkpoint_completed_and_select_new_checkpoint(void);

void navigation__copy_curr_car_navagation_waypoints_into_prev(car_navagation_waypoints_s *navigation_waypoints_one,
                                                              car_navagation_waypoints_s *navigation_waypoints_two);

bool navigation__has_closest_node_changed(car_navagation_waypoints_s *car_navigation);

void navigation__update_car_location_in_node(car_navagation_waypoints_s);

bool navigation__is_car_at_final_destination(checkpoint_s *checkpoints, uint8_t checkpoint_num);

void navigation__send_navigation_status_over_can_bus(void);

void navigation__update_curr_car_navigation_waypoints_values(car_navagation_waypoints_s *navigation_waypoints);
uint8_t navigation__update_checkpoint_information(checkpoint_s *one, uint8_t two);

void navigation__insert_new_checkpoint_into_checkpoint_list(checkpoint_s *cehckpoints, uint8_t checkpoint_idx);

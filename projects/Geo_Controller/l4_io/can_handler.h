#pragma once

#include "testla_proto1.h"
#include <stdio.h>

static const uint16_t mia_increment_value = 100;

void can_handler__init(void);
void can_handler__manage_mia_10Hz(void);
void can_handler__transmit_debug_messages_1Hz(void);
void can_handler__transmit_status_10Hz(void);
void can_handler__handle_all_incoming_messages(void);
void can_handler__health_check(void);
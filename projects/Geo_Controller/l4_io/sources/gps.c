#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "board_io.h"
#include "clock.h" // needed for uart initialization
#include "gpio.h"
#include "gps.h"
#include "line_buffer.h"
#include "uart.h"

#define LATITUDE_LONGITUDE_STRING_MAX_SIZE 20
#define GPS_LINE_MAX_SIZE 200
#define GPS_HEADER_SIZE 7
static const uart_e gps_uart = UART__2;
static const char *expected_gps_header = "$GPGGA";
static const char *SET_8HZ_UPDATES = "$PMTK220,120*2D\r\n";
static const char *SET_115200_BAUD = "$PMTK251,115200*1F\r\n";
static const char *SET_GPGGA_ONLY = "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n";

#define SETUP_STATE_SET_GPS_BAUD 0
#define SETUP_STATE_CHANGE_SJTWO_BAUD 1
#define SETUP_STATE_SET_GPS_MESSAGE_TYPES 2
#define SETUP_STATE_SET_GPS_UPDATE_RATE 3
#define SETUP_STATE_DONE 4

static char line_buffer[2048];
static line_buffer_s line;

static gps__coordinates_t parsed_coordinates;
static uint8_t gps_fix;
static QueueHandle_t rxq_handle;
static QueueHandle_t txq_handle;
static gps__update_stats_t update_stats;

static void gps__transfer_data_from_uart_driver_to_line_buffer(void) {
  char byte;
  const uint32_t zero_timeout = 0;

  while (uart__get(gps_uart, &byte, zero_timeout)) {
    line_buffer__add_byte(&line, byte);
  }
}

static bool gps__grab_string_to_comma(char *search_string, size_t *index, char *return_string,
                                      size_t max_search_length) {
  bool valid_grab = false;
  size_t return_index = 0;
  size_t search_index = *index;
  while (return_index < max_search_length) {
    if (search_string[search_index] == ',') {
      return_string[return_index] = '\0';
      valid_grab = true;
      search_index++;
      break;
    } else if (search_string[search_index] == '\0') {
      break;
    } else {
      return_string[return_index] = search_string[search_index];
      search_index++;
      return_index++;
    }
  }

  if ((search_index - *index) < 2) {
    valid_grab = false;
  }

  *index = search_index;
  return valid_grab;
}

static void gps__ignore_to_next_comma(char *search_string, size_t *index, size_t max_search_length) {
  size_t search_index = *index;
  while (search_index < max_search_length) {
    if (search_string[search_index] == ',') {
      search_index++;
      break;
    }
    search_index++;
  }
  *index = search_index;
}

static float nmea_to_degrees(char *nmea) {
  int digits_to_left_of_decimal = 0;
  while (nmea[digits_to_left_of_decimal] != '.') {
    digits_to_left_of_decimal++;
  }
  int number_of_mm_digits = digits_to_left_of_decimal - 2;
  char mm[3] = {0};
  for (int i = 0; i < number_of_mm_digits; i++) {
    mm[i] = nmea[i];
  }
  char dd[20] = {0};
  strcpy(dd, &nmea[number_of_mm_digits]);
  float degrees = atof(mm) + (atof(dd) / 60);
  return degrees;
}

static void set_stats(uint16_t period_ms) {
  if (period_ms > update_stats.max_period_ms) {
    update_stats.max_period_ms = period_ms;
  }
  if (!update_stats.min_period_ms || period_ms < update_stats.min_period_ms) {
    update_stats.min_period_ms = period_ms;
  }
  int16_t mean = update_stats.average_period_ms;
  update_stats.average_period_ms = (uint16_t)(mean + (((int16_t)period_ms - mean) / (int16_t)update_stats.counter));
}

static void update_time_period(uint32_t tick_10Hz) {
  update_stats.counter++;
  static uint32_t last_tick_10Hz = 0;
  if (last_tick_10Hz) {
    int32_t tick_diff = tick_10Hz - last_tick_10Hz;
    if (tick_diff > 0) {
      uint16_t time_diff_ms = tick_diff * 100;
      set_stats(time_diff_ms);
    }
  }
  last_tick_10Hz = tick_10Hz;
}

static bool gps__parse_coordinates_from_line(void) {
  char gps_line[GPS_LINE_MAX_SIZE];
  char attempted_gps_line[GPS_LINE_MAX_SIZE];
  uint16_t line_count = 0;
  bool good_line = false;
  while (line_buffer__remove_line(&line, attempted_gps_line, sizeof(attempted_gps_line))) {
    strcpy(gps_line, attempted_gps_line);
    line_count++;
  }

  if (line_count > 0) {
    size_t read_index = 0;
    size_t gps_line_length = strlen(gps_line);

    char gps_header_string[GPS_HEADER_SIZE];
    good_line = gps__grab_string_to_comma(gps_line, &read_index, gps_header_string, GPS_HEADER_SIZE);
    good_line = (strcmp(expected_gps_header, gps_header_string) == 0);
    char lat_long_strings[2][LATITUDE_LONGITUDE_STRING_MAX_SIZE] = {{0}, {0}};
    if (good_line) {
      size_t bytes_left = gps_line_length - read_index;
      gps__ignore_to_next_comma(gps_line, &read_index, bytes_left);
      char compass_direction[2];
      for (int i = 0; i < 2; i++) {
        bytes_left = gps_line_length - read_index;
        size_t max_size = (sizeof(lat_long_strings[i]) < bytes_left) ? sizeof(lat_long_strings[i]) : bytes_left;
        good_line = gps__grab_string_to_comma(gps_line, &read_index, lat_long_strings[i], max_size);
        bytes_left = gps_line_length - read_index;
        max_size = (sizeof(lat_long_strings[i]) < bytes_left) ? sizeof(lat_long_strings[i]) : bytes_left;
        good_line = gps__grab_string_to_comma(gps_line, &read_index, &compass_direction[i], max_size);
      }
      char gps_fix_char;
      gps__grab_string_to_comma(gps_line, &read_index, &gps_fix_char, 1);
      gps_fix = gps_fix_char - '0';

      if (good_line) {
        parsed_coordinates.lat_rad = nmea_to_degrees(lat_long_strings[0]);
        parsed_coordinates.lat_rad *= compass_direction[0] == 'N' ? 1 : -1;
        parsed_coordinates.lon_rad = nmea_to_degrees(lat_long_strings[1]);
        parsed_coordinates.lon_rad *= compass_direction[1] == 'E' ? 1 : -1;
      }
    }
  }
  return good_line;
}

void put_config_sentence(const char *config) {
  int i = 0;
  while (config[i] != '\0') {
    uart__polled_put(gps_uart, config[i++]);
  }
}

bool gps__send_setup(void) {
  static uint8_t setup_state = 0;
  bool setup_complete = false;

  switch (setup_state) {
  case SETUP_STATE_SET_GPS_BAUD:
    put_config_sentence(SET_115200_BAUD);
    setup_state = SETUP_STATE_CHANGE_SJTWO_BAUD;
    break;

  case SETUP_STATE_CHANGE_SJTWO_BAUD: {
    uint32_t p_clock = clock__get_peripheral_clock_hz();
    uart__init(gps_uart, p_clock, (uint32_t)115200);
    setup_state = SETUP_STATE_SET_GPS_MESSAGE_TYPES;
  } break;

  case SETUP_STATE_SET_GPS_MESSAGE_TYPES:
    put_config_sentence(SET_GPGGA_ONLY);
    setup_state = SETUP_STATE_SET_GPS_UPDATE_RATE;
    break;

  case SETUP_STATE_SET_GPS_UPDATE_RATE:
    put_config_sentence(SET_8HZ_UPDATES);
    setup_state = SETUP_STATE_DONE;
    setup_complete = true;
    break;

  default:
    break;
  }
  return setup_complete;
}

void gps__init(void) {
  line_buffer__init(&line, line_buffer, sizeof(line_buffer));
  uint32_t p_clock = clock__get_peripheral_clock_hz();
  uart__init(gps_uart, p_clock, (uint32_t)9600);
  rxq_handle = xQueueCreate(2048, sizeof(char));
  txq_handle = xQueueCreate(8, sizeof(char));
  uart__enable_queues(gps_uart, rxq_handle, txq_handle);
}

void gps__run_once(uint32_t tick_10Hz) {
  gps__transfer_data_from_uart_driver_to_line_buffer();
  if (gps__parse_coordinates_from_line()) {
    update_time_period(tick_10Hz);
  }
  if (gps_fix) {
    gpio__reset(board_io__get_led3());
  } else {
    gpio__set(board_io__get_led3());
  }
}

// void gps__debug_dump(void) { line_buffer__print_buffer(&line); }

gps__coordinates_t gps__get_coordinates(void) { return parsed_coordinates; }
uint8_t gps__get_fix_status(void) { return gps_fix; }
gps__update_stats_t gps__get_stats(void) { return update_stats; }
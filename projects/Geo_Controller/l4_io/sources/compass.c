#include "compass.h"
#include "clock.h"
#include "i2c.h"
#include <math.h>
#include <stdio.h>

static const i2c_e compass_i2c = I2C__0;
StaticSemaphore_t sem;
StaticSemaphore_t mutex;
static const uint8_t compass_slave_address = 0xA0;
static const uint8_t compass_read_command = 0x3D;
static uint16_t compass_offset = 0;
static const uint16_t COMPASS_MAX_VAL = 359;
#define COMPASS_I2C_READ_SIZE 6

static uint16_t heading_degrees;

void compass__init(void) {
  uint32_t p_clock = clock__get_peripheral_clock_hz();
  i2c__initialize(compass_i2c, 1e5, p_clock, &sem, &mutex);
  fprintf(stderr, "compass initialized\n");
}

void stuff_compass_fields(uint8_t *bytes, int16_t *val) {
  // vals->x = (0xff00 & (bytes[1] << 8)) | bytes[0];
  // vals->y = (0xff00 & (bytes[3] << 8)) | bytes[2];
  *val = (0xff00 & (bytes[5] << 8)) | bytes[4];
}

uint16_t convert_to_unsigned_degrees(float signed_degrees) {
  uint16_t unsigned_degrees = 0;
  if (signed_degrees < 0) {
    unsigned_degrees = signed_degrees * -1;
  } else {
    unsigned_degrees = COMPASS_MAX_VAL - (int16_t)signed_degrees;
  }
  return unsigned_degrees;
}

uint16_t get_heading_with_offset(uint16_t heading_without_offset) {
  uint16_t heading_with_offset = heading_without_offset + compass_offset;
  if (heading_with_offset > COMPASS_MAX_VAL) {
    heading_with_offset -= COMPASS_MAX_VAL;
  }
  return heading_with_offset;
}

void compass__update_heading_10Hz(void) {
  uint8_t raw_data[COMPASS_I2C_READ_SIZE] = {0};
  // compass__raw_vals_t fields = {0};
  int16_t signed_val = 0;
  i2c__read_slave_data(compass_i2c, compass_slave_address, compass_read_command, raw_data, COMPASS_I2C_READ_SIZE);
  stuff_compass_fields(raw_data, &signed_val);

  /// ref: WT901 datasheet 5.1.4 pg12
  float heading_degrees_signed = ((float)signed_val / 32768) * 180;
  // N=0; E=90; S=180; W=270;
  uint16_t raw_heading = convert_to_unsigned_degrees(heading_degrees_signed);
  heading_degrees = get_heading_with_offset(raw_heading);
}

uint16_t compass__get_heading(void) { return heading_degrees; }

void compass__update_offset(void) { compass_offset = heading_degrees; }
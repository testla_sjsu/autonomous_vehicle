#include "can_handler.h"
#include "board_io.h"
#include "can_bus.h"
#include "geo.h"
#include "gpio.h"
#include "gps.h"

const uint32_t dbc_mia_threshold_SENSOR_DESTINATION_LOCATION = 500;
const dbc_SENSOR_DESTINATION_LOCATION_s dbc_mia_replacement_SENSOR_DESTINATION_LOCATION;
static dbc_SENSOR_DESTINATION_LOCATION_s can_msg__dest;
static can__num_e geo_can_num = can1;

void can_handler__init(void) {
  if (!can__init(geo_can_num, 500, 1024, 1024, NULL, NULL)) {
    fprintf(stderr, "Failed to init CAN\n");
  } else {
    // fprintf(stderr, "CAN running\n");
  }
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(geo_can_num);
  gpio__set(board_io__get_led1());
  gpio__set(board_io__get_led2());
}

void can_handler__manage_mia_10Hz(void) {
  if (dbc_service_mia_SENSOR_DESTINATION_LOCATION(&can_msg__dest, mia_increment_value)) {
    gpio__reset(board_io__get_led2());
  }
}

bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc) {
  can__msg_t tx_msg = {0};
  tx_msg.msg_id = message_id;
  tx_msg.frame_fields.data_len = dlc;
  for (int i = 0; i < 8; i++) {
    tx_msg.data.bytes[i] = bytes[i];
  }
  return can__tx(geo_can_num, &tx_msg, 0);
}

void can_handler__transmit_debug_messages_1Hz(void) {
  gps__coordinates_t debug_gps = gps__get_coordinates();
  uint8_t fix = gps__get_fix_status();
  dbc_DEBUG_GPS_CURRENT_LOCATION_s can_msg__debug_gps = {
      .DEBUG_GPS_CURRENT_LOCATION_latitude = debug_gps.lat_rad,
      .DEBUG_GPS_CURRENT_LOCATION_longitude = debug_gps.lon_rad,
      .DEBUG_GPS_CURRENT_LOCATION_fix = fix,
  };
  dbc_encode_and_send_DEBUG_GPS_CURRENT_LOCATION(NULL, &can_msg__debug_gps);

  gps__update_stats_t debug_gps_update = gps__get_stats();
  dbc_DEBUG_GEO_GPS_UPDATE_s can_msg__debug_gps_update = {
      .DEBUG_GEO_GPS_UPDATE_count = debug_gps_update.counter,
      .DEBUG_GEO_GPS_UPDATE_max_period = debug_gps_update.max_period_ms,
      .DEBUG_GEO_GPS_UPDATE_min_period = debug_gps_update.min_period_ms,
      .DEBUG_GEO_GPS_UPDATE_average_period = debug_gps_update.average_period_ms,
  };
  dbc_encode_and_send_DEBUG_GEO_GPS_UPDATE(NULL, &can_msg__debug_gps_update);
}

void can_handler__transmit_status_10Hz(void) {
  dbc_GEO_STATUS_s can_msg__geo_status = {0};
  geo__get_status(&can_msg__geo_status);

  can__msg_t can_msg_dist = {0};
  const dbc_message_header_t header_dist = dbc_encode_GEO_STATUS(can_msg_dist.data.bytes, &can_msg__geo_status);
  can_msg_dist.msg_id = header_dist.message_id;
  can_msg_dist.frame_fields.data_len = header_dist.message_dlc;
  can__tx(geo_can_num, &can_msg_dist, 0);
}

void can_handler__handle_all_incoming_messages(void) {
  can__msg_t can_msg = {};

  while (can__rx(geo_can_num, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    if (dbc_decode_SENSOR_DESTINATION_LOCATION(&can_msg__dest, header, can_msg.data.bytes)) {
      geo__update_dest(&can_msg__dest);
    }
    gpio__toggle(board_io__get_led1());
  }
}

void can_handler__health_check(void) {
  if (can__is_bus_off(geo_can_num)) {
    fprintf(stderr, "RESET CAN\n");
    can__reset_bus(geo_can_num);
  }
}

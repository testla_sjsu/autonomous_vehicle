#include "line_buffer.h"
#include <stdio.h>
#include <string.h>

static void line_buffer__delete_from_buffer(line_buffer_s *buffer, size_t count, size_t tail) {
  buffer->count -= count;

  if (tail == buffer->max_size) {
    tail = 0;
  }
  buffer->tail = tail;
}

void line_buffer__init(line_buffer_s *buffer, char *memory, size_t size) {
  if (buffer != NULL) {
    memset(buffer, 0, sizeof(line_buffer_s));

    if (memory != NULL) {
      buffer->memory = (char *)memory;
      buffer->max_size = size;
    }
  }
}
bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {
  bool pushed = false;
  if (buffer != NULL) {
    if (buffer->count < buffer->max_size) {
      buffer->memory[buffer->head] = byte;
      ++buffer->count;
      ++buffer->head;

      if (buffer->head == buffer->max_size) {
        buffer->head = 0;
      }
      pushed = true;
    }
  }
  return pushed;
}

bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {
  size_t count = 0;
  size_t tail = buffer->tail;
  bool line_removed = false;
  while (count < buffer->count && count < line_max_size) {
    if (buffer->memory[tail] == '\n') {
      ++count;
      ++tail;
      line_buffer__delete_from_buffer(buffer, count, tail);
      line_removed = true;
      break;
    } else {
      line[count] = buffer->memory[tail];
      ++count;
      ++tail;
      if (tail == buffer->max_size) {
        tail = 0;
      }
    }
  }
  if (count == buffer->max_size && !line_removed) {
    line_buffer__delete_from_buffer(buffer, count, tail);
    line[count - 1] = '\0';
    line_removed = true;
  }
  return line_removed;
}
#pragma once
#include <stdint.h>

union raw_val_union {
  uint16_t unsigned_val;
  int16_t signed_val;
};

typedef struct {
  int16_t x;
  int16_t z;
  int16_t y;
} compass__raw_vals_t;

void compass__init(void);
void compass__update_heading_10Hz(void);
uint16_t compass__get_heading(void);
void compass__update_offset(void);

#pragma once
#include <stdbool.h>

typedef struct {
  float lat_rad;
  float lon_rad;
} gps__coordinates_t;

typedef struct {
  uint16_t counter;
  uint16_t max_period_ms;
  uint16_t min_period_ms;
  uint16_t average_period_ms;
} gps__update_stats_t;

void gps__init(void);
void gps__run_once(uint32_t tick_10Hz);
void gps__debug_dump(void);
bool gps__send_setup(void);
gps__coordinates_t gps__get_coordinates(void);
uint8_t gps__get_fix_status(void);
gps__update_stats_t gps__get_stats(void);
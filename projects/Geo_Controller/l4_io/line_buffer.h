#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct {
  char *memory;
  size_t max_size;
  size_t head;
  size_t tail;
  size_t count;
} line_buffer_s;

void line_buffer__init(line_buffer_s *buffer, char *memory, size_t size);
bool line_buffer__add_byte(line_buffer_s *buffer, char byte);
bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size);

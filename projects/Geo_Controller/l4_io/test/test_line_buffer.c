#include "line_buffer.h"
#include "unity.h"
#include <string.h>

static line_buffer_s line_buffer;
static char memory[8];

void setup(void) {}

void teardown(void) {}

static void add_to_line_buffer(const char *string) {
  for (size_t i = 0; i < strlen(string); i++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, string[i]));
  }
}

void test_line_buffer__nominal_case(void) {
  line_buffer__init(&line_buffer, memory, sizeof(memory));
  add_to_line_buffer("abc\n");

  char line[8] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
  TEST_ASSERT_EQUAL_STRING("abc", line);
}

void test_line_buffer__incomplete_line(void) {
  add_to_line_buffer("xy");

  char line[8] = {0};
  TEST_ASSERT_FALSE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));

  TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, '\n'));
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
  TEST_ASSERT_EQUAL_STRING("xy", line);
}

void test_line_buffer__slash_r_slash_n_case(void) {
  add_to_line_buffer("ab\r\n");

  char line[8] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
  TEST_ASSERT_EQUAL_STRING("ab\r", line);
}

void test_line_buffer__multiple_lines(void) {
  add_to_line_buffer("ab\ncd\n");

  char line[8] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
  TEST_ASSERT_EQUAL_STRING("ab", line);

  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
  TEST_ASSERT_EQUAL_STRING("cd", line);
}

void test_line_buffer__overflow_case(void) {
  for (size_t i = 0; i < sizeof(memory); i++) {
    TEST_ASSERT_TRUE(line_buffer__add_byte(&line_buffer, 'a' + i));
  }

  TEST_ASSERT_FALSE(line_buffer__add_byte(&line_buffer, 'b'));

  char line[8] = {0};
  TEST_ASSERT_TRUE(line_buffer__remove_line(&line_buffer, line, sizeof(line)));
  TEST_ASSERT_EQUAL_STRING("abcdefg", line);
}
#include "Mockclock.h"
#include "Mocki2c.h"
#include "compass.h"
#include "unity.h"

void test_compass__init(void) {
  clock__get_peripheral_clock_hz_ExpectAndReturn(0);

  i2c__initialize_Expect(0, 0, 0, NULL, NULL);
  i2c__initialize_IgnoreArg_i2c_number();
  i2c__initialize_IgnoreArg_desired_i2c_bus_speed_in_hz();
  i2c__initialize_IgnoreArg_peripheral_clock_hz();
  i2c__initialize_IgnoreArg_binary_sem_struct();
  i2c__initialize_IgnoreArg_mutex_struct();

  compass__init();
}

void test_compass__update_heading_10Hz(void) {
  i2c__read_slave_data_ExpectAndReturn(0, 0, 0, NULL, 0, true);
  i2c__read_slave_data_IgnoreArg_i2c_number();
  i2c__read_slave_data_IgnoreArg_slave_address();
  i2c__read_slave_data_IgnoreArg_starting_slave_memory_address();
  i2c__read_slave_data_IgnoreArg_bytes_to_read();
  i2c__read_slave_data_IgnoreArg_number_of_bytes();

  compass__update_heading_10Hz();
}

void test_compass__get_heading(void) {
  static uint8_t raw_compass_bytes[6] = {0x9C, 0x82, 0x28, 0xFF, 0xE6, 0x24};
  i2c__read_slave_data_ExpectAndReturn(0, 0, 0, raw_compass_bytes, 0, true);
  i2c__read_slave_data_IgnoreArg_i2c_number();
  i2c__read_slave_data_IgnoreArg_slave_address();
  i2c__read_slave_data_IgnoreArg_starting_slave_memory_address();
  i2c__read_slave_data_IgnoreArg_bytes_to_read();
  i2c__read_slave_data_IgnoreArg_number_of_bytes();
  i2c__read_slave_data_ReturnMemThruPtr_bytes_to_read(raw_compass_bytes, 6);

  compass__update_heading_10Hz();
  TEST_ASSERT_EQUAL(308, compass__get_heading());
}
#include "unity.h"
#include <stdio.h>
#include <string.h>

#include "Mockclock.h"
#include "Mockqueue.h"
#include "Mockuart.h"

#include "Mockboard_io.h"
#include "Mockgpio.h"

#include "gps.h"
#include "line_buffer.h"

void setup(void) {}
void teardown(void) {}

void test_gps__init(void) {
  clock__get_peripheral_clock_hz_ExpectAndReturn(0);

  uart__init_Expect(0, 0, 0);
  uart__init_IgnoreArg_uart();
  uart__init_IgnoreArg_peripheral_clock();
  uart__init_IgnoreArg_baud_rate();

  QueueHandle_t dummy_handle = NULL;
  xQueueCreate_ExpectAndReturn(0, 0, dummy_handle);
  xQueueCreate_IgnoreArg_uxQueueLength();
  xQueueCreate_IgnoreArg_uxItemSize();
  xQueueCreate_ExpectAndReturn(0, 0, dummy_handle);
  xQueueCreate_IgnoreArg_uxQueueLength();
  xQueueCreate_IgnoreArg_uxItemSize();

  uart__enable_queues_ExpectAndReturn(0, 0, 0, true);
  uart__enable_queues_IgnoreArg_uart();
  uart__enable_queues_IgnoreArg_queue_receive();
  uart__enable_queues_IgnoreArg_queue_transmit();

  gps__init();
}

void test_GPGLL_line_is_ignored(void) {
  char *uart_driver_returned_data = "$GPGLL,210230,3855.4487,N,09446.0071,W,1,07,1.1,370.5,M,-29.5,M,,*7A\n";
  for (int i = 0; i < strlen(uart_driver_returned_data); i++) {
    uart__get_ExpectAndReturn(0, NULL, 0, true);
    uart__get_IgnoreArg_uart();
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte((char *)&uart_driver_returned_data[i]);
  }
  uart__get_ExpectAndReturn(0, NULL, 0, false);
  uart__get_IgnoreArg_uart();
  uart__get_IgnoreArg_input_byte();
  uart__get_IgnoreArg_timeout_ms();

  gpio_s gpio = {};
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__set_Expect(gpio);

  gps__run_once(0);
  gps__coordinates_t vals = {0};
  vals = gps__get_coordinates();
  TEST_ASSERT_EQUAL(0, vals.lat_rad);
  TEST_ASSERT_EQUAL(0, vals.lon_rad);
}

void test_GPGGA_coordinates_are_parsed(void) {
  const char *uart_driver_returned_data = "$GPGGA,210230,3855.4487,N,9446.0071,W,1,07,1.1,370.5,M,-29.5,M,,*7A\n";
  for (int i = 0; i < strlen(uart_driver_returned_data); i++) {
    uart__get_ExpectAndReturn(0, NULL, 0, true);
    uart__get_IgnoreArg_uart();
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte((char *)&uart_driver_returned_data[i]);
  }
  uart__get_ExpectAndReturn(0, NULL, 0, false);
  uart__get_IgnoreArg_uart();
  uart__get_IgnoreArg_input_byte();
  uart__get_IgnoreArg_timeout_ms();

  gpio_s gpio = {};
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  gps__run_once(0);
  gps__coordinates_t vals = {0};
  vals = gps__get_coordinates();
  uint8_t fix = gps__get_fix_status();
  TEST_ASSERT_EQUAL((float)(38.0f + (55.4487f / 60.0f)), vals.lat_rad);
  TEST_ASSERT_EQUAL((float)-(94.0f + (46.0071f / 60.0f)), vals.lon_rad);
  TEST_ASSERT_EQUAL(1, fix);
}

void test_newest_data_read(void) {
  const char *uart_driver_returned_data =
      "$GPGGA,210230,3855.4487,N,9446.0071,W,1,07,1.1,370.5,M,-29.5,M,,*7A\n$GPGGA,210230,4077.4487,N,9455.0071,W,1,07,"
      "1.1,370.5,M,-29.5,M,,*7A\n";
  for (int i = 0; i < strlen(uart_driver_returned_data); i++) {
    uart__get_ExpectAndReturn(0, NULL, 0, true);
    uart__get_IgnoreArg_uart();
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte((char *)&uart_driver_returned_data[i]);
  }
  uart__get_ExpectAndReturn(0, NULL, 0, false);
  uart__get_IgnoreArg_uart();
  uart__get_IgnoreArg_input_byte();
  uart__get_IgnoreArg_timeout_ms();

  gpio_s gpio = {};
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  gps__run_once(0);
  gps__coordinates_t vals = {0};
  vals = gps__get_coordinates();
  uint8_t fix = gps__get_fix_status();
  TEST_ASSERT_EQUAL((float)(40.0f + (77.4487f / 60.0f)), vals.lat_rad);
  TEST_ASSERT_EQUAL((float)-(94.0f + (55.0071f / 60.0f)), vals.lon_rad);
  TEST_ASSERT_EQUAL(1, fix);
}

void test_GPGGA_split_line(void) {
  const char *uart_driver_returned_data_first_half = "$GPGGA,210230,8265.4487,N,2333.0071,W,1,07,";
  for (int i = 0; i < strlen(uart_driver_returned_data_first_half); i++) {
    uart__get_ExpectAndReturn(0, NULL, 0, true);
    uart__get_IgnoreArg_uart();
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte((char *)&uart_driver_returned_data_first_half[i]);
  }
  uart__get_ExpectAndReturn(0, NULL, 0, false);
  uart__get_IgnoreArg_uart();
  uart__get_IgnoreArg_input_byte();
  uart__get_IgnoreArg_timeout_ms();

  gpio_s gpio = {};
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  gps__run_once(0);
  gps__coordinates_t vals = {0};
  vals = gps__get_coordinates();
  TEST_ASSERT_NOT_EQUAL((float)(82.0f + (65.4487f / 60.0f)), vals.lat_rad);
  TEST_ASSERT_NOT_EQUAL((float)-(23.0f + (33.0071f / 60.0f)), vals.lon_rad);

  const char *uart_driver_returned_data_second_half = "1.1,370.5,M,-29.5,M,,*7A\n";
  for (int i = 0; i < strlen(uart_driver_returned_data_second_half); i++) {
    uart__get_ExpectAndReturn(0, NULL, 0, true);
    uart__get_IgnoreArg_uart();
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte((char *)&uart_driver_returned_data_second_half[i]);
  }

  uart__get_ExpectAndReturn(0, NULL, 0, false);
  uart__get_IgnoreArg_uart();
  uart__get_IgnoreArg_input_byte();
  uart__get_IgnoreArg_timeout_ms();

  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  gps__run_once(0);
  vals = gps__get_coordinates();
  TEST_ASSERT_EQUAL((float)(82.0f + (65.4487f / 60.0f)), vals.lat_rad);
  TEST_ASSERT_EQUAL((float)-(23.0f + (33.0071f / 60.0f)), vals.lon_rad);
}

void test_GPGGA_incomplete_last_line(void) {
  const char *uart_driver_returned_data = "$GPGGA,210230,4855.4487,N,2746.0071,W,1,07,1.1,370.5,M,-29.5,M,,*7A\n$GPGGA,"
                                          "210230,4077.4487,N,9455.0071,W,1,07,";
  for (int i = 0; i < strlen(uart_driver_returned_data); i++) {
    uart__get_ExpectAndReturn(0, NULL, 0, true);
    uart__get_IgnoreArg_uart();
    uart__get_IgnoreArg_input_byte();
    uart__get_IgnoreArg_timeout_ms();
    uart__get_ReturnThruPtr_input_byte((char *)&uart_driver_returned_data[i]);
  }
  uart__get_ExpectAndReturn(0, NULL, 0, false);
  uart__get_IgnoreArg_uart();
  uart__get_IgnoreArg_input_byte();
  uart__get_IgnoreArg_timeout_ms();

  gpio_s gpio = {};
  board_io__get_led3_ExpectAndReturn(gpio);
  gpio__reset_Expect(gpio);

  gps__run_once(0);
  gps__coordinates_t vals = {0};
  vals = gps__get_coordinates();
  uint8_t fix = gps__get_fix_status();
  TEST_ASSERT_EQUAL((float)(48.0f + (55.4487f / 60.0f)), vals.lat_rad);
  TEST_ASSERT_EQUAL((float)-(27.0f + (55.0071f / 60.0f)), vals.lon_rad);
  TEST_ASSERT_EQUAL(1, fix);
}


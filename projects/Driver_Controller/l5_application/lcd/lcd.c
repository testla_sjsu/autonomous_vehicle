#include "lcd.h"

/*********************************************************************************************************************
 * Locally defined Data variables, structs, and enumerations used in the driver logic
 ********************************************************************************************************************/
// // declarations and storage of all data for these structs locally
// // structs to store data received from other controllers

// rx controller status messages
driver_geo_status_s *lcd__rx_geo_controller;
driver_encoder_status_s *lcd__rx_motor_controller;
driver_sonar_sensors_status_s *lcd__rx_sensor_controller;
// rx controller status messages
driver_geo_coordinates_s *lcd__rx_car_coordinates;
driver_geo_coordinates_s *lcd__rx_final_dest_coordinates;
driver_checkpoint_message_s *lcd__rx_checkpoint_message;
driver_navigation_message_s *lcd__rx_navigation_message;
// tx driver messages
driver_steering_and_velocity_s *lcd__tx_to_motor_controller;

/*********************************************************************************************************************
 * LCD debug print functions
 ********************************************************************************************************************/
void lcd__init(void) {
  lcd__copy_all_of_the_pointers_to_rx_and_tx_can_data_stored_in_driver_logic();

  uint8_t LCD_slave = 0x4E;
  uint8_t LCD_Max_collum = 20;
  uint8_t LCD_Max_row = 4;

  LiquidCrystal_I2C(LCD_slave, LCD_Max_collum, LCD_Max_row); // initialize LCD
  LCD_init();
  backlight();

  lcd__print_turn_on_sequence(); // Display our names in the turn on sequence :)
}

void lcd__printf_lcd_data_struct_pointers(void) {
  printf("lcd__rx_car_coordinates        %x\n\r", lcd__rx_car_coordinates);
  printf("lcd__rx_final_dest_coordinates %x\n\r", lcd__rx_final_dest_coordinates);
  printf("lcd__rx_checkpoint_message     %x\n\r", lcd__rx_checkpoint_message);
  printf("lcd__rx_navigation_message     %x\n\r", lcd__rx_navigation_message);
  printf("lcd__rx_geo_controller         %x\n\r", lcd__rx_geo_controller);
  printf("lcd__rx_motor_controller       %x\n\r", lcd__rx_motor_controller);
  printf("lcd__rx_sensor_controller      %x\n\r", lcd__rx_sensor_controller);
  printf("lcd__tx_to_motor_controller    %x\n\n\r", lcd__tx_to_motor_controller);
}

void lcd__copy_all_of_the_pointers_to_rx_and_tx_can_data_stored_in_driver_logic(void) {
  lcd__rx_car_coordinates = ret_cpy_of_rx_car_coordinates();
  lcd__rx_final_dest_coordinates = ret_cpy_of_rx_final_dest_coordinates();
  lcd__rx_checkpoint_message = ret_cpy_of_rx_checkpoint_message();
  lcd__rx_navigation_message = ret_cpy_of_rx_navigation_message();

  lcd__rx_geo_controller = ret_cpy_of_rx_geo_controller();
  lcd__rx_motor_controller = ret_cpy_of_rx_motor_controller();
  lcd__rx_sensor_controller = ret_cpy_of_rx_sensor_controller();

  lcd__tx_to_motor_controller = ret_cpy_of_tx_to_motor_controller();
}

// void driver__lcd_update_information(lcd_splash_screen_e lcd_splash_screen_number) {
//   static prev_lcd_splash_screen_number;

//   switch (lcd_splash_screen_number) {
//   case turn_on_sequence:
//     driver__lcd_print_turn_on_sequence();
//     break;
//   case lcd_info_splash_1:
//     driver__lcd_print_updated_sensor_info_screen_1();
//     if (prev_lcd_splash_screen_number != lcd_splash_screen_number) {
//       driver__lcd_setup_for_lcd_info_screen_number_1();
//     }
//     driver__lcd_print_updated_sensor_info_screen_1();
//     break;
//   case 2:
//     driver__lcd_print_updated_sensor_info_screen_2();
//     break;
//   }

//   // Set the prev value to the new value so we can print off the necessary background characters n the lcd splash
//   screen
//   // that we need to display next
//   prev_lcd_splash_screen_number = lcd_splash_screen_number;
//   return;
// }

void lcd__print_turn_on_sequence(void) {
  char lcd_info_splash_1_line_0[] = "       TESTLA       ";
  char lcd_info_splash_1_line_1[] = "Devin A      Scott L";
  char lcd_info_splash_1_line_2[] = "Thinh L       Bang N";
  char lcd_info_splash_1_line_3[] = "Mike H       Sinan B";
  // printstr_lcd(&ptr_input, row, collum);
  printstr_lcd(&lcd_info_splash_1_line_0, 0, 0);
  printstr_lcd(&lcd_info_splash_1_line_1, 1, 0);
  printstr_lcd(&lcd_info_splash_1_line_2, 2, 0);
  printstr_lcd(&lcd_info_splash_1_line_3, 3, 0);
}

void lcd__print_updated_sensor_info_screen_1(void) {

  const uint8_t current_coordinates_lcd_row_number = 0;
  const uint8_t speed_lcd_row_number = 1;
  const uint8_t heading_bearing_and_dist_to_destination_lcd_row_number = 2;
  const uint8_t sonar_sensor_lcd_row_number = 3;

  lcd__print_line_filled_with_sonar_sensor_data(sonar_sensor_lcd_row_number);
  lcd__print_line_filled_with_geo_status_data(heading_bearing_and_dist_to_destination_lcd_row_number);
  bool display_encoder_speed = false;
  bool display_checkpoint_data = true;
  lcd__print_line_filled_with_speed_and_checkpoint_data(speed_lcd_row_number, display_encoder_speed,
                                                        display_checkpoint_data, lcd__rx_navigation_message,
                                                        lcd__rx_checkpoint_message);
  lcd__print_line_filled_with_coordinates_data(current_coordinates_lcd_row_number);
}

/*********************************************************************************************************************
 * LCD Print line functions
 ********************************************************************************************************************/
void lcd__print_line_filled_with_coordinates_data(uint8_t lcd_row_num) {
  const uint8_t num_chars = 21; // Needs to be one larger than desired output length to attach NULL at end
  char complete_coordinates_line[20];
  char latitude_string[9];
  char longitude_string[10];
  float latitude_value = lcd__rx_car_coordinates->latitude;
  float longitude_value = lcd__rx_car_coordinates->longitude;
  lcd__helper_nine_char_latitude_data_string(latitude_string, latitude_value);
  lcd__helper_ten_char_longitude_data_string(longitude_string, longitude_value);
  snprintf(complete_coordinates_line, num_chars, "%s,%s", latitude_string, longitude_string);
  vTaskDelay(1); // delay to allow all buffers to be filled with data to avoid race conditions
  uint8_t lcd_col_num = 0;
  printstr_lcd(&complete_coordinates_line, lcd_row_num, lcd_col_num);
}

void lcd__print_line_filled_with_speed_and_checkpoint_data(uint8_t lcd_row_num, bool display_encoder_speed,
                                                           bool display_checkpoint_data,
                                                           driver_checkpoint_message_s *checkpoint_message,
                                                           driver_navigation_message_s *navigation_message) {
  const uint8_t num_chars = 21; // Needs to be one larger than desired output length to attach NULL at end
  char complete_speed_and_checkpoint_info_line[20];
  char first_half_of_row[10];
  char second_half_of_row[10];

  // First half of the row:
  if (display_encoder_speed) { // for wheel encoder speed use:
    // printf("Encoder\n\r");
    lcd__helper_ten_char_encoder_speed_string(first_half_of_row, lcd__rx_motor_controller->current_encoder_velocity);
  } else { // for speed limit using the set speed by driver use:
    // printf("Driver\n\r");
    lcd__helper_ten_char_driver_speed_string(first_half_of_row, lcd__tx_to_motor_controller->current_velocity);
  }
  //   printf("\n\rENCODER_SPEED:%s:\n\r", car_encoder_speed_data_without_unit);
  //   printf("DRIVER_SPEED:%s:\n\r", car_driver_set_speed_data_without_unit);

  // Second half of the row:
  lcd__helper_ten_char_navigation_or_checkpoint_message_string(second_half_of_row, display_checkpoint_data,
                                                               checkpoint_message, navigation_message);

  // Assemble the two strings first_half_of_row and second_half_of_row together
  snprintf(complete_speed_and_checkpoint_info_line, num_chars, "%s%s", first_half_of_row, second_half_of_row);
  //   snprintf(complete_speed_and_checkpoint_info_line, num_chars, "          %s", second_half_of_row);
  //   memcpy(complete_speed_and_checkpoint_info_line, first_half_of_row, sizeof(first_half_of_row));

  //   for (int i = 3; i < 10; i++) {
  //     complete_speed_and_checkpoint_info_line[i] = first_half_of_row[i];
  //   }

  uint8_t lcd_col_num = 0;
  //   printf("\n1st half:%s:\n\r", first_half_of_row);
  //   printf("2nd half:%s:\n\r", second_half_of_row);
  //   printf("FULL STRING:%s:\n\n\r", complete_speed_and_checkpoint_info_line);

  vTaskDelay(2); // delay to allow all buffers to be filled with data to avoid race conditions

  printstr_lcd(&complete_speed_and_checkpoint_info_line, lcd_row_num, lcd_col_num);
}

void lcd__print_line_filled_with_geo_status_data(uint8_t lcd_row_num) {
  const uint8_t num_chars = 21; // Needs to be one larger than desired output length to attach NULL at end
  char complete_geo_status_line[20];
  char car_heading[5];
  char dest_bearing[5];
  char distance_to_checkpoint[3];

  lcd__helper_heading_or_bearing_in_3_digit_number_string_with_degree_symbol(
      car_heading, lcd__rx_geo_controller->current_compass_heading);
  lcd__helper_heading_or_bearing_in_3_digit_number_string_with_degree_symbol(
      dest_bearing, lcd__rx_geo_controller->current_destination_bearing);
  lcd__helper_distance_to_checkpoint_in_3_digit_number_string(distance_to_checkpoint,
                                                              lcd__rx_geo_controller->current_distance_destination);
  // concatanate all of the strings for the lattitude and longitude to allow it to print in a nice pleasing way
  snprintf(complete_geo_status_line, num_chars, "H:%sB:%sDIST:%s", car_heading, dest_bearing, distance_to_checkpoint);

  vTaskDelay(1); // delay to allow all buffers to be filled with data to avoid race conditions

  uint8_t lcd_col_num = 0;
  printstr_lcd(&complete_geo_status_line, lcd_row_num, lcd_col_num);
}

void lcd__print_line_filled_with_sonar_sensor_data(uint8_t lcd_row_num) {
  const uint8_t num_chars = 20; // Needs to be one larger than desired output length to attach NULL at end
  char complete_sensor_line[19];
  char left_sensor[6];   // Needs to be one larger than desired output length to attach NULL at end
  char middle_sensor[6]; // Needs to be one larger than desired output length to attach NULL at end
  char right_sensor[6];  // Needs to be one larger than desired output length to attach NULL at end
  char back_sensor[5];   // Doesnt apply bc the last col will always be empty in the sensor line
  lcd__helper_five_char_sonar_sensor_data_string(left_sensor, "L", lcd__rx_sensor_controller->left_sensor);
  lcd__helper_five_char_sonar_sensor_data_string(middle_sensor, "M", lcd__rx_sensor_controller->middle_sensor);
  lcd__helper_five_char_sonar_sensor_data_string(right_sensor, "R", lcd__rx_sensor_controller->right_sensor);
  lcd__helper_five_char_sonar_sensor_data_string(back_sensor, "B", lcd__rx_sensor_controller->back_sensor);
  // concatanate all of the strings for the 4 sonar sensors to allow it to print in a nice pleasing way
  snprintf(complete_sensor_line, num_chars, "%s%s%s%s", left_sensor, middle_sensor, right_sensor, back_sensor);
  // clean up the spaces in between the 4 sensors details
  const char ascii_space = 0x20;
  complete_sensor_line[4] = ascii_space;
  complete_sensor_line[9] = ascii_space;
  complete_sensor_line[14] = ascii_space;
  complete_sensor_line[19] = ascii_space;
  //   vTaskDelay(1); // delay to allow all buffers to be filled with data to avoid race conditions
  uint8_t lcd_col_num = 0;
  printstr_lcd(&complete_sensor_line, lcd_row_num, lcd_col_num);
}

// TODO: Make all of these static
/*********************************************************************************************************************
 * LCD helper functions
 ********************************************************************************************************************/
void lcd__helper_ten_char_encoder_speed_string(char *ret_encoder_speed_string, float encoder_speed_value) {
  const uint8_t num_chars = 11;          // Needs to be one larger than desired output length to attach NULL at end
  char car_encoder_speed_data_string[5]; // two digts left of decimal with one digit precision
  lcd__helper_return_four_char_kph_speed_data_string_for_printout(car_encoder_speed_data_string, encoder_speed_value);
  printf("car_encoder_speed_data_string:%s:\n\r", car_encoder_speed_data_string);
  snprintf(ret_encoder_speed_string, num_chars, "E:    kph ", car_encoder_speed_data_string);
  ret_encoder_speed_string[2] = car_encoder_speed_data_string[0];
  ret_encoder_speed_string[3] = car_encoder_speed_data_string[1];
  ret_encoder_speed_string[4] = car_encoder_speed_data_string[2];
  ret_encoder_speed_string[5] = car_encoder_speed_data_string[3];
  printf("lcd__helper_ret_encoder_speed_string:%s:\n\r", ret_encoder_speed_string);
}

void lcd__helper_ten_char_driver_speed_string(char *ret_driver_speed_string, float driver_speed_value) {
  const uint8_t num_chars = 11;             // Needs to be one larger than desired output length to attach NULL at end
  char car_driver_set_speed_data_string[5]; // two digts left of decimal with one digit precision
  lcd__helper_return_four_char_kph_speed_data_string_for_printout(car_driver_set_speed_data_string, driver_speed_value);
  snprintf(ret_driver_speed_string, num_chars, "S:    kph ", car_driver_set_speed_data_string);
  ret_driver_speed_string[2] = car_driver_set_speed_data_string[0];
  ret_driver_speed_string[3] = car_driver_set_speed_data_string[1];
  ret_driver_speed_string[4] = car_driver_set_speed_data_string[2];
  ret_driver_speed_string[5] = car_driver_set_speed_data_string[3];
  printf("lcd__helper_ten_char_driver_speed_string:%s:\n\r", ret_driver_speed_string);
}

void lcd__helper_ten_char_navigation_or_checkpoint_message_string(char *ret_navigation_string,
                                                                  bool display_checkpoint_data,
                                                                  driver_checkpoint_message_s *checkpoint_message,
                                                                  driver_navigation_message_s *navigation_message) {
  const uint8_t num_chars = 11; // Needs to be one larger than desired output length to attach NULL at end
  // Other buffers that hold const messages to be copied into second half of buffer for printing
  char msg_navigation_currently_not_in_progress[11] = "NAV: Ready";
  char msg_navigation_in_progress[11] = "NAV:InProg";
  char msg_navigation_just_finished[11] = "NAV:Finish";
  char msg_navigation_error[11] = "NAV: ERROR";

  navigation_status_e navigation_status_switch;
  bool display_checkpoint_status_instead_of_nav_inprog;
  //   display_checkpoint_status_instead_of_nav_inprog = display_checkpoint_data;  // uncomment after testing
  display_checkpoint_status_instead_of_nav_inprog = true;
  //   navigation_status_switch = navigation_message->navigation_status; // uncomment after testing
  navigation_status_switch = navigation_in_progress;

  switch (navigation_status_switch) {
  // Display "NAV: Ready" in second half of the line
  case navigation_currently_not_in_progress:
    snprintf(ret_navigation_string, num_chars, "%s", msg_navigation_currently_not_in_progress);
    break;
  // Display "NAV:InProg" in second half of the line
  case navigation_in_progress:
    if (display_checkpoint_status_instead_of_nav_inprog) {
      // Checkpoint num buffers used to display the current checkpoint information
      char curr_checkpoint_string[3];  // two digts max
      char total_checkpoint_string[3]; // two digts max
      uint8_t curr_checkpoint_number;
      uint8_t total_checkpoint_number;
      //   curr_checkpoint_number = checkpoint_message->curr_checkpoint_number;  // uncomment after testing
      curr_checkpoint_number = 3;
      //   total_checkpoint_number = checkpoint_message->total_checkpoint_number;  // uncomment after testing
      total_checkpoint_number = 14;
      lcd__helper_checkpoint_number_in_2_digit_number_string(curr_checkpoint_string, curr_checkpoint_number);
      lcd__helper_checkpoint_number_in_2_digit_number_string(total_checkpoint_string, total_checkpoint_number);
      snprintf(ret_navigation_string, num_chars, "CHK:%sof%s", curr_checkpoint_string, total_checkpoint_string);
    } else {
      snprintf(ret_navigation_string, num_chars, "%s", msg_navigation_in_progress);
    }
    break;
  // Display "NAV:Finish" in second half of the line
  case navigation_just_finished:
    snprintf(ret_navigation_string, num_chars, "%s", msg_navigation_just_finished);
    break;
  // Display "NAV: ERROR" in second half of the line
  case navigation_error:
    snprintf(ret_navigation_string, num_chars, "%s", msg_navigation_error);
    break;
  }
  return;
}

void lcd__helper_nine_char_latitude_data_string(char *ret_latitude_string, float latitude_value) {
  const uint8_t num_chars = 10; // Needs to be one larger than desired output length to attach NULL at end
  // remove "-" sign because we are going to be in the same quadrant of the earth for all of the testing :)
  if (latitude_value < 0) {
    latitude_value *= -1.0;
  }
  // cast the coordinate float data as strings with 6 points of decimal precision for proper printout
  if (latitude_value < 10) {
    snprintf(ret_latitude_string, num_chars, " %.6f", latitude_value);
  } else if ((latitude_value >= 10) && (latitude_value < 100)) {
    snprintf(ret_latitude_string, num_chars, "%.6f", latitude_value);
  }
  return;
}

void lcd__helper_ten_char_longitude_data_string(char *ret_longitude_string, float longitude_value) {
  const uint8_t num_chars = 11; // Needs to be one larger than desired output length to attach NULL at end
  // remove "-" sign because we are going to be in the same quadrant of the earth for all of the testing :)
  if (longitude_value < 0) {
    longitude_value *= -1.0;
  }
  // cast the coordinate float data as strings with 6 points of decimal precision for proper printout
  if (longitude_value < 10) {
    snprintf(ret_longitude_string, num_chars, "  %.6f", longitude_value);
  } else if ((longitude_value >= 10) && (longitude_value < 100)) {
    snprintf(ret_longitude_string, num_chars, " %.6f", longitude_value);
  } else if (longitude_value >= 100) {
    snprintf(ret_longitude_string, num_chars, "%.6f", longitude_value);
  }
  return;
}

void lcd__helper_five_char_sonar_sensor_data_string(char *ret_sensor_string, char *sensor_postion,
                                                    uint8_t sensor_value) {
  const uint8_t num_chars = 6; // Needs to be one larger than desired output length to attach NULL at end
  const char ascii_space = 0x20;
  const char blank_space[1] = {ascii_space};
  const char two_extra_space[2] = {ascii_space, ascii_space};
  // compensate for sensor values being 1, 2, or 3 chars long
  if (sensor_value < 10) {
    char two_extra_space[2] = {ascii_space, ascii_space};
    char one_digit_sensor_value[1];
    sprintf(one_digit_sensor_value, "%d", sensor_value);
    snprintf(ret_sensor_string, num_chars, "%s%s%s%s", sensor_postion, one_digit_sensor_value, two_extra_space,
             blank_space);
  } else if (sensor_value >= 10 && sensor_value < 100) {
    char two_digit_sensor_value[2];
    sprintf(two_digit_sensor_value, "%d", sensor_value);
    snprintf(ret_sensor_string, num_chars, "%s%s%s%s", sensor_postion, two_digit_sensor_value, blank_space,
             blank_space);
  } else if (sensor_value >= 100) {
    char three_digit_sensor_value[3];
    sprintf(three_digit_sensor_value, "%d", sensor_value);
    snprintf(ret_sensor_string, num_chars, "%s%s%s", sensor_postion, three_digit_sensor_value, blank_space);
  }
  return;
}

void lcd__helper_return_four_char_kph_speed_data_string_for_printout(char *ret_speed_string, float speed_value) {
  const uint8_t num_chars = 5; // Needs to be one larger than desired output length to attach NULL at end
  // compensate for speed values being 3 or 4 chars long
  if (speed_value < -10) {
    char speed_value_under_negative_10_kph[4];
    float positive_value = speed_value * -1.0;
    sprintf(speed_value_under_negative_10_kph, "-%.0f", positive_value);
    snprintf(ret_speed_string, num_chars, " %s", speed_value_under_negative_10_kph);
  } else if ((speed_value >= -10) && (speed_value < 0)) {
    // printf("\n\r2 Speed Value:%f:\n\r", speed_value);
    char speed_value_between_negative_10_and_0[5];
    float positive_value = speed_value * -1.0;
    sprintf(speed_value_between_negative_10_and_0, "-%.1f", positive_value);
    snprintf(ret_speed_string, num_chars, "%s", speed_value_between_negative_10_and_0);
    // printf("speed_value_between_negative_10_and_0:%s:\n\r", speed_value_between_negative_10_and_0);
    // printf("ret_speed_string:%s:\n\r", ret_speed_string);
  } else if ((speed_value >= 0) && (speed_value < 10)) {
    char speed_value_between_0_and_under_10_kph[4];
    sprintf(speed_value_between_0_and_under_10_kph, "%.1f", speed_value);
    snprintf(ret_speed_string, num_chars, " %s", speed_value_between_0_and_under_10_kph);
  } else if ((speed_value >= 10) && (speed_value < 100)) {
    char speed_value_over_10_kph[5];
    sprintf(speed_value_over_10_kph, "%.1fkpm", speed_value);
    snprintf(ret_speed_string, num_chars, "%s", speed_value_over_10_kph);
  }
}

void lcd__helper_heading_or_bearing_in_3_digit_number_string_with_degree_symbol(char *ret_degree_val,
                                                                                uint16_t degree_value) {
  const uint8_t num_chars = 5; // Needs to be one larger than desired output length to attach NULL at end
  const char degree_symbol = 178;
  // compensate for heading or bearing degree strings being 1, 2, or 3 chars long
  if (degree_value < 10) {
    snprintf(ret_degree_val, num_chars, "%d%c  ", degree_value, degree_symbol);
  } else if (degree_value >= 10 && degree_value < 100) {
    snprintf(ret_degree_val, num_chars, "%d%c ", degree_value, degree_symbol);
  } else if (degree_value >= 100) {
    snprintf(ret_degree_val, num_chars, "%d%c", degree_value, degree_symbol);
  }
}

void lcd__helper_distance_to_checkpoint_in_3_digit_number_string(char *ret_distance_val,
                                                                 float distance_to_next_checkpoint) {
  const uint8_t num_chars = 4;
  // compensate for distance to next checkpoint strings being 1, 2, or 3 chars long

  if (distance_to_next_checkpoint < 10) {
    snprintf(ret_distance_val, num_chars, "%.1f", distance_to_next_checkpoint);
  } else if (distance_to_next_checkpoint >= 10 && distance_to_next_checkpoint < 100) {
    snprintf(ret_distance_val, num_chars, "%.0f ", distance_to_next_checkpoint);
  } else if (distance_to_next_checkpoint >= 100 && distance_to_next_checkpoint < 1000) {
    snprintf(ret_distance_val, num_chars, "%.0f", distance_to_next_checkpoint);
  } else {
    snprintf(ret_distance_val, num_chars, ">km"); // print > than 1km if greater than 1km away
  }
  return;
}

void lcd__helper_checkpoint_number_in_2_digit_number_string(char *ret_checkpoint_string, uint8_t checkpoint_num) {
  const uint8_t num_chars = 3;
  // compensate for number of checkpoint being 1 or 2 chars long with error for 3 digit numbers
  if (checkpoint_num < 10) {
    snprintf(ret_checkpoint_string, num_chars, "% d", checkpoint_num);
  } else if ((checkpoint_num >= 10) && (checkpoint_num < 100)) {
    snprintf(ret_checkpoint_string, num_chars, "%d", checkpoint_num);
  } else if (checkpoint_num >= 100) {
    snprintf(ret_checkpoint_string, num_chars, "er", checkpoint_num);
  }
  return;
}

/*********************************************************************************************************************
 * LCD test print ascii functions
 ********************************************************************************************************************/
// Functions to test out what prints off if certain chars are desired to be displayed
void lcd__test_print_off_chars_0_79(void) {
  uint8_t starting_offset = 0;
  lcd__helper_test_print_off_chars(starting_offset);
}

void lcd__test_print_off_chars_80_159(void) {
  uint8_t starting_offset = 80;
  lcd__helper_test_print_off_chars(starting_offset);
}

void lcd__test_print_off_chars_160_239(void) {
  uint8_t starting_offset = 160;
  lcd__helper_test_print_off_chars(starting_offset);
}

void lcd__test_print_off_chars_240_255(void) {
  uint8_t starting_offset = 240;
  lcd__helper_test_print_off_chars(starting_offset);
}

static void lcd__helper_test_print_off_chars(uint8_t starting_offset) {
  uint8_t total_iterations = 80;
  uint8_t total_per_row = 20;
  char test_print_data[80];
  char test_row_0[20];
  char test_row_1[20];
  char test_row_2[20];
  char test_row_3[20];
  for (int i = 0; i < total_iterations; i++) {
    test_print_data[i] = (char)(starting_offset + i);
  }

  //   memcpy(test_row_0, test_print_data[0], total_per_row);
  //   memcpy(test_row_1, test_print_data[20], total_per_row);
  //   memcpy(test_row_2, test_print_data[40], total_per_row);
  //   memcpy(test_row_3, test_print_data[60], total_per_row);

  for (int i = 0; i < total_per_row; i++) {
    test_row_0[i] = test_print_data[0 + i];
    test_row_1[i] = test_print_data[20 + i];
    test_row_2[i] = test_print_data[40 + i];
    test_row_3[i] = test_print_data[60 + i];
  }

  uint8_t lcd_col_0_num = 0;

  uint8_t lcd_row_0_num = 0;
  uint8_t lcd_row_1_num = 1;
  uint8_t lcd_row_2_num = 2;
  uint8_t lcd_row_3_num = 3;

  //   vTaskDelay(1);

  printf("\n\r");
  printf("%s\n\r", test_row_0);
  printf("%s\n\r", test_row_1);
  printf("%s\n\r", test_row_2);
  printf("%s\n\r", test_row_3);
  printf("\n\r");

  //   printstr_lcd(&lcd_info_splash_1_line_1, 1, 0);
  //   printstr_lcd(&lcd_info_splash_1_line_2, 2, 0);
  //   printstr_lcd(&lcd_info_splash_1_line_3, 3, 0);

  printstr_lcd(&test_row_0, lcd_row_0_num, lcd_col_0_num);
  printstr_lcd(&test_row_1, lcd_row_1_num, lcd_col_0_num);
  printstr_lcd(&test_row_2, lcd_row_2_num, lcd_col_0_num);
  printstr_lcd(&test_row_3, lcd_row_3_num, lcd_col_0_num);
}

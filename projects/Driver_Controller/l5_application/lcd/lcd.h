#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LCD_I2C.h"
#include "driver_logic.h"

/*********************************************************************************************************************
 * Data structures and enumerations used in LCD debug display logic
 ********************************************************************************************************************/
typedef enum {
  turn_on_sequence = 0,
  lcd_info_screen_number_1 = 1,
  lcd_info_screen_number_2 = 2,
  lcd_info_screen_number_3 = 3,
  lcd_info_screen_number_4 = 4,
  arrived_at_final_destination = 5,
  low_battery = 6
} lcd_splash_screen_e;

/*********************************************************************************************************************
 * LCD functions
 ********************************************************************************************************************/
void lcd__init(void);

void lcd__printf_lcd_data_struct_pointers(void);

void lcd__copy_all_of_the_pointers_to_rx_and_tx_can_data_stored_in_driver_logic(void);

// void driver__lcd_update_information(lcd_splash_screen_e lcd_splash_screen_number);

void lcd__print_turn_on_sequence(void);

void lcd__print_updated_sensor_info_screen_1(void);

/*********************************************************************************************************************
 * LCD Print line functions
 ********************************************************************************************************************/
void lcd__print_line_filled_with_coordinates_data(uint8_t lcd_row_num);

void lcd__print_line_filled_with_speed_and_checkpoint_data(uint8_t lcd_row_num, bool display_encoder_speed,
                                                           bool display_checkpoint_data,
                                                           driver_checkpoint_message_s *checkpoint_message,
                                                           driver_navigation_message_s *navigation_message);

void lcd__print_line_filled_with_geo_status_data(uint8_t lcd_row_num);

void lcd__print_line_filled_with_sonar_sensor_data(uint8_t lcd_row_num);

/*********************************************************************************************************************
 * LCD helper functions
 ********************************************************************************************************************/
void lcd__helper_ten_char_encoder_speed_string(char *ret_encoder_speed_string, float encoder_speed_value);

void lcd__helper_ten_char_driver_speed_string(char *ret_driver_speed_string, float driver_speed_value);

void lcd__helper_ten_char_navigation_or_checkpoint_message_string(char *ret_navigation_string,
                                                                  bool display_checkpoint_data,
                                                                  driver_checkpoint_message_s *checkpoint_message,
                                                                  driver_navigation_message_s *navigation_message);

void lcd__helper_nine_char_latitude_data_string(char *ret_latitude_string, float latitude_value);

void lcd__helper_ten_char_longitude_data_string(char *ret_longitude_string, float longitude_value);

void lcd__helper_five_char_sonar_sensor_data_string(char *sensor_line, char *sensor_postion, uint8_t sensor_value);

void lcd__helper_return_four_char_kph_speed_data_string_for_printout(char *ret_speed_string, float speed_value);

void lcd__helper_heading_or_bearing_in_3_digit_number_string_with_degree_symbol(char *ret_degree_val,
                                                                                uint16_t degree_value);

void lcd__helper_distance_to_checkpoint_in_3_digit_number_string(char *ret_distance_val,
                                                                 float distance_to_next_checkpoint);

void lcd__helper_checkpoint_number_in_2_digit_number_string(char *ret_checkpoint_string, uint8_t checkpoint_num);

/*********************************************************************************************************************
 * LCD test print ascii functions
 ********************************************************************************************************************/
// Functions to test out what prints off if certain chars are desired to be displayed
void lcd__test_print_off_chars_0_79(void);

void lcd__test_print_off_chars_80_159(void);

void lcd__test_print_off_chars_160_239(void);

void lcd__test_print_off_chars_240_255(void);

static void lcd__helper_test_print_off_chars(uint8_t starting_offset);
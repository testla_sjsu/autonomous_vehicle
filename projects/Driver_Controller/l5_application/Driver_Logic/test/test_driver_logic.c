// #include "Mocklcd.h"
#include "driver_logic.h"
#include "unity.h"

// Thresholds for oject detection which then is used for obstacle avoidance or navigating to target
// Unit cm
static uint16_t OBJECT_THRESHOLD_PRIORITY_0; // Threshold: Used to detect if car stuck
static uint16_t OBJECT_THRESHOLD_PRIORITY_1; // Threshold: Closer bound of obstacle avoidance case
static uint16_t OBJECT_THRESHOLD_PRIORITY_2; // Threshold: Farther bound of obstacle avoidance case
static uint16_t OBJECT_THRESHOLD_PRIORITY_3; // Threshold: Used to slow down navigation branch speed

void setUp(void) {
  driver__init();
  // Fill in the data that is stored global to this file with static const values from "driver_logic.c"
  OBJECT_THRESHOLD_PRIORITY_0 = driver__fetch_OBJECT_THRESHOLD_PRIORITY_0();
  OBJECT_THRESHOLD_PRIORITY_1 = driver__fetch_OBJECT_THRESHOLD_PRIORITY_1();
  OBJECT_THRESHOLD_PRIORITY_2 = driver__fetch_OBJECT_THRESHOLD_PRIORITY_2();
  OBJECT_THRESHOLD_PRIORITY_3 = driver__fetch_OBJECT_THRESHOLD_PRIORITY_3();
}

void tearDown(void) {}

// void test_driver__init(void) { driver__malloc_for_all_static_pointers(); }

// typedef enum {
//   emergency_brake = 0,
//   full_speed_towards_destination =
//       1, // reserved for when no object is detected on any sensor. Drive towards next waypoint
//   all_front_sensor_triggered = 2,
//   no_sensor_triggered = 3,
//   left_sensor_triggered = 4,
//   middle_sensor_triggered = 5,
//   right_sensor_triggered = 6,
//   left_and_middle_sensor_triggered = 7,
//   right_and_middle_sensor_triggered = 8,
//   back_sensor_triggered = 9,
//   all_front_and_back_sensor_are_triggered = 10

// } obstacle_detected_on_sensors_e;

/*********************************************************************************************************************
 * Unit Test(s) for:
 * obstacle_detected_on_sensors_e driver__check_sensors_and_update_sensors_triggered_enum(uint16_t left_sensor_dist,
 *                                                                                        uint16_t middle_sensor_dist,
 *                                                                                        uint16_t right_sensor_dist,
 *                                                                                        uint16_t back_sensor_dist);
 ********************************************************************************************************************/
// void test_driver__main_driver_state_expect_ emergency_brake(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_0 - 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_0 - 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_0 - 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float EMERGENCY_STOP_THRESHOLD_WHEN_OBJECT_DETECTED_AT_THRESHOLD_PRIORITY_0 = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(emergency_brake, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ full_speed_towards_destination(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(full_speed_towards_destination, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ all_front_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(all_front_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ no_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(no_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ left_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(left_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ middle_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(middle_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ right_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(right_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ left_and_middle_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(left_and_middle_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ right_and_middle_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(right_and_middle_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ back_sensor_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(back_sensor_triggered, ret_obstacle_detected_on_sensors);
// }

// void test_driver__main_driver_state_expect_ all_front_and_back_sensor_are_triggered(void) {
//     uint16_t left_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t middle_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t right_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     uint16_t back_sensor = OBJECT_THRESHOLD_PRIORITY_1 + 1;  // cm
//     float car_speed = 0;
//     obstacle_detected_on_sensors_e ret_obstacle_detected_on_sensors;
//     ret_obstacle_detected_on_sensors = driver__check_sensors_and_update_sensors_triggered_enum(left_sensor,
//     middle_sensor, right_sensor, back_sensor, car_speed);
//   TEST_ASSERT_EQUAL(all_front_and_back_sensor_are_triggered, ret_obstacle_detected_on_sensors);
// }

/*********************************************************************************************************************
 * Unit Test(s) for:
 * obstacle_closer_on_side_e driver__oa_is_object_closer_left_or_right(uint16_t left_sensor,
 *                                                                  uint16_t right_sensor);
 ********************************************************************************************************************/
void test_driver__oa_is_object_closer_on_left_side(void) {
  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side;
  uint16_t left_sensor = 30;  // cm
  uint16_t right_sensor = 60; // cm
  ret_potential_object_closer_on_a_side = driver__is_object_closer_left_or_right(left_sensor, right_sensor);
  TEST_ASSERT_EQUAL(object_closer_on_left_side, ret_potential_object_closer_on_a_side);
}

void test_driver__oa_is_object_closer_on_right_side(void) {
  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side;
  uint16_t left_sensor = 60;  // cm
  uint16_t right_sensor = 30; // cm
  ret_potential_object_closer_on_a_side = driver__is_object_closer_left_or_right(left_sensor, right_sensor);
  TEST_ASSERT_EQUAL(object_closer_on_right_side, ret_potential_object_closer_on_a_side);
}

void test_driver__oa_is_object_closer_left_and_right_side_same_distance(void) {
  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side;
  uint16_t left_sensor = 70;  // cm
  uint16_t right_sensor = 70; // cm
  ret_potential_object_closer_on_a_side = driver__is_object_closer_left_or_right(left_sensor, right_sensor);
  TEST_ASSERT_EQUAL(object_same_distance_on_both_sides, ret_potential_object_closer_on_a_side);
}

/*********************************************************************************************************************
 * Unit Test(s) for:
 * obstacle_closer_on_side_e driver__oa_is_object_closer_front_or_back(uint16_t left_sensor,
 *                                                                     uint16_t middle_sensor,
 *                                                                     uint16_t right_sensor,
 *                                                                     uint16_t back_sensor);
 ********************************************************************************************************************/
void test_driver__oa_is_object_closer_on_front_side(void) {
  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side;
  uint16_t left_sensor = 30;   // cm
  uint16_t middle_sensor = 30; // cm
  uint16_t right_sensor = 60;  // cm
  uint16_t back_sensor = 40;   // cm
  ret_potential_object_closer_on_a_side =
      driver__is_object_closer_front_or_back(left_sensor, middle_sensor, right_sensor, back_sensor);
  TEST_ASSERT_EQUAL(object_closer_on_front_side, ret_potential_object_closer_on_a_side);
}

void test_driver__oa_is_object_closer_on_back_side(void) {
  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side;
  uint16_t left_sensor = 30;   // cm
  uint16_t middle_sensor = 30; // cm
  uint16_t right_sensor = 60;  // cm
  uint16_t back_sensor = 20;   // cm
  ret_potential_object_closer_on_a_side =
      driver__is_object_closer_front_or_back(left_sensor, middle_sensor, right_sensor, back_sensor);
  TEST_ASSERT_EQUAL(object_closer_on_back_side, ret_potential_object_closer_on_a_side);
}

void test_driver__oa_is_object_closer_front_and_back_side_same_distance(void) {
  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side;
  uint16_t left_sensor = 30;   // cm
  uint16_t middle_sensor = 40; // cm
  uint16_t right_sensor = 60;  // cm
  uint16_t back_sensor = 30;   // cm
  ret_potential_object_closer_on_a_side =
      driver__is_object_closer_front_or_back(left_sensor, middle_sensor, right_sensor, back_sensor);
  TEST_ASSERT_EQUAL(object_same_distance_on_both_sides, ret_potential_object_closer_on_a_side);
}

// const uint16_t OBJECT_THRESHOLD_PRIORITY_0 = 30;                                           // unit: cm
// const uint16_t OBJECT_THRESHOLD_PRIORITY_1 = 75;                                           // unit: cm
// const uint16_t OBJECT_THRESHOLD_PRIORITY_2 = 100;

// void test_driver__check_sensors_under_OBJECT_THRESHOLD_1(void) {
//   uint16_t left_sensor = 75 + 1;   // cm
//   uint16_t middle_sensor = 75 + 1; // cm
//   uint16_t right_sensor = 75 - 1;  // cm
//   uint16_t back_sensor = 75 + 1;   // cm
// }

/*********************************************************************************************************************
 * Unit Test(s) for:
 * void driver__obstacle_avoidance(car_direction_e *car_direction, car_steering_limit_e *car_steering_limit,
 *                                car_speed_limit_e *car_speed_limit,
 *                                obstacle_detected_on_sensors_e obstacles_threshold_0,
 *                                obstacle_detected_on_sensors_e obstacles_threshold_1,
 *                                obstacle_detected_on_sensors_e obstacles_threshold_2, uint16_t left_sensor,
 *                                uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor);
 ********************************************************************************************************************/
void test_driver__obstacle_avoidance_left(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;

  // Store information for obstacles detected at 3 different thresholds relevant to (Obstacle Avoidance)
  obstacle_detected_on_sensors_e obstacles_threshold_0; // Threshold: Used to detect if car stuck in obstacle avoidance
  obstacle_detected_on_sensors_e obstacles_threshold_1; // Threshold: Closer bound of obstacle avoidance case
  obstacle_detected_on_sensors_e obstacles_threshold_2; // Threshold: Farther bound of obstacle avoidance case
  // Figure out which relavant thresholds obstacles are detected at on all sensors
  obstacles_threshold_0 = driver__check_sensors_under_OBJECT_THRESHOLD_0(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_1 = driver__check_sensors_under_OBJECT_THRESHOLD_1(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_2 = driver__check_sensors_under_OBJECT_THRESHOLD_2(l_sensor, m_sensor, r_sensor, b_sensor);
  // Call function we are testing
  driver__obstacle_avoidance(&direction, &steering_limit, &speed_limit, obstacles_threshold_0, obstacles_threshold_1,
                             obstacles_threshold_2, l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(forwards, direction);
  TEST_ASSERT_EQUAL(right, steering_limit);
  TEST_ASSERT_EQUAL(medium, speed_limit);
}

// done
/*********************************************************************************************************************
 * Unit Test(s) for:
 * void driver__obstacle_avoidance_determine_steering_and_velocity(
 *    car_direction_e *ret_car_direction, car_steering_limit_e *ret_car_steering_limit,
 *    car_speed_limit_e *ret_car_speed_limit, obstacle_detected_on_sensors_e obstacles_threshold_0,
 *    obstacle_detected_on_sensors_e obstacles_threshold_1, obstacle_detected_on_sensors_e obstacles_threshold_2,
 *    uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor);
 ********************************************************************************************************************/
void test_driver__obstacle_avoidance_determine_steering_and_velocity_not_stuck(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;

  // Store information for obstacles detected at 3 different thresholds relevant to (Obstacle Avoidance)
  obstacle_detected_on_sensors_e obstacles_threshold_0; // Threshold: Used to detect if car stuck in obstacle avoidance
  obstacle_detected_on_sensors_e obstacles_threshold_1; // Threshold: Closer bound of obstacle avoidance case
  obstacle_detected_on_sensors_e obstacles_threshold_2; // Threshold: Farther bound of obstacle avoidance case
  // Figure out which relavant thresholds obstacles are detected at on all sensors
  obstacles_threshold_0 = driver__check_sensors_under_OBJECT_THRESHOLD_0(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_1 = driver__check_sensors_under_OBJECT_THRESHOLD_1(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_2 = driver__check_sensors_under_OBJECT_THRESHOLD_2(l_sensor, m_sensor, r_sensor, b_sensor);
  // Call function we are testing
  driver__obstacle_avoidance_determine_steering_and_velocity(
      &direction, &steering_limit, &speed_limit, obstacles_threshold_0, obstacles_threshold_1, obstacles_threshold_2,
      l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(forwards, direction);
  TEST_ASSERT_EQUAL(right, steering_limit);
  TEST_ASSERT_EQUAL(medium, speed_limit);
}

void test_driver__obstacle_avoidance_determine_steering_and_velocity_stuck(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_0 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_0 - 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_0 - 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_0 - 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;

  // Store information for obstacles detected at 3 different thresholds relevant to (Obstacle Avoidance)
  obstacle_detected_on_sensors_e obstacles_threshold_0; // Threshold: Used to detect if car stuck in obstacle avoidance
  obstacle_detected_on_sensors_e obstacles_threshold_1; // Threshold: Closer bound of obstacle avoidance case
  obstacle_detected_on_sensors_e obstacles_threshold_2; // Threshold: Farther bound of obstacle avoidance case
  // Figure out which relavant thresholds obstacles are detected at on all sensors
  obstacles_threshold_0 = driver__check_sensors_under_OBJECT_THRESHOLD_0(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_1 = driver__check_sensors_under_OBJECT_THRESHOLD_1(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_2 = driver__check_sensors_under_OBJECT_THRESHOLD_2(l_sensor, m_sensor, r_sensor, b_sensor);
  // Call function we are testing
  driver__obstacle_avoidance_determine_steering_and_velocity(
      &direction, &steering_limit, &speed_limit, obstacles_threshold_0, obstacles_threshold_1, obstacles_threshold_2,
      l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(not_moving, direction);
  TEST_ASSERT_EQUAL(straight, steering_limit);
  TEST_ASSERT_EQUAL(stopped, speed_limit);
}

// done
/*********************************************************************************************************************
 * Unit Test(s) for:
 * void driver__oa_helper_determine_steering_and_velocity_not_stuck(
 *    car_direction_e *ret_car_direction, car_steering_limit_e *ret_car_steering_limit,
 *    car_speed_limit_e *ret_car_speed_limit, obstacle_detected_on_sensors_e obstacles_threshold_0,
 *    obstacle_detected_on_sensors_e obstacles_threshold_1, obstacle_detected_on_sensors_e obstacles_threshold_2,
 *    uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor);
 ********************************************************************************************************************/
void driver__oa_helper_determine_steering_and_velocity_not_stuck_left_above_priority_1(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;
  // Store information for obstacles detected at 3 different thresholds relevant to (Obstacle Avoidance)
  obstacle_detected_on_sensors_e obstacles_threshold_0; // Threshold: Used to detect if car stuck in obstacle avoidance
  obstacle_detected_on_sensors_e obstacles_threshold_1; // Threshold: Closer bound of obstacle avoidance case
  obstacle_detected_on_sensors_e obstacles_threshold_2; // Threshold: Farther bound of obstacle avoidance case
  // Figure out which relavant thresholds obstacles are detected at on all sensors
  obstacles_threshold_0 = driver__check_sensors_under_OBJECT_THRESHOLD_0(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_1 = driver__check_sensors_under_OBJECT_THRESHOLD_1(l_sensor, m_sensor, r_sensor, b_sensor);
  obstacles_threshold_2 = driver__check_sensors_under_OBJECT_THRESHOLD_2(l_sensor, m_sensor, r_sensor, b_sensor);
  // Call function we are testing
  driver__oa_helper_determine_steering_and_velocity_not_stuck(
      &direction, &steering_limit, &speed_limit, obstacles_threshold_0, obstacles_threshold_1, obstacles_threshold_2,
      l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(forwards, direction);
  TEST_ASSERT_EQUAL(right, steering_limit);
  TEST_ASSERT_EQUAL(medium, speed_limit);
}

/*********************************************************************************************************************
 * Unit Test(s) for:
 * void driver__obstacle_avoidance(obstacle_detected_on_sensors_e obstacles_threshold_0,
 *                                 obstacle_detected_on_sensors_e obstacles_threshold_1,
 *                                 obstacle_detected_on_sensors_e obstacles_threshold_2, uint16_t left_sensor,
                                   uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor)
 ********************************************************************************************************************/
///////////////////////////////////////// threshold_2_left cases /////////////////////////////////////////
// void test_driver__obstacle_avoidance_threshold_2_left_default(void) {
//   // INPUT VALUES TO CHANGE to check logic of this function
//   uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
//   uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   // RETURN VALUES TO CHECK to check logic of this function
//   car_direction_e direction;
//   car_steering_limit_e steering_limit;
//   car_speed_limit_e speed_limit;
//   // Call fuction with return and input parameters
//   driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor,
//   b_sensor);
//   // Compare results of the expected return value vs actual return value
//   TEST_ASSERT_EQUAL(forwards, direction);
//   TEST_ASSERT_EQUAL(right, steering_limit);
//   TEST_ASSERT_EQUAL(medium, speed_limit);
// }

// void test_driver__obstacle_avoidance_threshold_2_left_threshold_1_left(void) {
//   // INPUT VALUES TO CHANGE to check logic of this function
//   uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_1 - 1; // cm
//   uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   // RETURN VALUES TO CHECK to check logic of this function
//   car_direction_e direction;
//   car_steering_limit_e steering_limit;
//   car_speed_limit_e speed_limit;
//   // Call fuction with return and input parameters
//   driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor,
//   b_sensor);
//   // Compare results of the expected return value vs actual return value
//   TEST_ASSERT_EQUAL(backwards, direction);
//   TEST_ASSERT_EQUAL(left, steering_limit);
//   TEST_ASSERT_EQUAL(slow, speed_limit);
// }

///////////////////////////////////////// threshold_2_back_left cases /////////////////////////////////////////
void test_driver__obstacle_avoidance_threshold_2_back_left_default(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;
  // Call fuction with return and input parameters
  driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(forwards, direction);
  TEST_ASSERT_EQUAL(right, steering_limit);
  TEST_ASSERT_EQUAL(medium, speed_limit);
}

void test_driver__obstacle_avoidance_threshold_2_back_left__threshold_1_back(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_1 - 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;
  // Call fuction with return and input parameters
  driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(forwards, direction);
  TEST_ASSERT_EQUAL(hard_right, steering_limit);
  TEST_ASSERT_EQUAL(slow, speed_limit);
}

// void test_driver__obstacle_avoidance_threshold_2_back_left__threshold_1_left(void) {
//   // INPUT VALUES TO CHANGE to check logic of this function
//   uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_1 - 1; // cm
//   uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
//   uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
//   // RETURN VALUES TO CHECK to check logic of this function
//   car_direction_e direction;
//   car_steering_limit_e steering_limit;
//   car_speed_limit_e speed_limit;
//   // Call fuction with return and input parameters
//   driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor,
//   b_sensor);
//   // Compare results of the expected return value vs actual return value
//   TEST_ASSERT_EQUAL(backwards, direction);
//   TEST_ASSERT_EQUAL(hard_left, steering_limit);
//   TEST_ASSERT_EQUAL(slow, speed_limit);
// }

void test_driver__obstacle_avoidance_threshold_2_back_left__threshold_1_back_left__front_closer(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_1 - 2; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_1 - 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;
  // Call fuction with return and input parameters
  driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(backwards, direction);
  TEST_ASSERT_EQUAL(hard_left, steering_limit);
  TEST_ASSERT_EQUAL(slow, speed_limit);
}

void test_driver__obstacle_avoidance_threshold_2_back_left__threshold_1_back_left__back_closer(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_1 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_1 - 2; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;
  // Call fuction with return and input parameters
  driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(forwards, direction);
  TEST_ASSERT_EQUAL(hard_right, steering_limit);
  TEST_ASSERT_EQUAL(slow, speed_limit);
}

void test_driver__obstacle_avoidance_threshold_2_middle_left_default(void) {
  // INPUT VALUES TO CHANGE to check logic of this function
  uint16_t l_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  uint16_t m_sensor = OBJECT_THRESHOLD_PRIORITY_2 - 1; // cm
  uint16_t r_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  uint16_t b_sensor = OBJECT_THRESHOLD_PRIORITY_2 + 1; // cm
  // RETURN VALUES TO CHECK to check logic of this function
  car_direction_e direction;
  car_steering_limit_e steering_limit;
  car_speed_limit_e speed_limit;
  // Call fuction with return and input parameters
  driver__obstacle_avoidance_setup(&direction, &steering_limit, &speed_limit, l_sensor, m_sensor, r_sensor, b_sensor);
  // Compare results of the expected return value vs actual return value
  TEST_ASSERT_EQUAL(forwards, direction);
  TEST_ASSERT_EQUAL(hard_right, steering_limit);
  TEST_ASSERT_EQUAL(slow, speed_limit);
}
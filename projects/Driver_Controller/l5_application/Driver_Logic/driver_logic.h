#pragma once

#include "FreeRTOS.h" // Included for the vTaskdelay
#include "board_io.h"
#include "gpio.h"
#include "testla_proto1.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*********************************************************************************************************************
 * Data structures and enumerations used in driver logic to store data related to car states
 ********************************************************************************************************************/
typedef enum { backwards = -1, not_moving = 0, forwards = 1 } car_direction_e;

typedef enum { hard_left = -2, left = -1, straight = 0, right = 1, hard_right = 2 } car_steering_limit_e;

typedef enum {
  stopped = 0,
  slow = 1,
  medium = 2,
  fast = 3,
  ludicrous_speed = 4,
} car_speed_limit_e;

typedef enum {
  no_sensor_triggered = 0, /* navigate_towards_next_checkpoint() */
  left_triggered = 1,      /* if(thresh1 != thresh2){Fwd, Medium speed, Soft right} else{Bwd, Slow speed, Hard left} */
  middle_triggered = 2,    /* Bwd, Slow speed, if(left < right){Hard right} else{Hard left} */
  middle_left_triggered = 3, /* if(thresh1 != thresh2){Fwd, Slow speed, Hard right} else{Bwd, Slow speed, Hard left} */
  right_triggered = 4,      /* if(thresh1 != thresh2){Fwd, Medium speed, Soft left} else{Bwd, Slow speed, Hard right} */
  right_left_triggered = 5, /* TODO: Implement this */
  right_middle_triggered = 6, /* if(thresh1 != thresh2){Fwd, Slow speed, Hard left} else{Bwd, Slow speed, Hard right} */
  all_front_sensors_triggered = 7, /* Bwd, Slow speed, if(left < right){Hard right} else{Hard left} */
  back_triggered = 8,              /* navigate_towards_next_checkpoint() */
  back_left_triggered = 9,         /* Fwd, Medium speed, Soft right */
  back_middle_triggered =
      10, /* if(back<middle){Fwd, Slow speed, if(left<right){Hard right} else{Hard left};else opposite */
  back_middle_left_triggered = 11,             /* Fwd, Slow speed, Hard right, stop if threshold 0 value  */
  back_right_triggered = 12,                   /* if(back<right){Fwd, Slow speed, Soft left  */
  back_right_left_triggered = 13,              /* Fwd, Slow speed, Straight, stuck if thresh 0 left or right is */
  back_right_middle_triggered = 14,            /* Fwd, Slow speed, Hard left,  */
  all_front_and_back_sensor_are_triggered = 15 /* Stuck if also thresh0*/
} obstacle_detected_on_sensors_e;

typedef enum {
  object_same_distance_on_both_sides = 0,
  object_closer_on_left_side = 1,
  object_closer_on_right_side = 2,
  object_closer_on_front_side = 3,
  object_closer_on_back_side = 4
} obstacle_closer_on_side_e;

// used to store state of turning the navigation turn stat
typedef enum {
  nav_no_turn = 0,
  nav_soft_turn = 1,
  nav_medium_turn = 2,
  nav_hard_turn = 3
} navigation_turning_strength_e;

// used to store state of turning the navigation turn stat
typedef enum { nav_left = -1, nav_straight = 2, nav_right = 1 } navigation_turning_direction_e;

// Used by the checkpoint struct in the GEO controller and this message is also sent over can bus
typedef enum {
  checkpoint_not_completed_yet = 0, // start up, and switches into this after checkpoint_completed if more are queued
  checkpoint_completed = 1,         // when car is within n amount of meters of checkpoint, checkpoint_status updates
  checkpoint_uncompletable = 2      // when car times out trying to go to this checkpoint, it defaults to this value
} checkpoint_status_e;

// Used by the Navigation struct in the GEO controller and this message is also sent over can bus
typedef enum {
  navigation_currently_not_in_progress = 0, // a start up state when the car is first powered on
  navigation_in_progress = 1,               // if navigation is in progress it is represented with this number
  navigation_just_finished = 2, // if navigation sequence is finished, this is what will tell the rest of the MCUs
  navigation_error = 3 // if navigation sequence is attempted to begin, but it is not ready to initialize for reasons
} navigation_status_e;

/*********************************************************************************************************************
 * Data structures to store latest data passed on from other controllers on the can bus
 ********************************************************************************************************************/
typedef struct {
  float latitude;  // unit: Degrees
  float longitude; // unit: Degrees
} driver_geo_coordinates_s;

typedef struct {
  uint16_t current_compass_heading;     // unit: Degrees
  uint16_t current_destination_bearing; // unit: Degrees
  float current_distance_destination;   // unit: Meters
  uint16_t current_geo_frame_id;        // unit: interger
} driver_geo_status_s;

typedef struct {
  float latitude;                  // unit: Degrees
  float longitude;                 // unit: Degrees
  uint8_t curr_checkpoint_number;  // unit: Number in nav algorithm
  uint8_t total_checkpoint_number; // unit: Number in nav algorithm
  bool is_final_checkpoint;
  checkpoint_status_e checkpoint_status; // unit: refer to checkpoint_status_e
} driver_checkpoint_message_s;

typedef struct {
  navigation_status_e navigation_status; // unit: refer to navigation_status_e
} driver_navigation_message_s;

typedef struct {
  float current_encoder_velocity;  // unit: KPH (translated from rpm)
  uint8_t current_motor_heartbeat; // TODO: unit: bool?
} driver_encoder_status_s;

typedef struct {
  float current_velocity;       // unit: KPH
  float current_steering_angle; // unit: Radians
} driver_steering_and_velocity_s;

typedef struct {
  uint16_t left_sensor;             // unit: cm
  uint16_t middle_sensor;           // unit: cm
  uint16_t right_sensor;            // unit: cm
  uint16_t back_sensor;             // unit: cm
  uint16_t current_sensor_frame_id; // unit: interger
} driver_sonar_sensors_status_s;

/*********************************************************************************************************************
 * Private function to allocate memory to pointers for data structures that contain data stored from CAN bus comms
 ********************************************************************************************************************/
/// make space for storing pointers that contain data from can bus communications between controllers
void driver__malloc_for_all_static_pointers(void);

/*********************************************************************************************************************
 * Public functions relating to structures that store latest data passed on from other controllers on the CAN bus
 ********************************************************************************************************************/
/// rx controller status messages
driver_geo_status_s *ret_cpy_of_rx_geo_controller(void);
driver_encoder_status_s *ret_cpy_of_rx_motor_controller(void);
driver_sonar_sensors_status_s *ret_cpy_of_rx_sensor_controller(void);
/// rx controller status messages
driver_geo_coordinates_s *ret_cpy_of_rx_car_coordinates(void);
driver_geo_coordinates_s *ret_cpy_of_rx_final_dest_coordinates(void);
driver_checkpoint_message_s *ret_cpy_of_rx_checkpoint_message(void);
driver_navigation_message_s *ret_cpy_of_rx_navigation_message(void);
/// tx driver messages
driver_steering_and_velocity_s *ret_cpy_of_tx_to_motor_controller(void);

/*********************************************************************************************************************
 * Private functions used to update local copies of structs to hold can data transmitions
 ********************************************************************************************************************/
void driver__receive_latest_motor_controller_status(dbc_MOTOR_HEARTBEAT_s *motor_status);
void driver__receive_latest_geo_controller_status(dbc_GEO_STATUS_s *geo_status);
void driver__receive_car_coordinates(dbc_DEBUG_GPS_CURRENT_LOCATION_s *car_coordinates);
void driver__receive_latest_sensor_bridge_status(dbc_SENSOR_SONARS_s *sensor_status);
void driver__receive_destination_coordinates(dbc_SENSOR_DESTINATION_LOCATION_s *destination_coordinates);

/*********************************************************************************************************************
 * Public functions for initialization of driver logic
 ********************************************************************************************************************/
/// Put all initiliaztion functions inside the function below here
void driver__init(void);

/*********************************************************************************************************************
 * Public functions used to drive the vehicle
 ********************************************************************************************************************/
/// The main function of driver logic boss function
void driver__navigation_in_progress(void);
/// Output of this determines if we use (Navigation to Checkpoint Destination) or (Obstacle Avoidance)
bool driver__use_navigation_towards_next_checkpoint(uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor,
                                                    uint16_t back_sensor);
/// Branch of code that is called for (Navigation to Checkpoint Destination)
void driver__navigation_towards_next_checkpoint_setup(navigation_turning_direction_e *nav_turning_direction,
                                                      navigation_turning_strength_e *nav_turning_strength,
                                                      uint16_t left_sensor, uint16_t middle_sensor,
                                                      uint16_t right_sensor, uint16_t back_sensor);
void driver__navigation_towards_next_checkpoint(navigation_turning_direction_e *nav_turning_direction,
                                                navigation_turning_strength_e *nav_turning_strength,
                                                obstacle_detected_on_sensors_e obstacles_threshold_3);
/// Branch of code that is called for (Obstacle Avoidance)
void driver__obstacle_avoidance_setup(car_direction_e *car_direction, car_steering_limit_e *car_steering_limit,
                                      car_speed_limit_e *car_speed_limit, uint16_t left_sensor, uint16_t middle_sensor,
                                      uint16_t right_sensor, uint16_t back_sensor);
void driver__obstacle_avoidance(car_direction_e *car_direction, car_steering_limit_e *car_steering_limit,
                                car_speed_limit_e *car_speed_limit,
                                obstacle_detected_on_sensors_e obstacles_threshold_0,
                                obstacle_detected_on_sensors_e obstacles_threshold_1,
                                obstacle_detected_on_sensors_e obstacles_threshold_2, uint16_t left_sensor,
                                uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor);

/*********************************************************************************************************************
 * Private Functions used by BOTH (Navigation to Checkpoint Destination) and (Obstacle Avoidance) branches
 ********************************************************************************************************************/
/// Check sensor values against defined constant threshold and return bitfields for sensors that are < threshold
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_0(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor);
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_1(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor);
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_2(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor);
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_3(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor);
/// Helper function called by all 4 function above to provide required output value using (0b0000BRML) bit convention
obstacle_detected_on_sensors_e
driver__helper_check_sensors_under_OBJECT_THRESHOLD_N(uint16_t left_sensor, uint16_t middle_sensor,
                                                      uint16_t right_sensor, uint16_t back_sensor, uint16_t threshold);

/*********************************************************************************************************************
 * Private Functions used by ONLY (Navigation to Checkpoint Destination) branch
 ********************************************************************************************************************/
/// Function will return a value between -179 and +180, and when negative, turn left. When positive, turn right
int16_t driver__navigation_deg_offset_between_bearing_and_heading(uint16_t dest_bearing, uint16_t car_heading);
/// Returns enumeration type for left, straight, or right navigation steering direction
navigation_turning_direction_e driver__navigation_which_direction_to_turn(int16_t bearing_and_heading_dif);
/// Returns enumeration type for hard, medium, soft, or no turn based off of abs(bearing_and_heading_dif)
navigation_turning_strength_e driver__navigation_how_hard_to_turn(int16_t bearing_and_heading_dif);
/// Calculate steering from navigation_turning_direction_e and navigation_turning_strength_e enums
float driver__navigation_calculate_steering(navigation_turning_direction_e turning_direction,
                                            navigation_turning_strength_e turning_strength);
/// Calculate velocity from navigation_turning_direction_e and navigation_turning_strength_e enums
float driver__navigation_calculate_velocity(navigation_turning_strength_e nav_turning_strength,
                                            float dist_to_next_checkpoint, bool is_final_checkpoint,
                                            obstacle_detected_on_sensors_e obstacles_threshold_3,
                                            float calculated_steering_angle);

/*********************************************************************************************************************
 * Private Functions used by ONLY (Obstacle Avoidance) branch
 ********************************************************************************************************************/
/// Assign steering from enums returned by driver__obstacle_avoidance_determine_steering_and_velocity
float driver__obstacle_avoidance_assign_steering_angle(car_steering_limit_e *param_car_steering_limit);
/// Assign velocity from enums returned by driver__obstacle_avoidance_determine_steering_and_velocity
float driver__obstacle_avoidance_assign_velocity(car_speed_limit_e *param_car_speed_limit,
                                                 car_direction_e *param_car_direction);
/// The following functions are used when determining extra steering and velocity instructions when avoiding objects
obstacle_closer_on_side_e driver__is_object_closer_left_or_right(uint16_t left_sensor_dist, uint16_t right_sensor_dist);
obstacle_closer_on_side_e driver__is_object_closer_front_or_back(uint16_t left_sensor_dist, uint16_t middle_sensor_dist,
                                                                 uint16_t right_sensor_dist, uint16_t back_sensor_dist);
/// Multi-hundred line long set of switch case conditions that embody the entirety of obstacle avoidance state machine
void driver__obstacle_avoidance_determine_steering_and_velocity(
    car_direction_e *ret_car_direction, car_steering_limit_e *ret_car_steering_limit,
    car_speed_limit_e *ret_car_speed_limit, obstacle_detected_on_sensors_e obstacles_threshold_0,
    obstacle_detected_on_sensors_e obstacles_threshold_1, obstacle_detected_on_sensors_e obstacles_threshold_2,
    uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor);

void driver__oa_helper_determine_steering_and_velocity_stuck(car_direction_e *ret_car_direction,
                                                             car_steering_limit_e *ret_car_steering_limit,
                                                             car_speed_limit_e *ret_car_speed_limit);

void driver__oa_helper_determine_steering_and_velocity_not_stuck(
    car_direction_e *ret_car_direction, car_steering_limit_e *ret_car_steering_limit,
    car_speed_limit_e *ret_car_speed_limit, obstacle_detected_on_sensors_e obstacles_threshold_0,
    obstacle_detected_on_sensors_e obstacles_threshold_1, obstacle_detected_on_sensors_e obstacles_threshold_2,
    uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor);

/// Lengthy edgecase used in function above that uses ~60 lines of code, no need to copy and paste multiple times
void driver__oa_helper_threshold_1_back_middle_case(car_direction_e *ret_car_direction,
                                                    car_steering_limit_e *ret_car_steering_limit,
                                                    car_speed_limit_e *ret_car_speed_limit,
                                                    obstacle_detected_on_sensors_e obstacles_threshold_1,
                                                    obstacle_closer_on_side_e object_closer_left_or_right_side,
                                                    obstacle_closer_on_side_e object_closer_front_or_back);

/*********************************************************************************************************************
 * Private functions used for averaging
 ********************************************************************************************************************/
float driver__rolling_average_steering(float curr_car_steering);
float driver__rolling_average_velocity(float curr_car_velocity);
float average_n_samples(float *array_containing_values, uint8_t num_of_samples_to_avg);

/*********************************************************************************************************************
 * Private fetch value functions
 ********************************************************************************************************************/
/// Fetch values from structs that local structs that store data received over CAN bus, safer than directly accessing
float driver__fetch_dist_to_next_checkpoint(void);
int16_t driver__fetch_dest_bearing_to_next_checkpoint(void);
int16_t driver__fetch_car_heading_to_next_checkpoint(void);
bool driver__fetch_checkpoint_message_is_final_checkpoint(void);
uint16_t driver__fetch_left_sensor_distance(void);
uint16_t driver__fetch_middle_sensor_distance(void);
uint16_t driver__fetch_right_sensor_distance(void);
uint16_t driver__fetch_back_sensor_distance(void);

/*********************************************************************************************************************
 * Public fetch value functions (used only by the unit test for this file)
 ********************************************************************************************************************/
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_0(void);
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_1(void);
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_2(void);
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_3(void);

/*********************************************************************************************************************
 * Private set value functions
 ********************************************************************************************************************/
/// Set velocity and steering data in proper variables to be sent out over can bus to motor contoller
void driver__set_velocity_to_send_to_motor_controller(float param_set_car_velocity);
void driver__set_steering_to_send_to_motor_controller(float param_set_car_steering);

/*********************************************************************************************************************
 * Public functions used to get velocity and steering data
 ********************************************************************************************************************/
float driver__retrieve_motor_controller_velocity(void);
float driver__retrieve_motor_controller_steering_yaw(void);
void driver__copy_steering_and_velocity_values_for_transmission(dbc_DRIVER_STEERING_s *driver_steering_message);

/*********************************************************************************************************************
 * Public debug functions for printing off values
 ********************************************************************************************************************/
void driver__print_states_of_car(void);
void driver__print_sensor_data(void);
void driver__print_steering_and_velocity(void);
void driver__print_geo_status_message(void);
void driver__print_current_car_location_message(void);
void driver__print_final_destination_location_message(void);
void driver__print_next_checkpoint_location_message(void);
void driver__printf_data_struct_pointer_addresses(void);

/* FUNCTIONS STILL NEEDED
//
+ Check heartbeats of all the other controllers and map it to a character on the lcd
- possibly take over a portion of the LCD display to say communications not working
//
+ Ensure that the connection to the bridge is established
- Once connection is established, wait until command is sent out to start moving
//
+ Navigation algorithm
- use the Final destination and current location and break down the navigation into smaller steps (waypoints)
- Branch through the waypoint graph and make stops along the way to get to final desination
- ?find out if the waypoint is impossible to get to and ditch it if takes too much time to get to it?
*/

/* Things that the Driver controller needs to output to the LCD
 * Distance to target (in meters)
 * Current Heading (compass)
 * Speed (kph)
 * (and maybe angle of the steering)
 * Sensor distances FL: FC: FR: B: (in cm)
 * Connection to the WiFi Bridge established
 */
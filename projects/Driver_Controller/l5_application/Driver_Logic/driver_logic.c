#include "driver_logic.h"
#include <math.h>

/*********************************************************************************************************************
 * Constant values for use in the program
 ********************************************************************************************************************/
// Thresholds for oject detection which then is used for obstacle avoidance or navigating to target
// All units are in cm
const static uint16_t OBJECT_THRESHOLD_PRIORITY_0 = 20;  // Threshold: Used to detect if car stuck
const static uint16_t OBJECT_THRESHOLD_PRIORITY_1 = 40;  // Threshold: Closer bound of obstacle avoidance case
const static uint16_t OBJECT_THRESHOLD_PRIORITY_2 = 90;  // Threshold: Farther bound of obstacle avoidance case
const static uint16_t OBJECT_THRESHOLD_PRIORITY_3 = 125; // Threshold: Used to slow down navigation branch speed
// Multiplies all the speeds used in the logic to drive the car
const float TEST_SPEED_LIMIT_MULTIPLIER = 1.00f;
// Special Constant floats used in program
const float EMERGENCY_BRAKE_THRESHOLD_WHEN_OBJECT_DETECTED_AT_THRESHOLD_PRIORITY_2 = 10.0f; // unit: kph
const float MIDDLE_SENSOR_MULTIPLIER_DISTANCE = 1.0f;                                       // unit: multiplier
// used in how many data setpoints are usd in the sampling return
#define NUM_VELOCITY_SAMPLES_TO_AVEREAGE 2
#define NUM_STEERING_SAMPLES_TO_AVEREAGE 8

/*********************************************************************************************************************
 * Constant variable for setting steering and speed in (Navigation to Checkpoint Destination) and (Obstacle Avoidance)
 ********************************************************************************************************************/
// roughly ~20 degrees in radians
#define RAD_20_DEGS 0.348

/// Steering limits in radians used in Navigation to Destination
const float NAV_STEERING_LIMIT_NO_TURN = 0.0;       // unit: radians 0.000000 = 0 degrees
const float NAV_STEERING_LIMIT_SOFT_TURN = 0.087;   // unit: radians 0.087267 = pi/36 = 5 degrees
const float NAV_STEERING_LIMIT_MEDIUM_TURN = 0.174; // unit: radians 0.174533 = pi/18 = 10 degrees
const float NAV_STEERING_LIMIT_HARD_TURN = 0.348;   // unit: radians 0.349066 = pi/9 = 20 degrees

/// Steering Thresholds in degrees to
const uint16_t NAV_STEERING_THRESHOLD_NO_TURN = 3;      // unit: degrees
const uint16_t NAV_STEERING_THRESHOLD_SOFT_TURN = 6;    // unit: degrees
const uint16_t NAV_STEERING_THRESHOLD_MEDIUM_TURN = 20; // unit: degrees
const uint16_t NAV_STEERING_THRESHOLD_HARD_TURN = 23;   // unit: degrees

/// Steering limits in radians used in Obstacle Avoidance
const float OBSTACLE_AVOIDANCE_STEERING_LIMIT_HARD_LEFT = -0.348; // unit: radians 0.349066 = pi/9 = 20 degrees
const float OBSTACLE_AVOIDANCE_STEERING_LIMIT_LEFT = -0.174;      // unit: radians 0.174533 = pi/18 = 10 degrees
const float OBSTACLE_AVOIDANCE_STEERING_LIMIT_STRAIGHT = 0;       // unit: radians 0.000000 = 0 degrees
const float OBSTACLE_AVOIDANCE_STEERING_LIMIT_RIGHT = 0.174;      // unit: radians 0.174533 = pi/18 = 20 degrees
const float OBSTACLE_AVOIDANCE_STEERING_LIMIT_HARD_RIGHT = 0.348; // unit: radians 0.349066 = pi/9 = 20 degrees

// Navigation speeds while going towards target
const float NAV_SPEED_LIMIT_STOPPED_DESTINATION_REACHED = 0.0; // unit: kph
const float NAV_SPEED_LIMIT_TURNING_SLOW = 3.0;                // unit: kph
const float NAV_SPEED_LIMIT_TURNING_MEDIUM = 4.0;              // unit: kph
const float NAV_SPEED_LIMIT_TURNING_FAST = 5.0;                // unit: kph
const float NAV_SPEED_LIMIT_NOT_TURNING_LUDICROUS_SPEED = 6.0; // unit: kph

// Limitations for driving velocities in various scenarios
const float OBSTACLE_AVOIDANCE_SPEED_LIMIT_BACKWARD_MEDIUM = -4.0;        // unit: kph
const float OBSTACLE_AVOIDANCE_SPEED_LIMIT_BACKWARD_SLOW = -3.5;          // unit: kph
const float OBSTACLE_AVOIDANCE_SPEED_LIMIT_NOT_MOVING = 0.0;              // unit: kph
const float OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_SLOW = 3.0;            // unit: kph
const float OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_MEDIUM = 3.5;          // unit: kph
const float OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_FAST = 4.0;            // unit: kph
const float OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_LUDICROUS_SPEED = 6.0; // unit: kph

/*********************************************************************************************************************
 * Locally defined Data variables, structs, and enumerations used in the driver logic
 ********************************************************************************************************************/
// structs to store data received from other controllers
driver_geo_coordinates_s *rx_car_coordinates;
driver_geo_coordinates_s *rx_final_dest_coordinates;
driver_checkpoint_message_s *rx_checkpoint_message;
driver_navigation_message_s *rx_navigation_message;
driver_geo_status_s *rx_geo_controller;
driver_encoder_status_s *rx_motor_controller;
driver_sonar_sensors_status_s *rx_sensor_controller;
// structs to store data to transmit to other controllers
driver_steering_and_velocity_s *tx_to_motor_controller;

/*********************************************************************************************************************
 * Initialization
 ********************************************************************************************************************/
void driver__init(void) { driver__malloc_for_all_static_pointers(); }

/*********************************************************************************************************************
 * Function to copy over data structures pointers to other code modules in the Driver Controller
 ********************************************************************************************************************/
void driver__malloc_for_all_static_pointers(void) {
  rx_car_coordinates = malloc(sizeof(driver_geo_coordinates_s));
  rx_final_dest_coordinates = malloc(sizeof(driver_geo_coordinates_s));
  rx_checkpoint_message = malloc(sizeof(driver_checkpoint_message_s));
  rx_navigation_message = malloc(sizeof(driver_navigation_message_s));
  rx_geo_controller = malloc(sizeof(driver_geo_status_s));
  rx_motor_controller = malloc(sizeof(driver_encoder_status_s));
  rx_sensor_controller = malloc(sizeof(driver_sonar_sensors_status_s));
  tx_to_motor_controller = malloc(sizeof(driver_steering_and_velocity_s));
}

// done
// rx controller status messages
driver_geo_status_s *ret_cpy_of_rx_geo_controller(void) { return rx_geo_controller; }
driver_encoder_status_s *ret_cpy_of_rx_motor_controller(void) { return rx_motor_controller; }
driver_sonar_sensors_status_s *ret_cpy_of_rx_sensor_controller(void) { return rx_sensor_controller; }
// rx controller status messages
driver_geo_coordinates_s *ret_cpy_of_rx_car_coordinates(void) { return rx_car_coordinates; }
driver_geo_coordinates_s *ret_cpy_of_rx_final_dest_coordinates(void) { return rx_final_dest_coordinates; }
driver_checkpoint_message_s *ret_cpy_of_rx_checkpoint_message(void) { return rx_checkpoint_message; }
driver_navigation_message_s *ret_cpy_of_rx_navigation_message(void) { return rx_navigation_message; }
// tx driver messages
driver_steering_and_velocity_s *ret_cpy_of_tx_to_motor_controller(void) { return tx_to_motor_controller; }

/*********************************************************************************************************************
 * Functions used to update local copies of structs to hold can data transmitions
 ********************************************************************************************************************/
// done
void driver__receive_latest_geo_controller_status(dbc_GEO_STATUS_s *geo_status) { // should happen at 10Hz
  rx_geo_controller->current_compass_heading = geo_status->GEO_STATUS_compass_heading;
  rx_geo_controller->current_destination_bearing = geo_status->GEO_STATUS_destination_bearing;
  rx_geo_controller->current_distance_destination = geo_status->GEO_STATUS_destination_distance;
  //   // TODO: fetch frame ID once scott updates the dbc
}

// // review later: merge in changes to dbc and use that information to change this information
// void driver__receive_latest_motor_controller_status(dbc_MOTOR_HEARTBEAT_s *motor_status) { // should happen at 10Hz
//   rx_motor_controller->current_encoder_velocity = motor_status->MOTOR_HEARTBEAT_encoder;
//   rx_motor_controller->current_motor_heartbeat = motor_status->MOTOR_HEARTBEAT_encoder;
//   // TODO: remind mike to switch out the rpm velcolity uint8_t fr the the
// }

// done
void driver__receive_latest_sensor_bridge_status(dbc_SENSOR_SONARS_s *sensor_status) { // should happen at 10Hz
  rx_sensor_controller->left_sensor = sensor_status->SENSOR_SONARS_left;
  rx_sensor_controller->middle_sensor = sensor_status->SENSOR_SONARS_middle;
  rx_sensor_controller->right_sensor = sensor_status->SENSOR_SONARS_right;
  rx_sensor_controller->back_sensor = sensor_status->SENSOR_SONARS_back;
  rx_sensor_controller->current_sensor_frame_id = sensor_status->SENSOR_SONARS_frame_id;
}

// done
void driver__receive_car_coordinates(dbc_DEBUG_GPS_CURRENT_LOCATION_s *car_coordinates) { // should happen at 10Hz
  rx_car_coordinates->latitude = car_coordinates->DEBUG_GPS_CURRENT_LOCATION_latitude;    // unit: Degrees
  rx_car_coordinates->longitude = car_coordinates->DEBUG_GPS_CURRENT_LOCATION_longitude;  // unit: Degrees
}

// done
void driver__receive_destination_coordinates(
    dbc_SENSOR_DESTINATION_LOCATION_s *destination_coordinates) {                               // should happen at 10Hz
  rx_final_dest_coordinates->latitude = destination_coordinates->SENSOR_DESTINATION_latitude;   // unit: Degrees
  rx_final_dest_coordinates->longitude = destination_coordinates->SENSOR_DESTINATION_longitude; // unit: Degrees
}

// TODO: Change this so that this function use the next checkpoint coordinates
// void driver__receive_next_checkpoint_coordinates(dbc_CHECKPOINT_s *next_checkpoint) { // should happen at 10Hz
//   rx_next_checkpoint_coordinatesand_number->latitude = next_checkpoint->SENSOR_DESTINATION_longitude; // unit:
//   Degrees rx_next_checkpoint_coordinatesand_number->longitude = next_checkpoint->SENSOR_DESTINATION_longitude;//
//   unit: Degrees rx_next_checkpoint_coordinatesand_number->checkpoint_number = next_checkpoint->CHECKPOINT_NUMBER;//
//   unit: UINT8_T
// rx_next_checkpoint_coordinatesand_number->is_final_checkpoint = next_checkpoint->IS_FINAL_CHECKPOINT;// unit: UINT8_T
// }

/*********************************************************************************************************************
 * Public functions used to drive the vehicle
 ********************************************************************************************************************/
/// The main function of driver logic boss function
void driver__navigation_in_progress(void) {
  // Fetch the values for all 4 sensors
  uint16_t left_sensor = driver__fetch_left_sensor_distance();
  uint16_t middle_sensor = driver__fetch_middle_sensor_distance();
  uint16_t right_sensor = driver__fetch_right_sensor_distance();
  uint16_t back_sensor = driver__fetch_back_sensor_distance();
  // States returned from (Obstacle Avoidance) branch. Does nothing here, but enables unit testing
  car_direction_e obstacle_avoidance_car_direction;
  car_steering_limit_e obstacle_avoidance_car_steering_limit;
  car_speed_limit_e obstacle_avoidance_car_speed_limit;
  // States returned from (Navigation to Checkpoint Destination) branch. Does nothing here, but enables unit testing
  navigation_turning_direction_e navigation_turning_direction;
  navigation_turning_strength_e navigation_turning_strength;
  // Determine if we need to use (Obstacle Avoidance) or (Navigation to Checkpoint Destination) branch
  bool use_nav = driver__use_navigation_towards_next_checkpoint(left_sensor, middle_sensor, right_sensor, back_sensor);
  if (use_nav) {
    driver__navigation_towards_next_checkpoint_setup(&navigation_turning_direction, &navigation_turning_strength,
                                                     left_sensor, middle_sensor, right_sensor, back_sensor);
  } else {
    driver__obstacle_avoidance_setup(&obstacle_avoidance_car_direction, &obstacle_avoidance_car_steering_limit,
                                     &obstacle_avoidance_car_speed_limit, left_sensor, middle_sensor, right_sensor,
                                     back_sensor);
  }
}

/// Output of this determines if we use (Navigation to Checkpoint Destination) or (Obstacle Avoidance)
bool driver__use_navigation_towards_next_checkpoint(uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor,
                                                    uint16_t back_sensor) {
  bool ret_use_navigation_to_destination_branch = false;
  // If any front sensors trigger under this threshold use (Obstacle Avoidance), else (Navigation to Checkpoint
  // Destination)
  obstacle_detected_on_sensors_e obstacles_threshold_2; // Threshold: Farther bound of obstacle avoidance case
  obstacles_threshold_2 =
      driver__check_sensors_under_OBJECT_THRESHOLD_2(left_sensor, middle_sensor, right_sensor, back_sensor);
  // Based off of sensor data for obstacles_threshold_2, either navigate towards destination or avoid obstacles
  obstacle_detected_on_sensors_e front_sensors_bitmask = 0b00000111;
  obstacle_detected_on_sensors_e check_front_sensors_threshold_2 = obstacles_threshold_2 & front_sensors_bitmask;
  if (check_front_sensors_threshold_2 == no_sensor_triggered) {
    ret_use_navigation_to_destination_branch = true;
  } else {
    ret_use_navigation_to_destination_branch = false;
  }
  return ret_use_navigation_to_destination_branch;
}

/// Branch of code that is called for (Navigation to Checkpoint Destination) and the function called for unit testing it
void driver__navigation_towards_next_checkpoint_setup(navigation_turning_direction_e *nav_turning_direction,
                                                      navigation_turning_strength_e *nav_turning_strength,
                                                      uint16_t left_sensor, uint16_t middle_sensor,
                                                      uint16_t right_sensor, uint16_t back_sensor) {
  // Store information for obstacles detected at threshold 3 which is relevant to (Navigation to Checkpoint Destination)
  obstacle_detected_on_sensors_e obstacles_threshold_3; // Threshold: Used to slow down navigation branch speed
  obstacles_threshold_3 =
      driver__check_sensors_under_OBJECT_THRESHOLD_3(left_sensor, middle_sensor, right_sensor, back_sensor);
  // Call function for (Navigation to Checkpoint Destination) branch of logic
  driver__navigation_towards_next_checkpoint(nav_turning_direction, nav_turning_strength, obstacles_threshold_3);
}

/// Branch of code that is called for (Navigation to Checkpoint Destination)
void driver__navigation_towards_next_checkpoint(navigation_turning_direction_e *nav_turning_direction,
                                                navigation_turning_strength_e *nav_turning_strength,
                                                obstacle_detected_on_sensors_e obstacles_threshold_3) {
  // Fetch details regarding whether current checkpoint is final checkpoint
  bool is_final_checkpoint = driver__fetch_checkpoint_message_is_final_checkpoint();
  // Fetch the checkpoint details to use for determining speed and steering outputs
  float dist_to_next_checkpoint = driver__fetch_dist_to_next_checkpoint();
  int16_t dest_bearing = driver__fetch_dest_bearing_to_next_checkpoint();
  int16_t car_heading = driver__fetch_car_heading_to_next_checkpoint();
  // Use deg offset and replace car_steering_limit value so that we can turn by checkpoint nav
  int16_t bearing_and_heading_dif =
      driver__navigation_deg_offset_between_bearing_and_heading(dest_bearing, car_heading);
  // Store turning strength from driver__navigation_calculate_steering
  *nav_turning_direction = driver__navigation_which_direction_to_turn(bearing_and_heading_dif);
  *nav_turning_strength = driver__navigation_how_hard_to_turn(bearing_and_heading_dif);
  // Calculate the steering and velocity for the car using varius parameters
  float nav_car_steering = driver__navigation_calculate_steering(*nav_turning_direction, *nav_turning_strength);
  float nav_car_velocity = driver__navigation_calculate_velocity(
      *nav_turning_strength, dist_to_next_checkpoint, is_final_checkpoint, obstacles_threshold_3, nav_car_steering);
  // Set values for the two output parameters in the correct destination by calling these set value function
  driver__set_steering_to_send_to_motor_controller(nav_car_steering);
  driver__set_velocity_to_send_to_motor_controller(nav_car_velocity);
}

/// Branch of code that is called for (Obstacle Avoidance) and the function called for unit testing it
void driver__obstacle_avoidance_setup(car_direction_e *car_direction, car_steering_limit_e *car_steering_limit,
                                      car_speed_limit_e *car_speed_limit, uint16_t left_sensor, uint16_t middle_sensor,
                                      uint16_t right_sensor, uint16_t back_sensor) {
  // Store information for obstacles detected at 3 different thresholds relevant to (Obstacle Avoidance)
  obstacle_detected_on_sensors_e obstacles_threshold_0; // Threshold: Used to detect if car stuck in obstacle avoidance
  obstacle_detected_on_sensors_e obstacles_threshold_1; // Threshold: Closer bound of obstacle avoidance case
  obstacle_detected_on_sensors_e obstacles_threshold_2; // Threshold: Farther bound of obstacle avoidance case
  // Figure out which relavant thresholds obstacles are detected at on all sensors
  obstacles_threshold_0 =
      driver__check_sensors_under_OBJECT_THRESHOLD_0(left_sensor, middle_sensor, right_sensor, back_sensor);
  obstacles_threshold_1 =
      driver__check_sensors_under_OBJECT_THRESHOLD_1(left_sensor, middle_sensor, right_sensor, back_sensor);
  obstacles_threshold_2 =
      driver__check_sensors_under_OBJECT_THRESHOLD_2(left_sensor, middle_sensor, right_sensor, back_sensor);
  // Call function for (Obstacle Avoidance) branch of logic
  driver__obstacle_avoidance(car_direction, car_steering_limit, car_speed_limit, obstacles_threshold_0,
                             obstacles_threshold_1, obstacles_threshold_2, left_sensor, middle_sensor, right_sensor,
                             back_sensor);
}

/// Branch of code that is called for (Obstacle Avoidance)
void driver__obstacle_avoidance(car_direction_e *car_direction, car_steering_limit_e *car_steering_limit,
                                car_speed_limit_e *car_speed_limit,
                                obstacle_detected_on_sensors_e obstacles_threshold_0,
                                obstacle_detected_on_sensors_e obstacles_threshold_1,
                                obstacle_detected_on_sensors_e obstacles_threshold_2, uint16_t left_sensor,
                                uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor) {

  // Store values statically for the two output parameters that go on to be stored
  static float set_car_velocity;
  static float set_car_steering;
  // Process the sensor information and decide the parameters to use in setting the steering and velocity below
  driver__obstacle_avoidance_determine_steering_and_velocity(
      car_direction, car_steering_limit, car_speed_limit, obstacles_threshold_0, obstacles_threshold_1,
      obstacles_threshold_2, left_sensor, middle_sensor, right_sensor, back_sensor);
  // Assign the steering angle and velocity for the car when avoiding obstacles
  set_car_steering = driver__obstacle_avoidance_assign_steering_angle(car_steering_limit);
  set_car_velocity = driver__obstacle_avoidance_assign_velocity(car_speed_limit, car_direction);
  // Set values for the two output parameters in the correct destination by calling these set value function
  driver__set_steering_to_send_to_motor_controller(set_car_steering);
  driver__set_velocity_to_send_to_motor_controller(set_car_velocity);
  return;
}

/*********************************************************************************************************************
 * Private Functions used by BOTH (Navigation to Checkpoint Destination) and (Obstacle Avoidance) branches
 ********************************************************************************************************************/
/// Check sensor values against defined constant thresholds and return bitfields for sensors that are < threshold #
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_0(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor) {
  return driver__helper_check_sensors_under_OBJECT_THRESHOLD_N(left_sensor, middle_sensor, right_sensor, back_sensor,
                                                               OBJECT_THRESHOLD_PRIORITY_0);
}
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_1(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor) {
  return driver__helper_check_sensors_under_OBJECT_THRESHOLD_N(left_sensor, middle_sensor, right_sensor, back_sensor,
                                                               OBJECT_THRESHOLD_PRIORITY_1);
}
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_2(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor) {
  return driver__helper_check_sensors_under_OBJECT_THRESHOLD_N(left_sensor, middle_sensor, right_sensor, back_sensor,
                                                               OBJECT_THRESHOLD_PRIORITY_2);
}
obstacle_detected_on_sensors_e driver__check_sensors_under_OBJECT_THRESHOLD_3(uint16_t left_sensor,
                                                                              uint16_t middle_sensor,
                                                                              uint16_t right_sensor,
                                                                              uint16_t back_sensor) {
  return driver__helper_check_sensors_under_OBJECT_THRESHOLD_N(left_sensor, middle_sensor, right_sensor, back_sensor,
                                                               OBJECT_THRESHOLD_PRIORITY_3);
}
/// Helper function called by all 4 function above to provide required output value using (0b0000BRML) bit convention
obstacle_detected_on_sensors_e
driver__helper_check_sensors_under_OBJECT_THRESHOLD_N(uint16_t left_sensor, uint16_t middle_sensor,
                                                      uint16_t right_sensor, uint16_t back_sensor, uint16_t threshold) {
  obstacle_detected_on_sensors_e ret_sensors_trig = no_sensor_triggered; // Init to 0 for no obstacles detected
  // Sensor Bitmasks
  const uint8_t no_sensor_bitmask = 0b00000000;
  const uint8_t left_sensor_bitmask = 0b00000001;
  const uint8_t middle_sensor_bitmask = 0b00000010;
  const uint8_t right_sensor_bitmask = 0b00000100;
  const uint8_t back_sensor_bitmask = 0b00001000;
  // Bitwise OR each sensor below threshold passed in with it's respective bitmask
  ret_sensors_trig |= (left_sensor < threshold) ? left_sensor_bitmask : no_sensor_bitmask;
  ret_sensors_trig |= (middle_sensor < threshold) ? middle_sensor_bitmask : no_sensor_bitmask;
  ret_sensors_trig |= (right_sensor < threshold) ? right_sensor_bitmask : no_sensor_bitmask;
  ret_sensors_trig |= (back_sensor < threshold) ? back_sensor_bitmask : no_sensor_bitmask;
  return ret_sensors_trig;
}

/*********************************************************************************************************************
 * Private Functions used by ONLY (Navigation to Checkpoint Destination) branch
 ********************************************************************************************************************/
/// Function will return a value between -179 and +180, and when negative, turn left. When positive, turn right
int16_t driver__navigation_deg_offset_between_bearing_and_heading(uint16_t dest_bearing, uint16_t car_heading) {
  int16_t bearing_and_heading_difference = dest_bearing - car_heading;
  if (bearing_and_heading_difference > 180) { // Left
    bearing_and_heading_difference -= 360;    // Make NEGATIVE to ensure logic recognizes it is a Left turn
  } else if (bearing_and_heading_difference < 0 && bearing_and_heading_difference > -180) { // Left
    // bearing_and_heading_difference is already good to go in the correct NEGATIVE range
  } else if (bearing_and_heading_difference <= -180) { // Right
    bearing_and_heading_difference += 360;             // Make POSITIVE to ensure logic recognizes it is a Right turn
  } else if (bearing_and_heading_difference > 0 && bearing_and_heading_difference <= 180) { // Right
    // bearing_and_heading_difference is already good to go in the correct POSITIVE range
  }
  return bearing_and_heading_difference;
}

/// Returns enumeration type for left, straight, or right navigation steering direction
navigation_turning_direction_e driver__navigation_which_direction_to_turn(int16_t bearing_and_heading_dif) {
  navigation_turning_direction_e ret_turning_direction;
  // Assign values based on negative = left turn, 0 = straight, positive = right turn
  if (bearing_and_heading_dif < 0) {
    ret_turning_direction = nav_left;
  } else if (bearing_and_heading_dif == 0) {
    ret_turning_direction = nav_straight;
  } else if (bearing_and_heading_dif > 0) {
    ret_turning_direction = nav_right;
  }
  return ret_turning_direction;
}

/// Returns enumeration type for hard, medium, soft, or no turn based off of abs(bearing_and_heading_dif)
navigation_turning_strength_e driver__navigation_how_hard_to_turn(int16_t bearing_and_heading_dif) {
  navigation_turning_strength_e ret_turning_strength;
  uint16_t steering_strength = abs(bearing_and_heading_dif);
  // Determine the steering conditions based on global const uint16_t values for quicker tuning
  if (steering_strength >= NAV_STEERING_THRESHOLD_HARD_TURN) {
    ret_turning_strength = nav_hard_turn;
  } else if ((steering_strength < NAV_STEERING_THRESHOLD_HARD_TURN) &&
             (steering_strength >= NAV_STEERING_THRESHOLD_MEDIUM_TURN)) {
    ret_turning_strength = nav_medium_turn;
  } else if ((steering_strength < NAV_STEERING_THRESHOLD_MEDIUM_TURN) &&
             (steering_strength >= NAV_STEERING_THRESHOLD_SOFT_TURN)) {
    ret_turning_strength = nav_soft_turn;
  } else if (steering_strength < NAV_STEERING_THRESHOLD_SOFT_TURN) {
    ret_turning_strength = nav_no_turn;
  }
  return ret_turning_strength;
}

/// Calculate steering from navigation_turning_direction_e and navigation_turning_strength_e enums
float driver__navigation_calculate_steering(navigation_turning_direction_e turning_direction,
                                            navigation_turning_strength_e turning_strength) {
  float ret_car_steering_angle_in_radians = NAV_STEERING_LIMIT_NO_TURN; // init for straight steering
  if (turning_strength == nav_hard_turn) {
    ret_car_steering_angle_in_radians = NAV_STEERING_LIMIT_HARD_TURN;
  } else if (turning_strength == nav_medium_turn) {
    ret_car_steering_angle_in_radians = NAV_STEERING_LIMIT_MEDIUM_TURN;
  } else if (turning_strength == nav_soft_turn) {
    ret_car_steering_angle_in_radians = NAV_STEERING_LIMIT_SOFT_TURN;
  } else if (turning_strength == nav_no_turn) {
    ret_car_steering_angle_in_radians = NAV_STEERING_LIMIT_NO_TURN;
  }
  // Next, check whether we need to steer left or right
  if (turning_direction == nav_left) {
    ret_car_steering_angle_in_radians *= -1.0f; // steer left is a negative angle
  }
  return ret_car_steering_angle_in_radians;
}

/// Calculate velocity from navigation_turning_direction_e and navigation_turning_strength_e enums
float driver__navigation_calculate_velocity(navigation_turning_strength_e nav_turning_strength,
                                            float dist_to_next_checkpoint, bool is_final_checkpoint,
                                            obstacle_detected_on_sensors_e obstacles_threshold_3,
                                            float calculated_steering_angle) {
  float ret_car_velocity = NAV_SPEED_LIMIT_STOPPED_DESTINATION_REACHED; // Init speed = 0.0
  // Init the car velocities based on steering strength using global const floats for quicker speed tuning
  if (nav_turning_strength == nav_hard_turn) {
    ret_car_velocity = NAV_SPEED_LIMIT_TURNING_SLOW;
  } else if (nav_turning_strength == nav_medium_turn) {
    ret_car_velocity = NAV_SPEED_LIMIT_TURNING_MEDIUM;
  } else if (nav_turning_strength == nav_soft_turn) {
    ret_car_velocity = NAV_SPEED_LIMIT_TURNING_FAST;
  } else if (nav_turning_strength == nav_no_turn) {
    ret_car_velocity = NAV_SPEED_LIMIT_NOT_TURNING_LUDICROUS_SPEED; // Fastest speed when completely straight
  }
  // Thresholds for distances from the final checkpoint to use in order to get the car to slow down
  const float CHKPNT_DIST_THRESH_4 = 10.0f;
  const float CHKPNT_DIST_THRESH_3 = 7.5f;
  const float CHKPNT_DIST_THRESH_2 = 5.0f;
  const float CHKPNT_DIST_THRESH_1 = 2.5f;
  const float CHKPNT_DIST_THRESH_0 = 0.5f;
  // Next only slow down upon reaching the final checkpoint for smoother stops
  // if (is_final_checkpoint) {
  if (true) { // TODO: get rid of this when testing multiple checkpoints
    if (dist_to_next_checkpoint >= CHKPNT_DIST_THRESH_4) {
      ret_car_velocity *= 1.0f;
    } else if ((dist_to_next_checkpoint >= CHKPNT_DIST_THRESH_3) && (dist_to_next_checkpoint < CHKPNT_DIST_THRESH_4)) {
      ret_car_velocity *= 0.9f;
    } else if ((dist_to_next_checkpoint >= CHKPNT_DIST_THRESH_2) && (dist_to_next_checkpoint < CHKPNT_DIST_THRESH_3)) {
      ret_car_velocity *= 0.8f;
    } else if ((dist_to_next_checkpoint >= CHKPNT_DIST_THRESH_1) && (dist_to_next_checkpoint < CHKPNT_DIST_THRESH_2)) {
      ret_car_velocity *= 0.6f;
    } else if ((dist_to_next_checkpoint >= CHKPNT_DIST_THRESH_0) && (dist_to_next_checkpoint < CHKPNT_DIST_THRESH_1)) {
      ret_car_velocity *= 0.4f;
    } else if (dist_to_next_checkpoint < CHKPNT_DIST_THRESH_0) {
      ret_car_velocity = NAV_SPEED_LIMIT_STOPPED_DESTINATION_REACHED; // Considered done within CHKPNT_DIST_THRESH_0
    }
  }
  // Next apply additional slowdown multiplier if any objects in front sensors within OBJECT_THRESHOLD_PRIORITY_3 dist
  obstacle_detected_on_sensors_e all_front_sensors_bitmask = 0b00000111;
  obstacle_detected_on_sensors_e only_front_obstacles_threshold_3 = obstacles_threshold_3 & all_front_sensors_bitmask;
  if (only_front_obstacles_threshold_3) {
    ret_car_velocity *= 0.75f;
  }
  // Last, apply TEST_SPEED_LIMIT_MULTIPLIER if testing with slower speeds
  return ret_car_velocity *= TEST_SPEED_LIMIT_MULTIPLIER;
}

/*********************************************************************************************************************
 * Private Functions used by ONLY (Obstacle Avoidance) branch
 ********************************************************************************************************************/
/// Assign steering from enums returned by driver__obstacle_avoidance_determine_steering_and_velocity
float driver__obstacle_avoidance_assign_steering_angle(car_steering_limit_e *param_car_steering_limit) {
  float ret_car_steering_angle = OBSTACLE_AVOIDANCE_STEERING_LIMIT_STRAIGHT; // init for car going straight
  switch (*param_car_steering_limit) {
  case hard_left:
    ret_car_steering_angle = OBSTACLE_AVOIDANCE_STEERING_LIMIT_HARD_LEFT;
    break;
  case left:
    ret_car_steering_angle = OBSTACLE_AVOIDANCE_STEERING_LIMIT_LEFT;
    break;
  case straight:
    ret_car_steering_angle = OBSTACLE_AVOIDANCE_STEERING_LIMIT_STRAIGHT;
    break;
  case right:
    ret_car_steering_angle = OBSTACLE_AVOIDANCE_STEERING_LIMIT_RIGHT;
    break;
  case hard_right:
    ret_car_steering_angle = OBSTACLE_AVOIDANCE_STEERING_LIMIT_HARD_RIGHT;
    break;
  default:
    ret_car_steering_angle = OBSTACLE_AVOIDANCE_STEERING_LIMIT_STRAIGHT;
    break;
  }
  float averaged_steering_angle = driver__rolling_average_steering(ret_car_steering_angle);
  return averaged_steering_angle;
}

/// Assign velocity from enums returned by driver__obstacle_avoidance_determine_steering_and_velocity
float driver__obstacle_avoidance_assign_velocity(car_speed_limit_e *param_car_speed_limit,
                                                 car_direction_e *param_car_direction) {
  float ret_car_velocity = 0;
  switch (*param_car_direction) {
  case backwards: // TODO: Velocity is only slow when avoiding obstacles and going backwards
    switch (*param_car_speed_limit) {
    case medium:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_BACKWARD_MEDIUM;
      break;
    case slow:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_BACKWARD_SLOW;
      break;
    default:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_NOT_MOVING;
      break;
    }
    break;
  case forwards:
    switch (*param_car_speed_limit) {
    case ludicrous_speed:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_LUDICROUS_SPEED;
      break;
    case fast:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_FAST;
      break;
    case medium:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_MEDIUM;
      break;
    case slow:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_FORWARD_SLOW;
      break;
    default:
      ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_NOT_MOVING;
      break;
    }
    break;
  case not_moving:
    ret_car_velocity = OBSTACLE_AVOIDANCE_SPEED_LIMIT_NOT_MOVING;
    break;
  }

  // Last, apply TEST_SPEED_LIMIT_MULTIPLIER if testing with slower speeds
  ret_car_velocity *= TEST_SPEED_LIMIT_MULTIPLIER; // TODO: Remove this when finished with all testing
  float averaged_velocity = driver__rolling_average_velocity(ret_car_velocity);
  return averaged_velocity;
}

/// The following function is used when determining extra steering and velocity instructions when avoiding objects
obstacle_closer_on_side_e driver__is_object_closer_left_or_right(uint16_t left_sensor, uint16_t right_sensor) {
  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side = object_same_distance_on_both_sides;
  // See if potential objects are closer to left or right sensor, Only sometimes used to set steering
  if (left_sensor < right_sensor) {
    ret_potential_object_closer_on_a_side = object_closer_on_left_side;
  } else if (left_sensor > right_sensor) {
    ret_potential_object_closer_on_a_side = object_closer_on_right_side;
  } else {
    ret_potential_object_closer_on_a_side = object_same_distance_on_both_sides;
  }
  return ret_potential_object_closer_on_a_side;
}
/// The following function is used when determining extra steering and velocity instructions when avoiding objects
obstacle_closer_on_side_e driver__is_object_closer_front_or_back(uint16_t left_sensor, uint16_t middle_sensor,
                                                                 uint16_t right_sensor, uint16_t back_sensor) {

  obstacle_closer_on_side_e ret_potential_object_closer_on_a_side = object_same_distance_on_both_sides;
  uint16_t smallest_front_sensor_value = left_sensor;
  uint16_t new_middle_sensor = middle_sensor * MIDDLE_SENSOR_MULTIPLIER_DISTANCE; // TODO: Possibly remove this
  // See if either middle/right sensor shorter distances than the left sensor that we initialized and replace value
  if (smallest_front_sensor_value > new_middle_sensor) {
    smallest_front_sensor_value = new_middle_sensor;
  }
  if (smallest_front_sensor_value > right_sensor) {
    smallest_front_sensor_value = right_sensor;
  }
  // Use the shorter value of the front side and see if front or back is closer
  if (smallest_front_sensor_value < back_sensor) {
    ret_potential_object_closer_on_a_side = object_closer_on_front_side;
  } else if (smallest_front_sensor_value > back_sensor) {
    ret_potential_object_closer_on_a_side = object_closer_on_back_side;
  } else {
    ret_potential_object_closer_on_a_side = object_same_distance_on_both_sides;
  }
  return ret_potential_object_closer_on_a_side;
}

/// Multi-hundred line long set of switch case conditions that embody the entirety of obstacle avoidance state machine
void driver__obstacle_avoidance_determine_steering_and_velocity(
    car_direction_e *ret_car_direction, car_steering_limit_e *ret_car_steering_limit,
    car_speed_limit_e *ret_car_speed_limit, obstacle_detected_on_sensors_e obstacles_threshold_0,
    obstacle_detected_on_sensors_e obstacles_threshold_1, obstacle_detected_on_sensors_e obstacles_threshold_2,
    uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor) {

  // Check to see if the car is "stuck" happens when any front sensor AND back sensor are < OBJECT_THRESHOLD_PRIORITY_0
  switch (obstacles_threshold_0) { // Stuck condtion detected in following cases
  case back_left_triggered:
  case back_middle_triggered:
  case back_middle_left_triggered:
  case back_right_triggered:
  case back_right_left_triggered:
  case back_right_middle_triggered:
  case all_front_and_back_sensor_are_triggered:
    driver__oa_helper_determine_steering_and_velocity_stuck(ret_car_direction, ret_car_steering_limit,
                                                            ret_car_speed_limit);
    break;
  default: // Stuck condtion NOT detected, proceed with normal obstacle avoidance logic
    driver__oa_helper_determine_steering_and_velocity_not_stuck(
        ret_car_direction, ret_car_steering_limit, ret_car_speed_limit, obstacles_threshold_0, obstacles_threshold_1,
        obstacles_threshold_2, left_sensor, middle_sensor, right_sensor, back_sensor);
    break;
  }
  return;
}

/// Assign return pointers with values that correspond to the car being stuck
void driver__oa_helper_determine_steering_and_velocity_stuck(car_direction_e *ret_car_direction,
                                                             car_steering_limit_e *ret_car_steering_limit,
                                                             car_speed_limit_e *ret_car_speed_limit) {
  *ret_car_direction = not_moving;
  *ret_car_steering_limit = straight;
  *ret_car_speed_limit = stopped;
}

/// Assign return pointers with values that correspond to the car not being stuck by navigating through switch cases
void driver__oa_helper_determine_steering_and_velocity_not_stuck(
    car_direction_e *ret_car_direction, car_steering_limit_e *ret_car_steering_limit,
    car_speed_limit_e *ret_car_speed_limit, obstacle_detected_on_sensors_e obstacles_threshold_0,
    obstacle_detected_on_sensors_e obstacles_threshold_1, obstacle_detected_on_sensors_e obstacles_threshold_2,
    uint16_t left_sensor, uint16_t middle_sensor, uint16_t right_sensor, uint16_t back_sensor) {

  // See if potential objects are closer to left or right sensor
  obstacle_closer_on_side_e object_closer_left_or_right =
      driver__is_object_closer_left_or_right(left_sensor, right_sensor);
  // See if potential objects are closer to lowest value of front sensor or back sensor
  obstacle_closer_on_side_e object_closer_front_or_back =
      driver__is_object_closer_front_or_back(left_sensor, middle_sensor, right_sensor, back_sensor);

  // Begin checking obstacles_threshold_2 (upper bound) and sub cases obstacles_threshold_1 (lower bound)
  // for obstacle avoidance and che
  switch (obstacles_threshold_2) {
  case left_triggered:
    switch (obstacles_threshold_1) {
    case left_triggered:
      switch (obstacles_threshold_0) {
      case left_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = right;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Back Left triggered ///////////////////////////////
  case back_left_triggered:
    switch (obstacles_threshold_1) {
    case left_triggered: // Check obstacles_threshold_1 condition 0
      switch (obstacles_threshold_0) {
      case left_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case back_triggered: // Check obstacles_threshold_1 condition 1
      switch (obstacles_threshold_0) {
      case back_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = medium; // Potentially change
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case back_left_triggered: // Check obstacles_threshold_1 condition 2
      switch (object_closer_front_or_back) {
      case object_closer_on_front_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_back_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change * probably hard_left
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = right;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Middle triggered ///////////////////////////////
  case middle_triggered: // Slow speeds only
    switch (obstacles_threshold_1) {
    case middle_triggered: // Check special condition 0
      switch (object_closer_left_or_right) {
      case object_closer_on_left_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_right_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      switch (object_closer_left_or_right) {
      case object_closer_on_left_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_right_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    }
    break;

  /////////////////////////////// Back Middle triggered ///////////////////////////////
  case back_middle_triggered: // Slow speeds only
    switch (obstacles_threshold_1) {
    case middle_triggered: // Check special condition 0
      switch (object_closer_left_or_right) {
      case object_closer_on_left_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_right_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case back_triggered: // Check special condition 1
      switch (object_closer_left_or_right) {
      case object_closer_on_left_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_right_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = straight; // potentially change
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case back_middle_triggered: // Check special condition 2
      driver__oa_helper_threshold_1_back_middle_case(ret_car_direction, ret_car_steering_limit, ret_car_speed_limit,
                                                     obstacles_threshold_1, object_closer_left_or_right,
                                                     object_closer_front_or_back);
      break;
    default:
      switch (object_closer_left_or_right) {
      case object_closer_on_left_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_right_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    }
    break;

  /////////////////////////////// Middle Left triggered ///////////////////////////////
  case middle_left_triggered:
    switch (obstacles_threshold_1) {
    case left_triggered:         // Check obstacles_threshold_1 condition 0
      switch (obstacles_threshold_0) {
      case left_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case middle_triggered:      // Check obstacles_threshold_1 condition 1
    switch (obstacles_threshold_0) {
      case middle_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case middle_left_triggered: // Check obstacles_threshold_1 condition 2
    switch (obstacles_threshold_0) {
      case left_triggered:  // Check obstacles_threshold_0 condition 0
      case middle_triggered: // Check obstacles_threshold_0 condition 1
      case middle_left_triggered: // Check obstacles_threshold_0 condition 2
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = hard_right;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Back Middle Left triggered ///////////////////////////////
  case back_middle_left_triggered:
    switch (obstacles_threshold_1) {
    case left_triggered:        // Check special condition 0
    case middle_triggered:      // Check special condition 1
    case middle_left_triggered: // Check special condition 2
      *ret_car_direction = backwards;
      *ret_car_steering_limit = hard_left;
      *ret_car_speed_limit = slow;
      break;
    case back_middle_triggered: // Check special condition 3
      driver__oa_helper_threshold_1_back_middle_case(ret_car_direction, ret_car_steering_limit, ret_car_speed_limit,
                                                     obstacles_threshold_1, object_closer_left_or_right,
                                                     object_closer_front_or_back);
      break;
    case back_left_triggered:        // Check special condition 4
    case back_middle_left_triggered: // Check special condition 5
      switch (object_closer_front_or_back) {
      case object_closer_on_front_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_back_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change * probably hard_left
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = hard_right;
      *ret_car_speed_limit = slow;
      break;
    }
    break;

  /////////////////////////////// Right triggered ///////////////////////////////
  case right_triggered:
    switch (obstacles_threshold_1) {
    case right_triggered:
      switch (obstacles_threshold_0) {
      case right_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = left;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Back Right triggered ///////////////////////////////
  case back_right_triggered: // Medium speeds allowed on default
    switch (obstacles_threshold_1) {
    case right_triggered: // Check obstacles_threshold_1 condition 0
      switch (obstacles_threshold_0) {
      case right_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case back_triggered: // Check obstacles_threshold_1 condition 1
      switch (obstacles_threshold_0) {
      case back_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = medium; // Potentially change
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case back_right_triggered: // Check special condition 2

      switch (object_closer_front_or_back) {
      case object_closer_on_front_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_back_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change * probably hard_right
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = right;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Right Left triggered ///////////////////////////////
  case right_left_triggered: // TODO: Revamp the logic for this case
    switch (obstacles_threshold_1) {
    case left_triggered:
      switch (obstacles_threshold_0) {
      case left_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case right_triggered:
      switch (obstacles_threshold_0) {
      case right_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case right_left_triggered:
      switch (obstacles_threshold_0) {
      case left_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = left;
        *ret_car_speed_limit = slow;
        break;
      case right_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = right;
        *ret_car_speed_limit = slow;
        break;
      case right_left_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight;
        *ret_car_speed_limit = slow;
        break;
      default:
        switch (object_closer_left_or_right) {
        case object_closer_on_left_side:
          *ret_car_direction = forwards;        // potentially change
          *ret_car_steering_limit = hard_right; // potentially change
          *ret_car_speed_limit = slow;          // potentially change
          break;
        case object_closer_on_right_side:
          *ret_car_direction = forwards;       // potentially change
          *ret_car_steering_limit = hard_left; // potentially change
          *ret_car_speed_limit = slow;         // potentially change
          break;
        default:
          *ret_car_direction = forwards;      // potentially change
          *ret_car_steering_limit = straight; // potentially change
          *ret_car_speed_limit = slow;        // potentially change
          break;
        }
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = straight;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Back Right Left triggered ///////////////////////////////
  case back_right_left_triggered: // TODO: Revamp the logic for this case
    switch (obstacles_threshold_1) {
    case left_triggered:
      switch (obstacles_threshold_0) {
      case left_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case right_triggered:
      switch (obstacles_threshold_0) {
      case right_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case right_left_triggered:
      switch (obstacles_threshold_0) {
      case left_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = left;
        *ret_car_speed_limit = slow;
        break;
      case right_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = right;
        *ret_car_speed_limit = slow;
        break;
      case right_left_triggered:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight;
        *ret_car_speed_limit = slow;
        break;
      default:
        switch (object_closer_left_or_right) {
        case object_closer_on_left_side:
          *ret_car_direction = forwards;        // potentially change
          *ret_car_steering_limit = hard_right; // potentially change
          *ret_car_speed_limit = slow;          // potentially change
          break;
        case object_closer_on_right_side:
          *ret_car_direction = forwards;       // potentially change
          *ret_car_steering_limit = hard_left; // potentially change
          *ret_car_speed_limit = slow;         // potentially change
          break;
        default:
          *ret_car_direction = forwards;      // potentially change
          *ret_car_steering_limit = straight; // potentially change
          *ret_car_speed_limit = slow;        // potentially change
          break;
        }
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = straight;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Right Middle triggered ///////////////////////////////
  case right_middle_triggered:
  switch (obstacles_threshold_1) {
    case right_triggered:         // Check obstacles_threshold_1 condition 0
      switch (obstacles_threshold_0) {
      case right_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case middle_triggered:      // Check obstacles_threshold_1 condition 1
    switch (obstacles_threshold_0) {
      case middle_triggered: // Check obstacles_threshold_0 condition 0
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    case right_middle_triggered: // Check obstacles_threshold_1 condition 2
    switch (obstacles_threshold_0) {
      case right_triggered:  // Check obstacles_threshold_0 condition 0
      case middle_triggered: // Check obstacles_threshold_0 condition 1
      case right_middle_triggered: // Check obstacles_threshold_0 condition 2
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = hard_left;
      *ret_car_speed_limit = medium;
      break;
    }
    break;

  /////////////////////////////// Back Right Middle triggered ///////////////////////////////
  case back_right_middle_triggered:
    switch (obstacles_threshold_1) {
    case middle_triggered:       // Check special condition 0
    case right_triggered:        // Check special condition 1
    case right_middle_triggered: // Check special condition 2
      *ret_car_direction = backwards;
      *ret_car_steering_limit = hard_right;
      *ret_car_speed_limit = slow;
      break;
    case back_middle_triggered: // Check special condition 3
      driver__oa_helper_threshold_1_back_middle_case(ret_car_direction, ret_car_steering_limit, ret_car_speed_limit,
                                                     obstacles_threshold_1, object_closer_left_or_right,
                                                     object_closer_front_or_back);
      break;
    case back_right_triggered:        // Check special condition 4
    case back_right_middle_triggered: // Check special condition 5
      switch (object_closer_front_or_back) {
      case object_closer_on_front_side:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = hard_right;
        *ret_car_speed_limit = slow;
        break;
      case object_closer_on_back_side:
        *ret_car_direction = forwards;
        *ret_car_steering_limit = hard_left;
        *ret_car_speed_limit = slow;
        break;
      default:
        *ret_car_direction = backwards;
        *ret_car_steering_limit = straight; // potentially change * probably hard_right
        *ret_car_speed_limit = slow;
        break;
      }
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = hard_left;
      *ret_car_speed_limit = slow;
      break;
    }
    break;


    // TODO: Come back to implement the rest of this soon
  /////////////////////////////// Right Middle Left triggered ///////////////////////////////
  case all_front_sensors_triggered: // No need to check other statements, just go backwards for now
    // turn_around_for_x_amount_of_time_and_choose_steering_direction_from_logic_below()
    switch (object_closer_left_or_right) {
    case object_closer_on_left_side:
      *ret_car_direction = backwards;
      *ret_car_steering_limit = hard_left;
      *ret_car_speed_limit = slow;
      break;
    case object_closer_on_right_side:
      *ret_car_direction = backwards;
      *ret_car_steering_limit = hard_right;
      *ret_car_speed_limit = slow;
      break;
    default:
      *ret_car_direction = backwards;
      *ret_car_steering_limit = straight; // potentially change
      *ret_car_speed_limit = slow;
      break;
    }
    break;

  /////////////////////////////// Back Right Middle Left triggered ///////////////////////////////
  case all_front_and_back_sensor_are_triggered:
    // *ret_car_direction = not_moving;
    // *ret_car_steering_limit = straight;
    // *ret_car_speed_limit = stopped;
    // break;
    driver__oa_helper_threshold_1_back_middle_case(ret_car_direction, ret_car_steering_limit, ret_car_speed_limit,
                                                   obstacles_threshold_1, object_closer_left_or_right,
                                                   object_closer_front_or_back);
    break;
  // Skip these, as obstacle avoidance branch will never be triggered when these cases occur, navigation happens
  // case back_triggered:
  // case no_sensor_triggered:
  default:
    *ret_car_direction = not_moving;
    *ret_car_steering_limit = straight;
    *ret_car_speed_limit = stopped;
    break;
  }
}

/// Lengthy edgecase used in function above that uses ~60 lines of code, no need to copy and paste multiple times
void driver__oa_helper_threshold_1_back_middle_case(car_direction_e *ret_car_direction,
                                                    car_steering_limit_e *ret_car_steering_limit,
                                                    car_speed_limit_e *ret_car_speed_limit,
                                                    obstacle_detected_on_sensors_e obstacles_threshold_1,
                                                    obstacle_closer_on_side_e object_closer_left_or_right,
                                                    obstacle_closer_on_side_e object_closer_front_or_back) {
  switch (object_closer_front_or_back) {
  case object_closer_on_front_side:
    switch (object_closer_left_or_right) {

    case object_closer_on_left_side:
      *ret_car_direction = backwards;
      *ret_car_steering_limit = hard_left;
      *ret_car_speed_limit = slow;
      break;
    case object_closer_on_right_side:
      *ret_car_direction = backwards;
      *ret_car_steering_limit = hard_right;
      *ret_car_speed_limit = slow;
      break;
    default:
      *ret_car_direction = backwards;
      *ret_car_steering_limit = straight; // potentially change
      *ret_car_speed_limit = slow;
      break;
    }
    break;
  case object_closer_on_back_side:
    switch (object_closer_left_or_right) {

    case object_closer_on_left_side:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = hard_right;
      *ret_car_speed_limit = slow;
      break;
    case object_closer_on_right_side:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = hard_left;
      *ret_car_speed_limit = slow;
      break;
    default:
      *ret_car_direction = forwards;
      *ret_car_steering_limit = straight; // potentially change
      *ret_car_speed_limit = slow;
      break;
    }
    break;
  default: // This state is an unlikely edge case
    switch (object_closer_left_or_right) {

    case object_closer_on_left_side:
      *ret_car_direction = backwards;      // potentially change
      *ret_car_steering_limit = hard_left; // potentially change
      *ret_car_speed_limit = slow;         // potentially change
      break;
    case object_closer_on_right_side:
      *ret_car_direction = backwards;       // potentially change
      *ret_car_steering_limit = hard_right; // potentially change
      *ret_car_speed_limit = slow;          // potentially change
      break;
    default:
      *ret_car_direction = backwards;     // potentially change
      *ret_car_steering_limit = straight; // potentially change
      *ret_car_speed_limit = slow;        // potentially change
      break;
    }
    break;
  }
  return;
}

/*********************************************************************************************************************
 * Private functions used for averaging
 ********************************************************************************************************************/
float average_n_samples(float *array_containing_values, uint8_t num_of_samples_to_avg) {
  float total_to_avg = 0;
  for (uint8_t idx = 0; idx < num_of_samples_to_avg; idx++) {
    total_to_avg += array_containing_values[idx];
  }
  return total_to_avg / num_of_samples_to_avg;
}
float driver__rolling_average_steering(float curr_car_steering) {
  const uint8_t num_of_samples_to_avg = NUM_VELOCITY_SAMPLES_TO_AVEREAGE;
  static uint32_t counter;
  static float last_n_values[NUM_STEERING_SAMPLES_TO_AVEREAGE];
  last_n_values[counter % num_of_samples_to_avg] = curr_car_steering;
  counter += 1;
  return average_n_samples(last_n_values, num_of_samples_to_avg);
}
float driver__rolling_average_velocity(float curr_car_velocity) {
  const uint8_t num_of_samples_to_avg = NUM_VELOCITY_SAMPLES_TO_AVEREAGE;
  static uint32_t counter;
  static float last_n_values[NUM_VELOCITY_SAMPLES_TO_AVEREAGE];
  last_n_values[counter % num_of_samples_to_avg] = curr_car_velocity;
  counter += 1;
  return average_n_samples(last_n_values, num_of_samples_to_avg);
}

/*********************************************************************************************************************
 * Private fetch value functions
 ********************************************************************************************************************/
float driver__fetch_dist_to_next_checkpoint(void) { return rx_geo_controller->current_distance_destination; }
int16_t driver__fetch_dest_bearing_to_next_checkpoint(void) { return rx_geo_controller->current_destination_bearing; }
int16_t driver__fetch_car_heading_to_next_checkpoint(void) { return rx_geo_controller->current_compass_heading; }
bool driver__fetch_checkpoint_message_is_final_checkpoint(void) { return rx_checkpoint_message->is_final_checkpoint; }
uint16_t driver__fetch_left_sensor_distance(void) { return rx_sensor_controller->left_sensor; }
uint16_t driver__fetch_middle_sensor_distance(void) { return rx_sensor_controller->middle_sensor; }
uint16_t driver__fetch_right_sensor_distance(void) { return rx_sensor_controller->right_sensor; }
uint16_t driver__fetch_back_sensor_distance(void) { return rx_sensor_controller->back_sensor; }

/*********************************************************************************************************************
 * Public fetch value functions (used only by the unit test for this file)
 ********************************************************************************************************************/
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_0(void) { return OBJECT_THRESHOLD_PRIORITY_0; }
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_1(void) { return OBJECT_THRESHOLD_PRIORITY_1; }
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_2(void) { return OBJECT_THRESHOLD_PRIORITY_2; }
uint16_t driver__fetch_OBJECT_THRESHOLD_PRIORITY_3(void) { return OBJECT_THRESHOLD_PRIORITY_3; }

/*********************************************************************************************************************
 * Private set value functions
 ********************************************************************************************************************/
/// Set velocity and steering data in proper variables to be sent out over can bus to motor contoller
void driver__set_velocity_to_send_to_motor_controller(float param_set_car_velocity) {
  tx_to_motor_controller->current_velocity = param_set_car_velocity;
}
void driver__set_steering_to_send_to_motor_controller(float param_set_car_steering) {
  tx_to_motor_controller->current_steering_angle = param_set_car_steering;
}

/*********************************************************************************************************************
 * Public functions used to get velocity and steering data
 ********************************************************************************************************************/
void driver__copy_steering_and_velocity_values_for_transmission(dbc_DRIVER_STEERING_s *driver_steering_message) {
  driver_steering_message->DRIVER_STEERING_yaw = tx_to_motor_controller->current_steering_angle;
  driver_steering_message->DRIVER_STEERING_velocity = tx_to_motor_controller->current_velocity;
}

float driver__retrieve_motor_controller_velocity(void) { return tx_to_motor_controller->current_velocity; }

float driver__retrieve_motor_controller_steering_yaw(void) { return tx_to_motor_controller->current_steering_angle; }

/*********************************************************************************************************************
 * Public debug functions for printing off values
 ********************************************************************************************************************/
// void driver__print_states_of_car(void) {
//   switch (sensors_triggered) {
//   case full_speed_towards_destination:
//     printf("Sensors: full_speed_towards_destination\r\n");
//     break;
//   case all_front_sensors_triggered:
//     printf("Sensors: all_front_sensors_triggered\r\n");
//     break;
//   case no_sensor_triggered:
//     printf("Sensors: no_sensor_triggered\r\n");
//     break;
//   case left_triggered:
//     printf("Sensors: left_triggered\r\n");
//     break;
//   case middle_triggered:
//     printf("Sensors: middle_triggered\r\n");
//     break;
//   case right_triggered:
//     printf("Sensors: right_triggered\r\n");
//     break;
//   case middle_left_triggered:
//     printf("Sensors: middle_left_triggered\r\n");
//     break;
//   case right_middle_triggered:
//     printf("Sensors: right_middle_triggered\r\n");
//     break;
//   case back_triggered:
//     printf("Sensors: back_triggered\r\n");
//     break;
//   case all_front_and_back_sensor_are_triggered:
//     printf("Sensors: all_front_and_back_sensor_are_triggered\r\n");
//     break;

//   default:
//     printf("Sensors: unhandled: %d\r\n", sensors_triggered);
//     break;
//   }

//   switch (car_direction) {
//   case backwards:
//     printf("Direction: Backwards\r\n");
//     break;
//   case not_moving:
//     printf("Direction: Not Moving\r\n");
//     break;
//   case forwards:
//     printf("Direction: Forwards\r\n");
//     break;
//   }

//   switch (car_speed_limit) {
//   case stopped:
//     printf("Speed Limit: stopped\r\n");
//     break;
//   case slow:
//     printf("Speed Limit: slow\r\n");
//     break;
//   case medium:
//     printf("Speed Limit: medium\r\n");
//     break;
//   case fast:
//     printf("Speed Limit: fast\r\n");
//     break;
//   case ludicrous_speed:
//     printf("Speed Limit: ludicrous_speed\r\n");
//     break;

//   default:
//     printf("unhandled speed: %d\r\n", car_speed_limit);
//     break;
//   }

//   switch (potential_object_closer_on_a_side) {
//   case object_closer_on_left_side:
//     printf("Object closer on left side\r\n\n");
//     break;
//   case object_same_distance_on_both_sides:
//     printf("Object same distance on both sides\r\n\n");
//     break;
//   case object_closer_on_right_side:
//     printf("Object closer on right side\r\n\n");
//     break;
//   }
// }

// void driver__print_sensor_data(void) {
//   printf("Sensor Data Left:   %d\r\n", rx_sensor_controller->left_sensor);
//   printf("Sensor Data Middle: %d\r\n", rx_sensor_controller->middle_sensor);
//   printf("Sensor Data Right:  %d\r\n", rx_sensor_controller->right_sensor);
//   printf("Sensor Data Back:   %d\r\n", rx_sensor_controller->back_sensor);
//   printf("Sensor Data Frame:  %d\r\n\n", rx_sensor_controller->current_sensor_frame_id);
// }

// void driver__print_steering_and_velocity(void) {
//   printf("Velocity Data: %f\r\n", (double)car_velocity);
//   printf("Steering Data: %f\r\n\n", (double)car_steering);
// }

// void driver__print_geo_status_message(void) {
//   printf("Compass Heading: %d\r\n", rx_geo_controller->current_compass_heading);
//   printf("Destination Bearing: %d\r\n\n", rx_geo_controller->current_destination_bearing);
// }

// void driver__print_current_car_location_message(void) {
//   printf("Car Latitude:  %f\r\n", (double)rx_car_coordinates->latitude);
//   printf("Car Longitude: %f\r\n\n", (double)rx_car_coordinates->longitude);
// }

// void driver__print_final_destination_location_message(void) {
//   printf("Final Destination Latitude:  %f\r\n", (double)rx_final_dest_coordinates->latitude);
//   printf("Final Destination Longitude: %f\r\n\n", (double)rx_final_dest_coordinates->longitude);
// }

// void driver__print_next_checkpoint_location_message(void) {
//   printf("Checkpoint Status:   %d\r\n", rx_checkpoint_message->checkpoint_status);
//   printf("Curr Checkpoint Number:   %d\r\n", rx_checkpoint_message->curr_checkpoint_number);
//   printf("Total Checkpoint Number:  %d\r\n", rx_checkpoint_message->curr_checkpoint_number);
//   printf("Is it the final checkpoint: %d\r\n\n", rx_checkpoint_message->is_final_checkpoint);
//   printf("Checkpoint Latitude:  %f\r\n", (double)rx_checkpoint_message->latitude);
//   printf("Checkpoint Longitude: %f\r\n\n", (double)rx_checkpoint_message->longitude);
// }

// void driver__printf_data_struct_pointer_addresses(void) {
//   printf("rx_car_coordinates        %x\n\r", (unsigned int)rx_car_coordinates);
//   printf("rx_final_dest_coordinates %x\n\r", (unsigned int)rx_final_dest_coordinates);
//   printf("rx_checkpoint_message     %x\n\r", (unsigned int)rx_checkpoint_message);
//   printf("rx_navigation_message     %x\n\r", (unsigned int)rx_navigation_message);
//   printf("rx_geo_controller         %x\n\r", (unsigned int)rx_geo_controller);
//   printf("rx_motor_controller       %x\n\r", (unsigned int)rx_motor_controller);
//   printf("rx_sensor_controller      %x\n\r", (unsigned int)rx_sensor_controller);
//   printf("tx_to_motor_controller    %x\n\n\r", (unsigned int)tx_to_motor_controller);
// }

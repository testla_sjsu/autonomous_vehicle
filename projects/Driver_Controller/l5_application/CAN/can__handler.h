#pragma once

void can_handler__init(void);
void can_handler__read_messages_from_bus(void);
void can_handler__transmit_messages_10hz(void);
void can_handler__health_check(void);
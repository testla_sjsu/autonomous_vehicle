// #include "can_handler.h"
// #include "unity.h"

// #include "Mockboard_io.h"
// #include "Mockcan_bus.h"
// #include "Mockgeo.h"
// #include "Mockgpio.h"
// #include "Mockgps.h"

// void test_can_handler__init(void) {
//   can__init_ExpectAndReturn(0, 0, 0, 0, NULL, NULL, true);
//   can__init_IgnoreArg_can();
//   can__init_IgnoreArg_baudrate_kbps();
//   can__init_IgnoreArg_rxq_size();
//   can__init_IgnoreArg_txq_size();
//   can__init_IgnoreArg_bus_off_cb();
//   can__init_IgnoreArg_data_ovr_cb();

//   can__bypass_filter_accept_all_msgs_Expect();

//   can__reset_bus_Expect(0);
//   can__reset_bus_IgnoreArg_can();

//   can_handler__init();
// }

// void test_can_handler__handle_all_incoming_messages_no_message(void) {
//   can__rx_ExpectAndReturn(0, NULL, 0, false);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();
//   can_handler__handle_all_incoming_messages();
// }

// void test_can_handler__handle_all_incoming_messages_irrelevant_message(void) {
//   dbc_GEO_STATUS_s can_msg__geo_status = {};
//   can__msg_t can_msg = {};
//   const dbc_message_header_t header = dbc_encode_GEO_STATUS(can_msg.data.bytes, &can_msg__geo_status);
//   can_msg.msg_id = header.message_id;
//   can_msg.frame_fields.data_len = header.message_dlc;

//   // one irrelevant message
//   can__rx_ExpectAndReturn(0, NULL, 0, true);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();
//   can__rx_ReturnThruPtr_can_message_ptr(&can_msg);

//   // no more messages
//   can__rx_ExpectAndReturn(0, NULL, 0, false);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();

//   can_handler__handle_all_incoming_messages();
// }

// void test_can_handler__handle_all_incoming_messages_destination_message(void) {
//   dbc_SENSOR_DESTINATION_LOCATION_s can_msg__dest = {};
//   can__msg_t can_msg = {};
//   const dbc_message_header_t header = dbc_encode_SENSOR_DESTINATION_LOCATION(can_msg.data.bytes, &can_msg__dest);
//   can_msg.msg_id = header.message_id;
//   can_msg.frame_fields.data_len = header.message_dlc;

//   // one destination message
//   can__rx_ExpectAndReturn(0, NULL, 0, true);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();
//   can__rx_ReturnThruPtr_can_message_ptr(&can_msg);

//   geo__update_dest_Expect(NULL);
//   geo__update_dest_IgnoreArg_msg();

//   // no more messages
//   can__rx_ExpectAndReturn(0, NULL, 0, false);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();

//   can_handler__handle_all_incoming_messages();
// }

// void test_can_handler__manage_mia_10Hz_message_present(void) {
//   // no messages for 400ms
//   for (uint16_t i = 0; i < dbc_mia_threshold_SENSOR_DESTINATION_LOCATION - mia_increment_value;
//        i += mia_increment_value) {
//     can_handler__manage_mia_10Hz();
//   }

//   // one message
//   dbc_SENSOR_DESTINATION_LOCATION_s can_msg__dest = {};
//   can__msg_t can_msg = {};
//   const dbc_message_header_t header = dbc_encode_SENSOR_DESTINATION_LOCATION(can_msg.data.bytes, &can_msg__dest);
//   can_msg.msg_id = header.message_id;
//   can_msg.frame_fields.data_len = header.message_dlc;

//   // one destination message
//   can__rx_ExpectAndReturn(0, NULL, 0, true);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();
//   can__rx_ReturnThruPtr_can_message_ptr(&can_msg);

//   geo__update_dest_Expect(NULL);
//   geo__update_dest_IgnoreArg_msg();

//   // no more messages
//   can__rx_ExpectAndReturn(0, NULL, 0, false);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();

//   can_handler__handle_all_incoming_messages();
//   can_handler__manage_mia_10Hz();
// }

// void test_can_handler__manage_mia_10Hz_message_missing(void) {
//   // one message
//   dbc_SENSOR_DESTINATION_LOCATION_s can_msg__dest = {};
//   can__msg_t can_msg = {};
//   const dbc_message_header_t header = dbc_encode_SENSOR_DESTINATION_LOCATION(can_msg.data.bytes, &can_msg__dest);
//   can_msg.msg_id = header.message_id;
//   can_msg.frame_fields.data_len = header.message_dlc;

//   // one destination message
//   can__rx_ExpectAndReturn(0, NULL, 0, true);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();
//   can__rx_ReturnThruPtr_can_message_ptr(&can_msg);

//   geo__update_dest_Expect(NULL);
//   geo__update_dest_IgnoreArg_msg();

//   // no more messages
//   can__rx_ExpectAndReturn(0, NULL, 0, false);
//   can__rx_IgnoreArg_can();
//   can__rx_IgnoreArg_can_message_ptr();
//   can__rx_IgnoreArg_timeout_ms();

//   can_handler__handle_all_incoming_messages();

//   // 400ms missing messages
//   for (uint16_t i = 0; i < dbc_mia_threshold_SENSOR_DESTINATION_LOCATION - mia_increment_value;
//        i += mia_increment_value) {
//     can_handler__manage_mia_10Hz();
//   }

//   // last missing message
//   gpio_s gpio = {};
//   gpio__set_Expect(gpio);
//   board_io__get_led2_ExpectAndReturn(gpio);
//   can_handler__manage_mia_10Hz();
// }

// //#include "unity.h"

// // #include "can_bus__manager.h"

// // #include "Mockcan_bus.h"

// // void test_initialize_can_bus(void) {
// //   can__init_ExpectAnyArgsAndReturn(true);
// //   can__bypass_filter_accept_all_msgs_Expect();
// //   can__reset_bus_Expect(can1);

// //   TEST_ASSERT_TRUE(can_bus__initialize(can1, 100, 50, 50));
// // }

// // void test_can_bus_needs_reset(void) {
// //   can__is_bus_off_ExpectAndReturn(can1, true);
// //   can__reset_bus_Expect(can1);

// //   TEST_ASSERT_TRUE(can_bus__if_needs_reset(can1));
// // }

// // void test_can_bus_does_not_need_reset(void) {
// //   can__is_bus_off_ExpectAndReturn(can1, false);

// //   TEST_ASSERT_FALSE(can_bus__if_needs_reset(can1));
// // }

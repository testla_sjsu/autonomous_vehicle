#include "can__handler.h"
#include "board_io.h"
#include "can_bus.h"
#include "gpio.h"
#include "testla_proto1.h"
#include <stdio.h>

#include "driver_logic.h"

static dbc_SENSOR_SONARS_s can_msg__sonar;
static dbc_MOTOR_HEARTBEAT_s can_msg__motor;
static dbc_GEO_STATUS_s can_msg__geo;
static dbc_SENSOR_DESTINATION_LOCATION_s can_msg__destination_location;
static dbc_DEBUG_GPS_CURRENT_LOCATION_s can_msg__car_location;
// static dbc_DEBUG_NEXT_CHECKPOINT_LOCATION_s can_msg__next_checkpoint_location;

void can_handler__init(void) {
  if (!can__init(can1, 500, 1024, 1024, NULL, NULL)) {
    fprintf(stderr, "failed to init CAN\n");
  } else {
    fprintf(stderr, "can running\n");
  }
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(can1);
}

void can_handler__transmit_messages_10hz(void) {
  // bool switch_zero = gpio__get(board_io__get_sw0());
  // bool switch_one = gpio__get(board_io__get_sw1());
  // bool switch_two = gpio__get(board_io__get_sw2());

  // DRIVER_STEERING_AND_VELOCITY_s copy_of_commands_to_send;
  // copy_of_commands_to_send.CURRENT_VELOCITY = driver__retrieve_motor_controller_velocity();
  // copy_of_commands_to_send.CURRENT_STEERING_ANGLE = driver__retrieve_motor_controller_steering_yaw();

  // printf("Velocity Data: %f\r\n", copy_of_commands_to_send.CURRENT_VELOCITY);
  // printf("Steering Data: %f\r\n", copy_of_commands_to_send.CURRENT_STEERING_ANGLE);

  // dbc_DRIVER_STEERING_s driver_steering_message = {
  //     .DRIVER_STEERING_yaw = copy_of_commands_to_send.CURRENT_VELOCITY,
  //     .DRIVER_STEERING_velocity = copy_of_commands_to_send.CURRENT_VELOCITY,
  // };

  dbc_DRIVER_STEERING_s driver_steering_message = {};

  driver_steering_message.DRIVER_STEERING_yaw = driver__retrieve_motor_controller_steering_yaw();
  driver_steering_message.DRIVER_STEERING_velocity = driver__retrieve_motor_controller_velocity();

  // printf("Velocity Data: %f\r\n", driver_steering_message.DRIVER_STEERING_velocity);
  // printf("Steering Data: %f\r\n\n", driver_steering_message.DRIVER_STEERING_yaw);

  // driver__copy_steering_and_velocity_values_for_transmission(&driver_steering_message);

  // driver_steering_message.DRIVER_STEERING_yaw = 0.1;
  // driver_steering_message.DRIVER_STEERING_velocity = 0.5;

  can__msg_t can_msg = {};
  const dbc_message_header_t header = dbc_encode_DRIVER_STEERING(can_msg.data.bytes, &driver_steering_message);

  can_msg.msg_id = header.message_id;
  can_msg.frame_fields.data_len = header.message_dlc;
  can__tx(can1, &can_msg, 0);
}

void can_handler__read_messages_from_bus(void) {
  can__msg_t can_msg = {};

  while (can__rx(can1, &can_msg, 0)) {
    const dbc_message_header_t header = {
        .message_id = can_msg.msg_id,
        .message_dlc = can_msg.frame_fields.data_len,
    };

    if (dbc_decode_SENSOR_SONARS(&can_msg__sonar, header, can_msg.data.bytes)) {
      driver__receive_latest_sensor_bridge_status(&can_msg__sonar);
    } else if (dbc_decode_GEO_STATUS(&can_msg__geo, header, can_msg.data.bytes)) {
      driver__receive_latest_geo_controller_status(&can_msg__geo);
      // } else if (dbc_decode_MOTOR_HEARTBEAT(&can_msg__motor, header, can_msg.data.bytes)) {
      //   driver__receive_latest_motor_controller_status(&can_msg__motor);
    } else if (dbc_decode_SENSOR_DESTINATION_LOCATION(&can_msg__destination_location, header, can_msg.data.bytes)) {
      driver__receive_destination_coordinates(&can_msg__destination_location);
    } else if (dbc_decode_DEBUG_GPS_CURRENT_LOCATION(&can_msg__car_location, header, can_msg.data.bytes)) {
      driver__receive_car_coordinates(&can_msg__car_location);
    }
  }
}

void can_handler__health_check(void) {
  if (can__is_bus_off(can1)) {
    fprintf(stderr, "RESET CAN\n");
    can__reset_bus(can1);
  }
}
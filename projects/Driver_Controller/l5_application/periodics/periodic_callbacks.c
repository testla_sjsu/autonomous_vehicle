#include "periodic_callbacks.h"

#include "board_io.h"
#include "can__handler.h"
#include "driver_logic.h"
#include "gpio.h"
#include "lcd.h"

bool disable_motor;
bool disable_steering;
bool activate_esc_calibration;

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  can_handler__init();
  driver__init();
  lcd__init();
  printf("Hello World__initialize\n");

  disable_motor = false;
  disable_steering = false;
  activate_esc_calibration = false;
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led0());

  can_handler__health_check();

  // driver__print_sensor_data();
  // driver__print_states_of_car();
  // driver__print_steering_and_velocity();

  // lcd__print_updated_sensor_info_screen_1();
  // lcd__test_print_off_chars_0_79();
  // lcd__test_print_off_chars_80_159();
  // lcd__test_print_off_chars_160_239();
  // lcd__test_print_off_chars_240_255();

  // Serial terminal message
  // driver__print_geo_status_message();
  // driver__print_current_car_location_message();
  // driver__print_final_destination_location_message();

  // can_handler__transmit_debug_messages_1Hz();
  // printf("lat: %f\tlon: %f\n", test_coordiantes.lat_rad, test_coordiantes.lon_rad);
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led1());

  can_handler__read_messages_from_bus();
  // can_handler__manage_mia_10Hz();
  driver__navigation_in_progress();
  // driver__read_sensor_values_and_update_motor_controller_commands();
  can_handler__transmit_messages_10hz();

  // 5 Hz Signal
  if (!(callback_count % 2)) {
    lcd__print_updated_sensor_info_screen_1();
  }

  // // Switch controls
  // if (gpio__get(board_io__get_sw0())) {
  //   activate_esc_calibration = true;
  // } else if (gpio__get(board_io__get_sw1())) {
  //   disable_motor = true;
  // } else if (gpio__get(board_io__get_sw2())) {
  //   disable_steering = true;
  // } else if (gpio__get(board_io__get_sw3())) {
  //   disable_steering = false;
  //   disable_motor = false;
  // }
  // // End Switch controls}
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led2());
  // Add your code here
}

// /**
//  * @warning
//  * This is a very fast 1ms task and care must be taken to use this
//  * This may be disabled based on intialization of periodic_scheduler__initialize()
//  */
// void periodic_callbacks__1000Hz(uint32_t callback_count) {
//   gpio__toggle(board_io__get_led3());
//   // Add your code here
// }
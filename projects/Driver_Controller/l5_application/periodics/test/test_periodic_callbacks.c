#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file

#include "Mockboard_io.h"
#include "Mockcan__handler.h"
#include "Mockdriver_logic.h"
#include "Mockgpio.h"
#include "Mocklcd.h"

// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  driver__init_Expect();
  lcd__init_Expect();
  can_handler__init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  can_handler__health_check_Expect();
  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) {
  gpio_s gpio = {};
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  can_handler__read_messages_from_bus_Expect();

  driver__navigation_in_progress_Expect();
  // driver__read_sensor_values_and_update_motor_controller_commands_Expect();
  can_handler__transmit_messages_10hz_Expect();

  // 2 Hz Signal
  uint32_t simulated_callback_count;
  if (!(simulated_callback_count % 2)) {
    lcd__print_updated_sensor_info_screen_1_Expect();
  }
  simulated_callback_count += 1;

  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  gpio_s gpio = {};
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  periodic_callbacks__100Hz(0);
}
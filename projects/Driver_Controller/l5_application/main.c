#include <stdio.h>

#include "FreeRTOS.h"
#include "gpio.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

#include "periodic_scheduler.h"

int main(void) {
  // I2C0 is on P0.10, P0.11
  gpio__construct_with_function(GPIO__PORT_1, 30, GPIO__FUNCTION_4); // P1.30 - I2C-0 SDA
  gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCTION_4); // P1.31 - I2C-0 SCL

  // // UART2 is on P0.10, P0.11
  // gpio__construct_with_function(GPIO__PORT_0, 10, GPIO__FUNCTION_1); // P0.10 - Uart-2 TX
  // gpio__construct_with_function(GPIO__PORT_0, 11, GPIO__FUNCTION_1); // P0.11 - Uart-2 RX
  // gpio_s uart_rx_pin = {GPIO__PORT_0, 11};
  // gpio__disable_pull_down_resistors(uart_rx_pin);

  puts("Starting RTOS");
  periodic_scheduler__initialize();
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}
#include "LCD_I2C.h"
#include "clock.h"
#include "delay.h"
#include "i2c.h"
#include <stdint.h>
#include <stdio.h>

#define constrain(amt, low, high) ((amt) < (low) ? (low) : ((amt) > (high) ? (high) : (amt)))

// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).
void LiquidCrystal_I2C(uint8_t addr, uint8_t cols, uint8_t rows) {
  _Addr = addr;
  _cols = cols;
  _rows = rows;
  _backlightval = LCD_NOBACKLIGHT;
}

void LCD_init() { init_priv(); }

void init_priv() {
  // Wire.begin();
  static StaticSemaphore_t binary_semaphore_struct;
  static StaticSemaphore_t mutex_struct;
  uint32_t i2c_speed_hz = UINT32_C(100) * 1000; // Original 400kHZ
  i2c__initialize(I2C__2, i2c_speed_hz, clock__get_peripheral_clock_hz(), &binary_semaphore_struct, &mutex_struct);
  _displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
  begin(_cols, _rows, LCD_5x8DOTS);
}

void begin(uint8_t cols, uint8_t lines, uint8_t charsize) {
  if (lines > 1) {
    _displayfunction |= LCD_2LINE;
  }
  _numlines = lines;

  // for some 1 line displays you can select a 10 pixel high font
  if ((charsize != 0) && (lines == 1)) {
    _displayfunction |= LCD_5x10DOTS;
  }

  // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
  // according to datasheet, we need at least 40ms after power rises above 2.7V
  // before sending commands. Arduino can turn on way before 4.5V so we'll wait 50
  delay__us(50000);

  // Now we pull both RS and R/W low to begin commands
  expanderWrite(_backlightval); // reset expanderand turn backlight off (Bit 8 =1)
  delay__ms(1000);

  // put the LCD into 4 bit mode
  // this is according to the hitachi HD44780 datasheet
  // figure 24, pg 46

  // we start in 8bit mode, try to set 4 bit mode
  write4bits(0x30);
  delay__us(4500); // wait min 4.1ms

  // second try
  write4bits(0x30);
  delay__us(4500); // wait min 4.1ms

  // third go!
  write4bits(0x30);
  delay__us(150);

  // finally, set to 4-bit interface
  write4bits(0x20);

  // set # lines, font size, etc.
  command(LCD_FUNCTIONSET | _displayfunction);

  // turn the display on with no cursor or blinking default
  _displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
  display();

  // clear it off
  clear();

  // Initialize to default text direction (for roman languages)
  _displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;

  // set the entry mode
  command(LCD_ENTRYMODESET | _displaymode);

  home();
}

/********** high level commands, for the user! */
void clear() {
  command(LCD_CLEARDISPLAY); // clear display, set cursor position to zero
  delay__us(2000);           // this command takes a long time!
}

// Clear particular segment of a row
void clear_particular(uint8_t rowStart, uint8_t colStart, uint8_t colCnt) {
  // Maintain input parameters
  rowStart = constrain(rowStart, 0, _rows - 1);
  colStart = constrain(colStart, 0, _cols - 1);
  colCnt = constrain(colCnt, 0, _cols - colStart);
  // Clear segment
  setCursor(colStart, rowStart);
  for (uint8_t i = 0; i < colCnt; i++)
    write(' ');
  // Go to segment start
  setCursor(colStart, rowStart);
}

void home() {
  command(LCD_RETURNHOME); // set cursor position to zero
  delay__us(2000);         // this command takes a long time!
}

void setCursor(uint8_t col, uint8_t row) {
  int row_offsets[] = {0x00, 0x40, 0x14, 0x54};
  if (row > _numlines) {
    row = _numlines - 1; // we count rows starting w/0
  }
  command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void noDisplay() {
  _displaycontrol &= ~LCD_DISPLAYON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void display() {
  _displaycontrol |= LCD_DISPLAYON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turns the underline cursor on/off
void noCursor() {
  _displaycontrol &= ~LCD_CURSORON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void cursor() {
  _displaycontrol |= LCD_CURSORON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void noBlink() {
  _displaycontrol &= ~LCD_BLINKON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}
void blink() {
  _displaycontrol |= LCD_BLINKON;
  command(LCD_DISPLAYCONTROL | _displaycontrol);
}

// These commands scroll the display without changing the RAM
void scrollDisplayLeft(void) { command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT); }
void scrollDisplayRight(void) { command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT); }

// This is for text that flows Left to Right
void leftToRight(void) {
  _displaymode |= LCD_ENTRYLEFT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void rightToLeft(void) {
  _displaymode &= ~LCD_ENTRYLEFT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void autoscroll(void) {
  _displaymode |= LCD_ENTRYSHIFTINCREMENT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void noAutoscroll(void) {
  _displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
  command(LCD_ENTRYMODESET | _displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void createChar(uint8_t location, uint8_t charmap[]) {
  location &= 0x7; // we only have 8 locations 0-7
  command(LCD_SETCGRAMADDR | (location << 3));
  for (int i = 0; i < 8; i++) {
    write(charmap[i]);
  }
}

// Turn the (optional) backlight off/on
void noBacklight(void) {
  _backlightval = LCD_NOBACKLIGHT;
  expanderWrite(0);
}

void backlight(void) {
  _backlightval = LCD_BACKLIGHT;
  expanderWrite(0);
}

/*********** mid level commands, for sending data/cmds */

void command(uint8_t value) {
  // This is the function that send t conmand Byte into the bus (Control Register)
  send(value, 0);
}

size_t write(uint8_t value) {

  // This is the function that send the data Byte into the bus ( VRAM)
  send(value, Rs);
  return 1; // Number of processed bytes
}

/************ low level data pushing commands **********/

// write either command or data
void send(uint8_t value, uint8_t mode) {
  uint8_t highnib = value & 0xF0;
  uint8_t lownib = value << 4;
  write4bits((highnib) | mode);
  write4bits((lownib) | mode);
}

void write4bits(uint8_t value) {
  expanderWrite(value);
  pulseEnable(value);
}

void expanderWrite(uint8_t _data) {
  //===================Arduino Part================
  // Wire.beginTransmission(_Addr);
  // Wire.write((int)(_data) | _backlightval);
  // Wire.endTransmission();
  //===============================================

  /* This function is a low level byte transfer to LCD module*/
  uint8_t data_transfer = ((_data) | _backlightval);
  i2c__write_single(I2C__2, _Addr, 0x00, data_transfer);
}

void pulseEnable(uint8_t _data) {
  expanderWrite(_data | En); // En high
  delay__us(1);              // enable pulse must be >450ns

  expanderWrite(_data & ~En); // En low
  delay__us(50);              // commands need > 37us to settle
}

// Create custom characters for horizontal graphs
uint8_t graphHorizontalChars(uint8_t rowPattern) {
  uint8_t cc[LCD_CHARACTER_VERTICAL_DOTS];
  for (uint8_t idxCol = 0; idxCol < LCD_CHARACTER_HORIZONTAL_DOTS; idxCol++) {
    for (uint8_t idxRow = 0; idxRow < LCD_CHARACTER_VERTICAL_DOTS; idxRow++) {
      cc[idxRow] = rowPattern << (LCD_CHARACTER_HORIZONTAL_DOTS - 1 - idxCol);
    }
    createChar(idxCol, cc);
  }
  return LCD_CHARACTER_HORIZONTAL_DOTS;
}

// Create custom characters for vertical graphs
uint8_t graphVerticalChars(uint8_t rowPattern) {
  uint8_t cc[LCD_CHARACTER_VERTICAL_DOTS];
  for (uint8_t idxChr = 0; idxChr < LCD_CHARACTER_VERTICAL_DOTS; idxChr++) {
    for (uint8_t idxRow = 0; idxRow < LCD_CHARACTER_VERTICAL_DOTS; idxRow++) {
      cc[LCD_CHARACTER_VERTICAL_DOTS - idxRow - 1] = idxRow > idxChr ? 0b00000 : rowPattern;
    }
    createChar(idxChr, cc);
  }
  return LCD_CHARACTER_VERTICAL_DOTS;
}

// Initializes custom characters for input graph type
uint8_t init_bargraph(uint8_t graphtype) {
  // Initialize row state vector
  for (uint8_t i = 0; i < _rows; i++) {
    _graphstate[i] = 255;
  }
  switch (graphtype) {
  case LCDI2C_VERTICAL_BAR_GRAPH:
    graphVerticalChars(0b11111);
    // Initialize column state vector
    for (uint8_t i = _rows; i < _cols; i++) {
      _graphstate[i] = 255;
    }
    break;
  case LCDI2C_HORIZONTAL_BAR_GRAPH:
    graphHorizontalChars(0b11111);
    break;
  case LCDI2C_HORIZONTAL_LINE_GRAPH:
    graphHorizontalChars(0b00001);
    break;
  default:
    return 1;
  }
  _graphtype = graphtype;
  return 0;
}

// Display horizontal graph from desired cursor position with input value
void draw_horizontal_graph(uint8_t row, uint8_t column, uint8_t len, uint8_t pixel_col_end) {
  // Maintain input parameters
  row = constrain(row, 0, _rows - 1);
  column = constrain(column, 0, _cols - 1);
  len = constrain(len, 0, _cols - column);
  pixel_col_end = constrain(pixel_col_end, 0, (len * LCD_CHARACTER_HORIZONTAL_DOTS) - 1);
  _graphstate[row] = constrain(_graphstate[row], column, column + len - 1);
  // Display graph
  switch (_graphtype) {
  case LCDI2C_HORIZONTAL_BAR_GRAPH:
    setCursor(column, row);
    // Display full characters
    for (uint8_t i = 0; i < pixel_col_end / LCD_CHARACTER_HORIZONTAL_DOTS; i++) {
      write(LCD_CHARACTER_HORIZONTAL_DOTS - 1);
      column++;
    }
    // Display last character
    write(pixel_col_end % LCD_CHARACTER_HORIZONTAL_DOTS);
    // Clear remaining chars in segment
    for (uint8_t i = column; i < _graphstate[row]; i++)
      write(' ');
    // Last drawn column as graph state
    _graphstate[row] = column;
    break;
  case LCDI2C_HORIZONTAL_LINE_GRAPH:
    // Drawn column as graph state
    column += pixel_col_end / LCD_CHARACTER_HORIZONTAL_DOTS;
    // Clear previous drawn character if differs from new one
    if (_graphstate[row] != column) {
      setCursor(_graphstate[row], row);
      write(' ');
      _graphstate[row] = column;
    }
    // Display graph character
    setCursor(column, row);
    write(pixel_col_end % LCD_CHARACTER_HORIZONTAL_DOTS);
    break;
  default:
    return;
  }
}
// Display horizontal graph from desired cursor position with input value
void draw_vertical_graph(uint8_t row, uint8_t column, uint8_t len, uint8_t pixel_row_end) {
  // Maintain input parameters
  row = constrain(row, 0, _rows - 1);
  column = constrain(column, 0, _cols - 1);
  len = constrain(len, 0, row + 1);
  pixel_row_end = constrain(pixel_row_end, 0, (len * LCD_CHARACTER_VERTICAL_DOTS) - 1);
  _graphstate[column] = constrain(_graphstate[column], row - len + 1, row);
  // Display graph
  switch (_graphtype) {
  case LCDI2C_VERTICAL_BAR_GRAPH:
    // Display full characters
    for (uint8_t i = 0; i < pixel_row_end / LCD_CHARACTER_VERTICAL_DOTS; i++) {
      setCursor(column, row--);
      write(LCD_CHARACTER_VERTICAL_DOTS - 1);
    }
    // Display the highest character
    setCursor(column, row);
    write(pixel_row_end % LCD_CHARACTER_VERTICAL_DOTS);
    // Clear remaining top chars in column
    for (uint8_t i = _graphstate[column]; i < row; i++) {
      setCursor(column, i);
      write(' ');
    }
    _graphstate[column] = row; // Last drawn row as its state
    break;
  default:
    return;
  }
}
// Overloaded methods
void draw_horizontal_graph_percentage(uint8_t row, uint8_t column, uint8_t len, uint16_t percentage) {
  percentage = (percentage * len * LCD_CHARACTER_HORIZONTAL_DOTS / 100) - 1;
  draw_horizontal_graph(row, column, len, (uint8_t)percentage);
}
void draw_horizontal_graph_ratio(uint8_t row, uint8_t column, uint8_t len, float ratio) {
  ratio = (ratio * len * LCD_CHARACTER_HORIZONTAL_DOTS) - 1;
  draw_horizontal_graph(row, column, len, (uint8_t)ratio);
}
void draw_vertical_graph_percentage(uint8_t row, uint8_t column, uint8_t len, uint16_t percentage) {
  percentage = (percentage * len * LCD_CHARACTER_VERTICAL_DOTS / 100) - 1;
  draw_vertical_graph(row, column, len, (uint8_t)percentage);
}
void draw_vertical_graph_ratio(uint8_t row, uint8_t column, uint8_t len, float ratio) {
  ratio = (ratio * len * LCD_CHARACTER_VERTICAL_DOTS) - 1;
  draw_vertical_graph(row, column, len, (uint8_t)ratio);
}

// Alias functions

void on() { display(); }

void off() { noDisplay(); }

void cursor_on() { cursor(); }

void cursor_off() { noCursor(); }

void blink_on() { blink(); }

void blink_off() { noBlink(); }

void load_custom_character(uint8_t char_num, uint8_t *rows) { createChar(char_num, rows); }

void setBacklight(uint8_t new_val) {
  if (new_val) {
    backlight(); // turn backlight on
  } else {
    noBacklight(); // turn backlight off
  }
}

void printstr_lcd(char *String_Input, uint8_t rowStart, uint8_t colStart) {

  // wrap-around case -->
  if (rowStart == 2 && colStart < 21) {
    rowStart = 0;
    colStart = colStart + 20;
  } else if (rowStart == 3 && colStart < 21) {
    rowStart = 1;
    colStart = colStart + 20;
  }

  setCursor(colStart, rowStart);

  // find the size of the string
  uint8_t string_size = 0;
  while (NULL != String_Input[string_size]) {
    string_size++;
  }

  // Print out the string
  for (int string_index = 0; string_index < string_size; string_index++) {
    write(String_Input[string_index]);
  }
}

#include <math.h>
#include <stdio.h>

#include "i2c.h"
#include "lpc40xx.h"

#include "custom_queue.h"
#include "sensor.h"
#include "testla_proto1.h"

static dbc_SENSOR_SONARS_s sensor_data = {0};
static sonar_sensor__sample_sum_s sample_sum = {0};
static sonar_sensor__sample_queue_s sample_queue = {0};

static uint16_t memory_left[SAMPLE_N];
static uint16_t memory_middle[SAMPLE_N];
static uint16_t memory_right[SAMPLE_N];
static uint16_t memory_back[SAMPLE_N];

static queue_s *sensor__get_sample_queue(sonar_sensor__address_e address) {
  switch (address) {
  case SONAR_SENSOR__LEFT:
    return &sample_queue.left;
  case SONAR_SENSOR__MIDDLE:
    return &sample_queue.middle;
  case SONAR_SENSOR__RIGHT:
    return &sample_queue.right;
  case SONAR_SENSOR__BACK:
    return &sample_queue.back;
  }
  return NULL;
}

static float *sensor__get_sample_sum(sonar_sensor__address_e address) {
  switch (address) {
  case SONAR_SENSOR__LEFT:
    return &sample_sum.left;
  case SONAR_SENSOR__MIDDLE:
    return &sample_sum.middle;
  case SONAR_SENSOR__RIGHT:
    return &sample_sum.right;
  case SONAR_SENSOR__BACK:
    return &sample_sum.back;
  }
  return NULL;
}

static bool sensor__setup_single_sonar_sensor(sonar_sensor__address_e address, sonar_sensor__measure_mode_e mode,
                                              sonar_sensor__measure_range_e range) {
  uint8_t config_register_value = ((uint8_t)mode) | ((uint8_t)range);
  if (i2c__read_single(I2C__2, address, CONFIG_REGISTER) == config_register_value) {
    return true;
  } else {
    if (i2c__write_single(I2C__2, address, CONFIG_REGISTER, config_register_value)) {
      return true;
    } else {
      return false;
    }
  }
}

static uint16_t sensor__read_single_sonar_sensor(sonar_sensor__address_e address) {
  uint8_t distance_lowbyte = i2c__read_single(I2C__2, address, DISTANCE_LOWBYTE_REGISTER);
  uint8_t distance_highbyte = i2c__read_single(I2C__2, address, DISTANCE_HIGHBYTE_REGISTER);
  uint16_t distance = distance_lowbyte; // ignore highbyte for now
  return distance;
}

static void sensor__collect_single_sensor_sample(sonar_sensor__address_e sensor) {
  uint16_t sample = sensor__read_single_sonar_sensor(sensor);
  queue_s *queue_ptr = sensor__get_sample_queue(sensor);
  float *sum_ptr = sensor__get_sample_sum(sensor);
  // if queue is full, remove oldest sample from sample sum
  if (queue__get_item_count(queue_ptr) == SAMPLE_N) {
    uint16_t popped_value;
    queue__pop(queue_ptr, &popped_value);
    *sum_ptr = *sum_ptr - popped_value;
  }
  // add latest sample to sample sum
  *sum_ptr = *sum_ptr + sample;
  // add to queue
  queue__push(queue_ptr, sample);
}

static void sensor__collect_sample() {
  sensor__collect_single_sensor_sample(SONAR_SENSOR__LEFT);
  sensor__collect_single_sensor_sample(SONAR_SENSOR__MIDDLE);
  sensor__collect_single_sensor_sample(SONAR_SENSOR__RIGHT);
  sensor__collect_single_sensor_sample(SONAR_SENSOR__BACK);
}

static dbc_SENSOR_SONARS_s sensor__calculate_sensor_data() {
  dbc_SENSOR_SONARS_s sensor = {0};
  sensor.SENSOR_SONARS_left = (uint16_t)ceil(sample_sum.left / queue__get_item_count(&sample_queue.left));
  sensor.SENSOR_SONARS_middle = (uint16_t)ceil(sample_sum.middle / queue__get_item_count(&sample_queue.middle));
  sensor.SENSOR_SONARS_right = (uint16_t)ceil(sample_sum.right / queue__get_item_count(&sample_queue.right));
  sensor.SENSOR_SONARS_back = (uint16_t)ceil(sample_sum.back / queue__get_item_count(&sample_queue.back));
  return sensor;
}

bool sensor__init() {
  queue__init(&sample_queue.left, memory_left, SAMPLE_N);
  queue__init(&sample_queue.middle, memory_middle, SAMPLE_N);
  queue__init(&sample_queue.right, memory_right, SAMPLE_N);
  queue__init(&sample_queue.back, memory_back, SAMPLE_N);
  return sensor__setup_single_sonar_sensor(SONAR_SENSOR__LEFT, MEASURE_MODE__AUTOMATIC, MEASURE_RANGE__150) &&
         sensor__setup_single_sonar_sensor(SONAR_SENSOR__MIDDLE, MEASURE_MODE__AUTOMATIC, MEASURE_RANGE__150) &&
         sensor__setup_single_sonar_sensor(SONAR_SENSOR__RIGHT, MEASURE_MODE__AUTOMATIC, MEASURE_RANGE__150) &&
         sensor__setup_single_sonar_sensor(SONAR_SENSOR__BACK, MEASURE_MODE__AUTOMATIC, MEASURE_RANGE__150);
}

void sensor__run_once() {
  sensor__collect_sample();
  sensor_data = sensor__calculate_sensor_data();
  can_handler__send_sensor_update(&sensor_data);
}

dbc_SENSOR_SONARS_s sensor__get_sensor_data() { return sensor_data; }

#pragma once

#include "custom_queue.h"
#include <string.h>

void queue__init(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_words) {
  // reset queue struct
  memset(queue, 0, sizeof(queue_s));
  if (static_memory_for_queue != NULL) {
    // update queue memory & memory_size
    queue->static_memory_for_queue = static_memory_for_queue;
    queue->static_memory_size_in_words = static_memory_size_in_words;
    // clear queue memory
    memset(queue->static_memory_for_queue, 0, static_memory_size_in_words * sizeof(uint16_t));
    // start & end point to first byte in queue_memory
    queue->start_ptr = &queue->static_memory_for_queue[0];
    queue->end_ptr = &queue->static_memory_for_queue[0];
  }
}

bool queue__push(queue_s *queue, uint16_t push_value) {
  // only push when queue is not full
  if (queue->item_count < queue->static_memory_size_in_words) {
    // insert value to queue_memory
    *queue->end_ptr = push_value;
    // increment end_ptr (with wrap around)
    if (queue->end_ptr == &queue->static_memory_for_queue[queue->static_memory_size_in_words - 1]) {
      queue->end_ptr = &queue->static_memory_for_queue[0];
    } else {
      queue->end_ptr++;
    }
    // increment item_count
    queue->item_count++;
    return true;
  }
  return false;
}

bool queue__pop(queue_s *queue, uint16_t *pop_value) {
  // only pop when queue is not empty
  if (queue->item_count > 0) {
    // update pop_value
    *pop_value = *queue->start_ptr;
    // increment start_ptr (with wrap around)
    if (queue->start_ptr == &queue->static_memory_for_queue[queue->static_memory_size_in_words - 1]) {
      queue->start_ptr = &queue->static_memory_for_queue[0];
    } else {
      queue->start_ptr++;
    }
    // decrement item_count
    queue->item_count--;
    return true;
  }
  return false;
}

size_t queue__get_item_count(const queue_s *queue) { return queue->item_count; }

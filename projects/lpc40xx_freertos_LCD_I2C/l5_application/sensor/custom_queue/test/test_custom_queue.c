#include "custom_queue.h"
#include "unity.h"
#include <string.h>

#define SIZE 10

static uint16_t memory[SIZE];
queue_s queue;

void setUp() { queue__init(&queue, memory, SIZE); }

void test_queue__init() {
  // create a new memory block
  // we don't use global memory for this test
  static uint16_t mem[10];

  // make this memory block dirty
  memset(mem, (uint8_t)0xFF, sizeof(mem));

  // inititialize queue
  queue__init(&queue, mem, 10);

  // static_memory_for_queue should point to mem
  TEST_ASSERT_EQUAL_PTR(mem, queue.static_memory_for_queue);

  // queue__init should reset queue memory
  for (uint16_t i = 0; i < 10; i++) {
    TEST_ASSERT_EQUAL_UINT16(0, queue.static_memory_for_queue[i]);
  }
}

void test_queue__init__no_memory() {
  // inititialize queue with no mem block
  queue__init(&queue, NULL, 10);
  // static_memory_size_in_bytes should be 0
  TEST_ASSERT_EQUAL_size_t(0, queue.static_memory_size_in_words);
  // all internal pointers should be NULL
  TEST_ASSERT_NULL(queue.static_memory_for_queue);
  TEST_ASSERT_NULL(queue.start_ptr);
  TEST_ASSERT_NULL(queue.end_ptr);
  // item_count should be zero;
  TEST_ASSERT_EQUAL_size_t(0, queue.item_count);
}

void test_queue__push() {
  uint16_t queue_value = 10;

  // fill up queue
  // queue__push should return True
  for (uint16_t i = 0; i < SIZE; i++) {
    TEST_ASSERT_TRUE(queue__push(&queue, queue_value));
  }

  // insert new value when queue is full
  // queue__push should return False
  TEST_ASSERT_FALSE(queue__push(&queue, queue_value));
}

void test_queue__pop() {
  uint16_t popped_value = 0;

  // pop when queue is empty (right after queue__init)
  // queue__pop should return False
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));

  // fill up queue with value from 1 to 128
  for (uint16_t value = 1; value <= SIZE; value++) {
    queue__push(&queue, value);
  }

  // pop until queue is empty
  for (uint16_t value = 1; value <= SIZE; value++) {
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL_UINT16(value, popped_value);
  }

  // pop when queue is empty
  // queue__pop should return False
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));
}

void test_queue__get_item_count() {
  uint16_t item_value = 10;
  uint16_t n_items = 10;

  // item_count should be 0 after init
  TEST_ASSERT_EQUAL_size_t(0, queue__get_item_count(&queue));

  // push 50 items to queue
  for (uint16_t i = 0; i < 10; i++) {
    queue__push(&queue, item_value);
  }

  // item_count should be 50
  TEST_ASSERT_EQUAL_size_t(10, queue__get_item_count(&queue));
}

void test_comprehensive(void) {

  for (size_t item = 0; item < SIZE; item++) {
    const uint16_t item_pushed = (uint16_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue__push(&queue, 123));
  TEST_ASSERT_EQUAL(SIZE, queue__get_item_count(&queue));

  // Pull and verify the FIFO order
  for (size_t item = 0; item < SIZE; item++) {
    uint16_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint16_t)item, popped_value);
  }

  // Test wrap-around case
  const uint16_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue__push(&queue, pushed_value));
  uint16_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count(&queue));
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));
}

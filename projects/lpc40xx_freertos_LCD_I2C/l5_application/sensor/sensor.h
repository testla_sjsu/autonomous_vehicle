#pragma once
#include "custom_queue.h"
#include "testla_proto1.h"

#include <stdint.h>

#define ADDRESS_REGISTER 0x00
#define CONFIG_REGISTER 0x07
#define DISTANCE_HIGHBYTE_REGISTER 0x04
#define DISTANCE_LOWBYTE_REGISTER 0x04

#define SAMPLE_N 10
#define SENSOR_N 4

typedef struct {
  float left;
  float middle;
  float right;
  float back;
} sonar_sensor__sample_sum_s;

typedef struct {
  queue_s left;
  queue_s middle;
  queue_s right;
  queue_s back;
} sonar_sensor__sample_queue_s;

typedef enum {
  SONAR_SENSOR__LEFT = 0x20,
  SONAR_SENSOR__MIDDLE = 0x22,
  SONAR_SENSOR__RIGHT = 0x24,
  SONAR_SENSOR__BACK = 0x26,
} sonar_sensor__address_e;

typedef enum {
  MEASURE_MODE__AUTOMATIC = 0x80,
  MEASURE_MODE__PASSIVE = 0x00,
} sonar_sensor__measure_mode_e;

typedef enum {
  MEASURE_RANGE__150 = 0x00,
  MEASURE_RANGE__300 = 0x10,
  MEASURE_RANGE__500 = 0x20,
} sonar_sensor__measure_range_e;

bool sensor__init();
void sensor__run_once();

dbc_SENSOR_SONARS_s sensor__get_sensor_data();

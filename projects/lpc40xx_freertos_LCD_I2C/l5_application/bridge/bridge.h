#pragma once

#define BRIDGE_BUFFER_SIZE 64

typedef char bridge__line_buffer_t[BRIDGE_BUFFER_SIZE];

void bridge__init(void);
void bridge__run_once(void);

void bridge__network_handle_task(void);
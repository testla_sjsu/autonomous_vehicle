#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "board_io.h"
#include "esp32.h"
#include "esp32_task.h"
#include "gpio.h"
#include "uart3_init.h"

#include "testla_proto1.h"

#include "bridge.h"
#include "can_handler.h"
#include "sensor.h"

static const uart_e esp32__uart = UART__3;

static const char *hostname = "54.151.27.107"; // aws ec2
static uint16_t port = 8080;

static bridge__line_buffer_t response_buffer = {0};
static volatile bool esp32_ready = false;
static volatile bool tcp_socket_created = false;

static gpio_s wifi_reset_sw;
static uint16_t sw_debounce_state = 0;

// static dbc_DEBUG_GPS_CURRENT_LOCATION_s location;
// static bool new_location_received = false;

static bool wifi_reset_pressed() {
  bool isPressed = gpio__get(wifi_reset_sw);
  sw_debounce_state = (sw_debounce_state << 1) | isPressed | 0xE000;
  if (sw_debounce_state == 0xf000) {
    printf("network reset pressed\n");
    return true;
  } else {
    return false;
  }
}

static void callback__server_response(char *response, size_t length) {
  dbc_SENSOR_DESTINATION_LOCATION_s dest = {0};
  memset(response_buffer, '\0', sizeof(char) * BRIDGE_BUFFER_SIZE); // clear BUFFER
  memcpy(response_buffer, response, length);                        // copy new response to buffer
  char *message = strstr(response_buffer, "GPS_DEST:");             // move ptr to start of message
  if (message != NULL) {
    message += 9; // offset "GPS_DEST:" header
    char *ptr = strtok(message, ",");
    if (ptr != NULL) {
      dest.SENSOR_DESTINATION_latitude = (float)atof(ptr);
    }
    ptr = strtok(NULL, ",");
    if (ptr != NULL) {
      dest.SENSOR_DESTINATION_longitude = (float)atof(ptr);
    }
    if (dest.SENSOR_DESTINATION_latitude != 0 && dest.SENSOR_DESTINATION_longitude != 0) {
      can_handler__send_gps_destination(&dest);
    }
  }
}

static bool setup_esp32() {
  vTaskDelay(200);
  bool success = true;
  // Init ESP32
  esp32__init(esp32__uart);
  esp32__send_command("AT+RST");
  esp32__clear_receive_buffer(5000);
  esp32__register_response_callback(callback__server_response);
  // Test AT commands
  success &= esp32__wait_for_successful_command("AT", "OK", "Basic communication test");
  success &= esp32__wait_for_successful_command("ATE0", "OK", "Turn off echo");
  success &= esp32__wait_for_successful_command("AT+CIPMUX=0", "OK", "Single connection");
  // Enter "Station Mode"
  success &= esp32__wait_for_successful_command("AT+CWMODE=1", "OK", "Connect to WIFI");
  return success;
}

static void bridge__send_sensors_to_server(dbc_SENSOR_SONARS_s sensors) {
  bridge__line_buffer_t buffer = {0};
  snprintf(buffer, sizeof(buffer), "SENSORS:%d,%d,%d,%d\r\n", sensors.SENSOR_SONARS_left, sensors.SENSOR_SONARS_middle,
           sensors.SENSOR_SONARS_right, sensors.SENSOR_SONARS_back);
  esp32__cipsend(buffer, strlen(buffer));
}

static void bridge__send_location_to_server(dbc_DEBUG_GPS_CURRENT_LOCATION_s location) {
  bridge__line_buffer_t buffer = {0};
  snprintf(buffer, sizeof(buffer), "LOCATION:%.8f,%.8f\r\n", location.DEBUG_GPS_CURRENT_LOCATION_latitude,
           location.DEBUG_GPS_CURRENT_LOCATION_longitude);
  esp32__cipsend(buffer, strlen(buffer));
}

void bridge__init(void) {}

void bridge__run_once(void) {
  if (tcp_socket_created) {
    dbc_SENSOR_SONARS_s sensor_data = sensor__get_sensor_data();
    bridge__send_sensors_to_server(sensor_data);
    if (can_handler__new_location_available()) {
      dbc_DEBUG_GPS_CURRENT_LOCATION_s location = can_handler__get_current_location();
      bridge__send_location_to_server(location);
    }
  } else if (esp32_ready) {
    tcp_socket_created = esp32__tcp_connect(hostname, port, 5000); // establish tcp/ip socket
  }
}

void bridge__network_handle_task(void) {
  uart3_init();
  esp32_ready = setup_esp32() && esp32__status_wifi_connected();
  while (true) {
    if (wifi_reset_pressed()) {
      esp32_ready = false;
      esp32_ready = setup_esp32() && esp32_task__connect_wifi_by_reading_ssid_from_file();
      if (esp32_ready) {
        printf("Successfully reset ESP32 Wifi\n");
      } else {
        printf("Unable to reset ESP32 Wifi\n");
      }
    }
    vTaskDelay(100);
  }
}
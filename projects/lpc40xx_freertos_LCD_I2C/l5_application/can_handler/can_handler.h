#pragma once

#include "testla_proto1.h"

bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc);

void can_handler__init(void);
bool can_handler__send_sensor_update(dbc_SENSOR_SONARS_s *sensor_data);
bool can_handler__send_gps_destination(dbc_SENSOR_DESTINATION_LOCATION_s *gps_data);
bool can_handler__new_location_available();
void can_handler__handle_all_incoming_messages(void);
dbc_DEBUG_GPS_CURRENT_LOCATION_s can_handler__get_current_location();
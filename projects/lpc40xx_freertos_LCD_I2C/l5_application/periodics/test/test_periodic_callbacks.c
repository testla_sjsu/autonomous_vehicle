#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockbridge.h"
#include "Mockcan_handler.h"
#include "Mockgpio.h"
#include "Mocksensor.h"
// Include the source we wish to test
#include "periodic_callbacks.h"

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  can_handler__init_Expect();
  sensor__init_ExpectAndReturn(true);
  bridge__init_Expect();
  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  // led test
  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  bridge__run_once_Expect();

  periodic_callbacks__1Hz(0);
}

void test__periodic_callbacks__10Hz(void) {
  // led test
  gpio_s gpio = {};
  board_io__get_led1_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  sensor__run_once_Expect();
  can_handler__handle_all_incoming_messages_Expect();

  periodic_callbacks__10Hz(0);
}

void test__periodic_callbacks__100Hz(void) {
  // led test
  gpio_s gpio = {};
  board_io__get_led2_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);

  periodic_callbacks__100Hz(0);
}